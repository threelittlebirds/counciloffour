package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 * Created by Med on 16/06/2016.
 */
public class HandPermitDecoratorMessage implements ResponseMsg {

    private int choice;

    public int getChoice() {
        return choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
