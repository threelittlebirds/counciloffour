package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;
import projectPackage.model.utility.table.TextTable;

import java.util.Arrays;


public class PermitCard extends BonusContainer implements Card, Cloneable {

    private char[] cities; //RICORDA: METTERE TUTTI I CARATTERI IN MAIUSCOLO NELLA FASE DI INZIALIZZAZIONE
	private static int cont = 1;

	public PermitCard() {
	}

	public PermitCard(Bonus bonus, char[] cities) {
        super(bonus);
        this.cities = cities;
        for(int i=0;i<this.cities.length;i++){
        	this.cities[i] = Character.toUpperCase(this.cities[i]);
        }
		cont++;
    }

	public char[] getCities() {
		return cities;
	}

	@Override
	public void activateBonus() throws CloneNotSupportedException {
		System.out.print("PermitCard,");
		super.activateBonus();
	}

	public TextTable printCard(){
		String[] columnNames = { "Permit Card" };
		int numberOfBonuses = bonus.getNumberOfBonuses();
		Object[][] data = new Object [numberOfBonuses+2][columnNames.length];
		String s = "";
		for(int i=0;i<cities.length;i++){
			s = s + cities[i];
			if(i!=cities.length-1) s = s + ",";
		}
		data[0][0] = s;
		data[1][0] = "";
		int i = 2;
		for (String string : bonus.getSeparatedBonuses()) {
			data[i][0] = string;
			i++;
		}
		TextTable tt = new TextTable(columnNames, data);
		return tt;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new PermitCard(bonus, cities);
	}

	@Override
	public int hashCode() {
		final int molt = 41;
		return molt* Arrays.hashCode(cities) + cont;
	}

    /*
    @Override
    public boolean equals(Object obj) {
        PermitCard pc =(PermitCard) obj;
        return ((Arrays.equals(pc.getCities(), cities)) && (pc.getBonus().equals(bonus)));
    }
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermitCard that = (PermitCard) o;

        return Arrays.equals(getCities(), that.getCities());

    }
}

