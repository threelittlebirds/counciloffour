package projectPackage.sharedInterface;

import projectPackage.view.messages.broadcastMessage.BroadcastMessage;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This remote interface is used by the publisher component to send messages to a
 * subscriber.
 */
public interface ISubscriber extends Remote{

        /**
         * Send a message to a subscriber.
         *
         * @param message the message to dispatch
         * @throws RemoteException if there was a problem during data transfer
         */
        void sendMessage(BroadcastMessage message) throws RemoteException;


}
