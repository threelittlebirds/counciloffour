package projectPackage.view.messages.responseMessages;

import projectPackage.view.Token;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.requestMessages.RequestMessage;

/**
 * Created by carme on 27/06/2016.
 */
public class ConnectionResponseMessage implements ResponseMsg {


    private Token token;

    public ConnectionResponseMessage(Token token) {
        this.token=token;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

    public boolean isValid(RequestMessage msg){
        return true;
    }
}
