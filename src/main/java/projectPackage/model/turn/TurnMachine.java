package projectPackage.model.turn;

import java.io.Serializable;
import java.util.LinkedList;

import projectPackage.controller.action.Action;
import projectPackage.model.GameBoard;
import projectPackage.model.Player;
import projectPackage.model.utility.MapLoad;
import projectPackage.view.messages.ResultMsgPair;


public class TurnMachine implements Serializable {
	
	private boolean mainExecuted, quickExecuted, jumpQuickAction,
            mainOrQuick, quick4, isExecutingActionBonus;

	private int actionBonus;
	
	private int counterMarket;

	private int counterBuySell;

    private TurnState currentState;

    private GameBoard gameBoard;

	private String mapSelected;

	private MapLoad mapLoad;

	private int lastPlayerOfMach;

	public TurnMachine(String mapSelected, LinkedList<Player> allPlayers) {

		this.mapSelected=mapSelected;
        currentState  = new InitState(this);
        this.mainExecuted = false;
        this.quickExecuted = false;
        this.jumpQuickAction = false;
		this.mainOrQuick = false;
		this.quick4 = false;
        isExecutingActionBonus = false;
		this.actionBonus = 0;
		this.lastPlayerOfMach = -1;
		this.counterMarket = 1;
		this.counterBuySell = 0;
		gameBoard = new GameBoard(allPlayers);
		this.getGameBoard().setIsFinished(false);
		mapLoad = new MapLoad(this, mapSelected);
		gameBoard.initializeMap(mapLoad.getMap());

    }

	public boolean isJumpQuickAction() {
		return jumpQuickAction;
	}

	public void setJumpQuickAction(boolean jumpQuickAction) {
		this.jumpQuickAction = jumpQuickAction;
	}

	public boolean isMainOrQuick() {
		return mainOrQuick;
	}

	public void setMainOrQuick(boolean mainOrQuick) {
		this.mainOrQuick = mainOrQuick;
	}

	public boolean isMainExecuted() {
		return mainExecuted;
	}

	public void setMainExecuted(boolean mainExecuted) {
		this.mainExecuted = mainExecuted;
	}

	public boolean isQuickExecuted() {
		return quickExecuted;
	}

	public void setQuickExecuted(boolean quickExecuted) {
		this.quickExecuted = quickExecuted;
	}

	public TurnState getCurrentState(){
    	return currentState;
    }

	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}

	public GameBoard getGameBoard(){
    	return gameBoard;
    }
    
    public ResultMsgPair start(Action a) throws CloneNotSupportedException{

		currentState.start(a);
    	return nextState();
    }
    
    public ResultMsgPair nextState() throws CloneNotSupportedException{
    	return currentState.nextState();
    }
    
    public void setCurrentState(TurnState currentState) {
		this.currentState = currentState;
	}


	/**
	 * Return true if player has just done Quick Action 4
	 *
	 * @return quick4
	 */
	public boolean isQuick4() {
		return quick4;
	}

	public void setQuick4(boolean quick4) {
		this.quick4 = quick4;
	}

	/**
	 * This method is used for set the next player during
	 * the normal game or during the phase of marketing
	 * in particular the sell phase.
	 */

	public Player nextPlayer(){
    	if(counterMarket != gameBoard.getNumPlayers()) counterMarket++;
		else counterBuySell++;
    	return gameBoard.nextPlayer();
    }

	/**
	 * This method is used for set the next random player during
	 * the phase of marketing in particular the buy phase.
	 */

	public Player nextRandomPlayer(){
		counterBuySell++;
		return gameBoard.nextRandomPlayer();
	}

    public int getCounterMarket(){
    	return counterMarket;
    }

	public int getCounterBuySell() {
		return counterBuySell;
	}

	public void setCounterBuySell(int counterBuySell) {
		this.counterBuySell = counterBuySell;
	}

	public void setCounterMarket(int newCounterMarket){
    	this.counterMarket = newCounterMarket;
    }

	public int getLastPlayerOfMach() {
		return lastPlayerOfMach;
	}

	public void setLastPlayerOfMach(int lastPlayerOfMach) {
		this.lastPlayerOfMach = lastPlayerOfMach;
	}

    public int getActionBonus() {
        return actionBonus;
    }

    public void setActionBonus(int actionBonus) {
        this.actionBonus = actionBonus;
    }

    public boolean isExecutingActionBonus() {
        return isExecutingActionBonus;
    }

    public void setExecutingActionBonus(boolean executingActionBonus) {
        isExecutingActionBonus = executingActionBonus;
    }

    public void addActionBonus() {
        actionBonus++;
    }

    public void removeActionBonus() {
        actionBonus--;
    }

}
