package projectPackage.model.track;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import projectPackage.model.track.square.Square;

/**
 * This generic class models a track of {@link Square} so it admites
 * any type of Object that implements the Interface Square.
 * The class gives the default methods of generic classes,
 * like adding a Square, checking if contains a Square,
 * if the Track is empty, etc.
 *
 * @param <C> the element type
 */

public class Track<C extends Square> implements Iterable<C>, Serializable{

    protected C track[];
    protected int size;


	public Track(int length) {
		super();
		this.track = (C[]) new Square [length];
		size=0;
	}
    
    public int indexOf(C elem){
    	for(int i=0;i<size;i++){
    		if(track[i].equals(elem)) return i;
    	}
    	return -1;
    }
    
    public boolean contains(C elem){
    	return (indexOf(elem) != -1);
    }

    public boolean isEmpty(){
    	return size == 0;
    }
    
    public C getSquare(int pos){
    	if(pos < 0 || pos >=size) throw new IndexOutOfBoundsException();
    	return track[pos];
    }

	public void add(C elem){
		track[size] = elem;
		size++;
	}

    
    public int size(){
    	return size;
    }
    

	@Override
	public Iterator<C> iterator() {
		return new TrackIterator();
	}

    
	private class TrackIterator implements Iterator<C> {
		
		private int corrente = -1;
		
		@Override
		public boolean hasNext() {
			if(corrente == -1) return size() > 0;
			return corrente < size()-1;
		}

		@Override
		public C next() {
			if(!hasNext()) throw new NoSuchElementException();
			corrente++;
			return track[corrente];
		}
		
	}
    

}
