package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class EndAction extends Action {
    
	
	
	public EndAction(TurnMachine tm) {
		super(tm);
	}

	@Override
	public ResultMsgPair execute() throws CloneNotSupportedException {
		return tm.start(this);
	}

}
