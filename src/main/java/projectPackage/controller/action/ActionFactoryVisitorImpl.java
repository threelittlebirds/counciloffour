package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.requestMessages.BonusFilledMessage;
import projectPackage.view.messages.requestMessages.requestActions.*;


public class ActionFactoryVisitorImpl implements ActionFactoryVisitor {
    
	private TurnMachine tm;
	
	public ActionFactoryVisitorImpl(TurnMachine tm) {
		super();
		this.tm = tm;
	}

	@Override
    public Action visit(InitMessage msg){
    	return new InitAction(msg.getPlayers(),tm);
    }
	
    @Override
    public Action visit(MainAction1Message msg) {
    	return new MainAction1(msg.getWhichCouncillor(),msg.getWhichCouncil(),tm);
    }
    
    public Action visit(MainAction2Message msg){
    	return new MainAction2(msg.getWhichPermitCard(),msg.getWhichCouncilRegion() , tm);
    	
    }

    @Override
	public Action visit(MainAction3Message msg) {
		return new MainAction3(msg.getWhichPrivatePermitCard(),msg.getWhichCharacter(), tm);
	}

	@Override
	public Action visit(MainAction4Message msg) {
		return new MainAction4(msg.getWhichCity(), tm);
	}	
	
	@Override
	public Action visit(QuickAction1Message msg) {
		return new QuickAction1(tm);
	}

	@Override
	public Action visit(QuickAction2Message msg) {
		return new QuickAction2(msg.getWhichRegion(), tm);
	}

	@Override
	public Action visit(QuickAction3Message msg) {
		return new QuickAction3(msg.getWhichRegion(),msg.getWhichCouncillor(), tm);
	}

	@Override
	public Action visit(QuickAction4Message msg) {
		return new QuickAction4(tm);
	}
	
	@Override
	public Action visit(MainQuickRequestMsg msg) {
		return new BeginAction(msg.getChoiceAction(), tm);
	}

	@Override
	public Action visit(NextTurnMessage msg) {
		return new NextTurnAction(tm);
	}

	@Override
	public Action visit(EndRequestMsg msg) {
		return new EndAction(tm);
	}

	@Override
	public Action visit(BuyRequestMsg msg) {
		return new WantToBuyAction(tm);
	}

	@Override
	public Action visit(SellRequestMsg msg) {
		return new WantToSellAction(tm);
	}

	@Override
	public Action visit(BonusFilledMessage msg) {
		return new ActivateBonusAction(tm, msg.getList());
	}

	@Override
	public Action visit(SellFilledRequestMsg msg) {
		return new SellAction(msg.getPermitToSell(),msg.getPoliticToSell(),msg.getAssintantToSell(),tm);
	}

	@Override
	public Action visit(BuyFilledRequestMsg msg) {
		return new BuyAction(msg.getStaffToBuy(),tm);
	}


}