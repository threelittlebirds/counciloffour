package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class QuickAction1Message extends ActionRequestMsg {

	public QuickAction1Message(Token token) {
		super(token);
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
