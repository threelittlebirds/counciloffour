package projectPackage.view.messages.responseMessages;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by carme on 04/07/2016.
 */
public class InfoResponseMsg implements ResponseMsg {

    private String genericInfo;

    public InfoResponseMsg(String genericInfo) {
        this.genericInfo = genericInfo;
    }

    public String getGenericInfo() {
        return genericInfo;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
