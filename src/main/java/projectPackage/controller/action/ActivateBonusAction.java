package projectPackage.controller.action;


import projectPackage.model.bonus.decorators.specialDecorator.SpecialDecorator;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.responseMessages.SuccessfulBonusActivationMessage;
import java.util.LinkedList;

public class ActivateBonusAction extends Action {

    private LinkedList<Object> list;

    public ActivateBonusAction(TurnMachine tm, LinkedList<Object> list) {
        super(tm);
        this.list = list;
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {

        SpecialDecorator specialBonus = (SpecialDecorator) tm.getGameBoard().getSpecialBonus().removeFirst();

        specialBonus.setParameters(list);

        specialBonus.activateBonus();

        return new ResultMsgPair(new SuccessfulBonusActivationMessage(), null);

    }


}
