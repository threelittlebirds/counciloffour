package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.model.utility.Pair;
import projectPackage.view.Token;

import java.util.LinkedList;

/**
 * Created by carme on 07/07/2016.
 */
public class SellFilledRequestMsg extends ActionRequestMsg {

    private LinkedList<Pair> permitToSell;
    private LinkedList<Pair> politicToSell;
    private Pair assintantToSell;

    public SellFilledRequestMsg(Token token, LinkedList<Pair> permitToSell, LinkedList<Pair> politicToSell, Pair assintantToSell) {
        super(token);
        this.permitToSell = permitToSell;
        this.politicToSell = politicToSell;
        this.assintantToSell = assintantToSell;
    }

    public LinkedList<Pair> getPermitToSell() {
        return permitToSell;
    }

    public LinkedList<Pair> getPoliticToSell() {
        return politicToSell;
    }

    public Pair getAssintantToSell() {
        return assintantToSell;
    }
    @Override
    public Action createAction(ActionFactoryVisitorImpl visitor) {
        return visitor.visit(this);
    }

}