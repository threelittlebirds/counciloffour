package projectPackage.view.messages.responseMessages;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by Med on 05/07/2016.
 */
public class SuccessfulBonusActivationMessage implements ResponseMsg {

    public SuccessfulBonusActivationMessage() {
    }

    @Override
    public void display(MessageVisitor visitor) {
        System.out.println("Bonus activated.\n");
    }
}
