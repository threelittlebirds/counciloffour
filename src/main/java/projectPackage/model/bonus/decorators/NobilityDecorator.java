package projectPackage.model.bonus.decorators;

import projectPackage.model.bonus.Bonus;

public class NobilityDecorator extends BonusDecorator {

    private int number;

    public NobilityDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
        tempBonus.setNobilityDecorator(true);
    }


    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        getGameBoard().moveOnNobilityTrack(number);
        System.out.println("Activating bonus NOBILITY+"+number+"\n");
    }

    @Override
    public String toStringDecorator() {
        return "NOBILITY +"+number;
    }
}
