package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class NextTurnAction extends Action {
    
	
	public NextTurnAction(TurnMachine tm) {
		super(tm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ResultMsgPair execute() throws CloneNotSupportedException {
		return tm.start(this);
	}

}
