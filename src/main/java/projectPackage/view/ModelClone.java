package projectPackage.view;

import java.util.*;

import projectPackage.model.*;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.utility.Pair;

public class ModelClone {
	
	private GameBoard gameBoard;

	public ModelClone() {
	}

	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}

	public ModelClone(GameBoard gameBoard) {
		super();
		this.gameBoard = gameBoard;
	}

	public LinkedList<PermitCard> getHandPermitDeck() {
		LinkedList<PermitCard> list = new LinkedList<>();
		for (PermitCard pc : gameBoard.getCurrPlayer().getPermitDeck()) {
			list.add(pc);
		}
		return (LinkedList<PermitCard>) list.clone();
	}

	public String currentPlayerSituation(){
		return gameBoard.getCurrPlayer().toString();
	}

	public LinkedList<PoliticCard> getHandPoliticDeck() {
		LinkedList<PoliticCard> list = new LinkedList<>();
		for (PoliticCard pc : gameBoard.getCurrPlayer().getPoliticDeck()) {
			list.add(pc);
		}
		return (LinkedList<PoliticCard>) list.clone();
	}


	public LinkedList<PermitCard> getGraveDeck() {
		LinkedList<PermitCard> list = new LinkedList<>();
		for (PermitCard pc : gameBoard.getCurrPlayer().getGraveDeck()) {
			list.add(pc);
		}
		return (LinkedList<PermitCard>) list.clone();
	}

	public LinkedList<City> getCitiesOfPlayerExceptNobilityDec() {
		LinkedList<City> list = (LinkedList<City>) gameBoard.getCitiesOfPlayer().clone();
        for (City c : list) {
			if (c.getCoin().getBonus().hasNobilityDecorator()) {
				list.remove(c);
			}
		}
		return list;
	}
	
    public boolean containsCity(char c){
    	return gameBoard.containsShortCity(c);
    }
    
	
	public ArrayList<PoliticCard> getReserveOfCouncillors() {
		return (ArrayList<PoliticCard>) gameBoard.getReserveOfCouncillors().clone();
	}
	
	public PermitCard getFirstRegionPermitCards(TypeRegion whichRegion) throws CloneNotSupportedException {
		return (PermitCard) gameBoard.getRegion(whichRegion).getFirstPermitCard().clone();
		
	}
	
	public LinkedList<City> getAllCities(){
		return (LinkedList<City>) gameBoard.getAllCities().clone();
	}
	
	public PermitCard getSecondRegionPermitCards(TypeRegion whichRegion) throws CloneNotSupportedException {
		return (PermitCard) gameBoard.getRegion(whichRegion).getSecondPermitCard().clone();
	}

	public LinkedList<PermitCard> getPrivatePermitCards(){
		return (LinkedList<PermitCard>) gameBoard.getCurrPlayer().getPermitDeck().getDeckCards().clone();
	}
	public LinkedList<Council> getCouncils() {
		LinkedList<Council> l = new LinkedList<>();
		l.add(new Council((LinkedList<Councillor>) gameBoard.getRegion(TypeRegion.HILLS).getRegionCouncil().getCouncillors().clone(), TypeCouncil.HILLS));
		l.add(new Council((LinkedList<Councillor>) gameBoard.getRegion(TypeRegion.COAST).getRegionCouncil().getCouncillors().clone(), TypeCouncil.COAST));
		l.add(new Council((LinkedList<Councillor>) gameBoard.getRegion(TypeRegion.MOUNTAINS).getRegionCouncil().getCouncillors().clone(), TypeCouncil.MOUNTAINS));
		l.add(new Council((LinkedList<Councillor>) gameBoard.getKingCouncil().getCouncillors().clone(), TypeCouncil.KING));
		return l;
	}

	public LinkedList<Council> getRegionCouncils() {
		LinkedList<Council> list = new LinkedList<>();
		list.add(new Council((LinkedList<Councillor>) gameBoard.getRegion(TypeRegion.HILLS).getRegionCouncil().getCouncillors().clone(), TypeCouncil.HILLS));
		list.add(new Council((LinkedList<Councillor>) gameBoard.getRegion(TypeRegion.COAST).getRegionCouncil().getCouncillors().clone(), TypeCouncil.COAST));
		list.add(new Council((LinkedList<Councillor>) gameBoard.getRegion(TypeRegion.MOUNTAINS).getRegionCouncil().getCouncillors().clone(), TypeCouncil.MOUNTAINS));
		return list;
	}

	public String playerSituation(){
		return gameBoard.getCurrPlayer().toString();
	}

	public int numAssistant(){return gameBoard.getCurrPlayer().getNumAssistant();}

	public HashMap<PoliticCard, Integer> getPoliticCardInSelling(){
		return (HashMap<PoliticCard, Integer>) gameBoard.getPoliticToBuy().clone();
	}
	public HashMap<PermitCard, Integer> getPermitCardInSelling(){
		return (HashMap<PermitCard, Integer>) gameBoard.getPermitToBuy().clone();
	}
	public LinkedList<Pair> getAssistantInSelling(){
		return (LinkedList<Pair>)gameBoard.getAssistantToBuy().clone();
	}

	public int getPlayerHash(){
		return gameBoard.getCurrPlayer().hashCode();
	}
	public Region getRegion(int choice) {
		return gameBoard.getRegion(choice);
	}
}




