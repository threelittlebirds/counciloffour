package projectPackage.model.utility;



import projectPackage.model.City;
import projectPackage.model.bonus.containers.ColorCard;
import projectPackage.model.cards.Deck;
import projectPackage.model.cards.MultiColor;
import projectPackage.model.cards.PoliticCard;

import java.awt.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

/**
 * Created by carme on 15/06/2016.
 */
public final class Util implements Serializable {
    private static Map<Color, String> colorMap = new HashMap<>();
    private static final String black = "Black";
    private static final String yellow = "Yellow";
    private static final String blue = "Blue";
    private static final String gray = "Gray";
    private static final String orange = "Orange";
    private static final String magenta = "Magenta";
    private static final String red = "Red";
    private static final String white = "White";
    private static final String pink = "Pink";

    //questo metodo genera un numero casuale tra min e max
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static String getStringFromColor(Color col) {
        colorMap.put(Color.BLACK, black);
        colorMap.put(Color.YELLOW, yellow);
        colorMap.put(Color.BLUE, blue);
        colorMap.put(Color.GRAY, gray);
        colorMap.put(Color.ORANGE, orange);
        colorMap.put(Color.MAGENTA, magenta);
        colorMap.put(Color.RED, red);
        colorMap.put(Color.WHITE, white);
        colorMap.put(Color.PINK, pink);
        if(col instanceof MultiColor) return "Multicolor";
        for (Color c : colorMap.keySet()) {
            if (col == c) return colorMap.get(c);
        }
        return "illegal";
    }

    public static Color getColorFromString(String s) {
        colorMap.put(Color.BLACK, black);
        colorMap.put(Color.YELLOW, yellow);
        colorMap.put(Color.BLUE, blue);
        colorMap.put(Color.GRAY, gray);
        colorMap.put(Color.ORANGE, orange);
        colorMap.put(Color.MAGENTA, magenta);
        colorMap.put(Color.RED, red);
        colorMap.put(Color.WHITE, white);
        colorMap.put(Color.PINK, pink);
        for (Color c : colorMap.keySet()) {
            if (s.equals(colorMap.get(c))) return c;
        }
        //throw new NullPointerException("Colore non trovato");
        return Color.BLACK;
    }

    /**
     * This method calculate the min path from source City
     * to destination City in a Graph of City
     *
     * @param map graph composed by cities
     * @param source departure city
     * @param destination city of arrival
     *
     * @return int min number of street to go from source city to destination city
     */

    public static int minPathOfGraph(HashMap<City,LinkedList<City>> map, City source, City destination){
        if(!map.keySet().contains(destination)) throw new IllegalArgumentException();
        if(source.equals(destination)) return 0;
        if(map.get(source).contains(destination)) return 1;
        LinkedList<City> coda = new LinkedList<>();
        LinkedList<City> contaLivelliVisitati = new LinkedList<>();
        HashMap<City,Boolean> visitati = new HashMap<>();
        for(City c : map.keySet()){
            visitati.put(c, false);
        }
        coda.addLast(source);
        contaLivelliVisitati.addAll(map.get(source));
        visitati.put(source, true);
        int minDistanza = 0;
        boolean flag1 = false;
        while(!coda.isEmpty()){
            City car = coda.removeFirst();
            LinkedList<City> adiacenti = map.get(car);
            for(City c : adiacenti){
                if(!visitati.get(c)){
                    if(c.equals(destination)) {
                        flag1 = true;
                        break;
                    }
                    visitati.put(c, true);
                    coda.addLast(c);
                }
            }
            boolean flag2 = true;
            for(City c : contaLivelliVisitati){
                if(!visitati.get(c)) flag2 = false;
            }
            if(flag2){
                minDistanza++;
                LinkedList<City> support = new LinkedList<>();
                for(City c1 : contaLivelliVisitati){
                    map.get(c1).stream().filter(c2 -> !support.contains(c2)).forEach(support::add);
                }
                contaLivelliVisitati.addAll(support);
            }
            if(flag1){
                minDistanza++;
                break;
            }
        }
        return minDistanza;
    }
   
}
