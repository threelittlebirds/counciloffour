package projectPackage.view.messages.requestMessages;

import projectPackage.view.Token;

/**
 * A message ot request the public log of a game.
 */
public class PublicLogRequestMsg extends RequestMessage {

    /**
     * Create a new PublicLogRequestMsg request message.
     *
     * @param token the token that identifies the client
     */
    public PublicLogRequestMsg(Token token) {

        super(token);
    }

}
