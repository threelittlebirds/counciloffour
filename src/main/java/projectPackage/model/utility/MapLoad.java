package projectPackage.model.utility;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import projectPackage.model.*;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.*;
import projectPackage.model.bonus.decorators.*;
import projectPackage.model.bonus.decorators.specialDecorator.*;
import projectPackage.model.cards.Deck;
import projectPackage.model.cards.MultiColor;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.turn.TurnMachine;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static projectPackage.model.TypeRegion.COAST;
import static projectPackage.model.TypeRegion.HILLS;
import static projectPackage.model.TypeRegion.MOUNTAINS;

/**
 * Created by carme on 16/06/2016.
 */
public class MapLoad implements Serializable {
    private static final Logger LOG = Logger.getLogger(MapLoad.class.getName());
    private final Color black = Color.BLACK;
    private final Color blue = Color.BLUE;
    private final Color orange = Color.ORANGE;
    private final Color magenta = Color.MAGENTA;
    private final Color pink = Color.PINK;
    private final Color white = Color.WHITE;
    private String mapSelected;
    private TurnMachine tm;

    public MapLoad(TurnMachine tm, String mapSelected) {

        this.tm = tm;
        this.mapSelected=mapSelected;
    }


    public HashMap<City, LinkedList<City>> getMap() {

        LinkedList<Character> allCharCoastCities = new LinkedList<>();
        LinkedList<Character> allCharHillsCities = new LinkedList<>();
        LinkedList<Character> allCharMountainsCities = new LinkedList<>();
        LinkedList<Color> citiesColor = new LinkedList<>();
        HashMap<City, LinkedList<City>> mappa = new HashMap<>();
        try {
            //ClassLoader classLoader = getClass().getClassLoader();
            //File inputFile = new File(classLoader.getResource(mapSelected).getFile());
            File inputFile = new File("src\\main\\java\\projectPackage\\map\\" + mapSelected);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            //Cities
            NodeList cityNodeList = doc.getElementsByTagName("City");        //Leggo tutte le città
            for (int temp = 0; temp < cityNodeList.getLength(); temp++) {    //Ogni item "City"
                Node nNode = cityNodeList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name1 = eElement.getAttribute("Name");
                    String coloreinteoria = eElement.getAttribute("Color");
                    String typeBonus = eElement.getAttribute("Coin");
                    StringTokenizer ts = new StringTokenizer(typeBonus, ", ");
                    Color color1 = Util.getColorFromString(coloreinteoria);
                    TypeRegion type1 = TypeRegion.valueOf(eElement.getAttribute("TypeRegion"));
                    City tempCity = new City(name1, color1, type1);
                    citiesColor.add(tempCity.getCityColor());
                    Bonus b = bonusGenerator(new StringTokenizer(reOrderBonus(ts), ", "));
                    tempCity.setCoin(new Coin(b));
                    mappa.put(tempCity, null);
                    switch (tempCity.getTypeRegion()) {
                        case COAST: allCharCoastCities.add(tempCity.getShortName());
                        case HILLS: allCharHillsCities.add(tempCity.getShortName());
                        case MOUNTAINS: allCharMountainsCities.add(tempCity.getShortName());
                    }
                }
            }
            //AdiacentCities
            for (int temp = 0; temp < cityNodeList.getLength(); temp++) { //Ogni nodo "City"
                Node nNode = cityNodeList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name1 = eElement.getAttribute("Name");
                    City city = null;
                    for (City c : mappa.keySet()) {
                        if (c.getName().equals(name1)) {
                            city = c;
                            break;
                        }
                    }
                    NodeList adiacent = eElement.getElementsByTagName("AdiacentCity");
                    LinkedList<City> adiacentCity = new LinkedList<>();
                    for (int temp3 = 0; temp3 < adiacent.getLength(); temp3++) //ogni nodo "AdiacentCity"
                    {
                        Node adiacentNode = adiacent.item(temp3);
                        if (adiacentNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element aElement = (Element) adiacentNode;
                            String name2 = aElement.getAttribute("Name");
                            for (City c : mappa.keySet())
                                if (c.getName().equals(name2)) {
                                    adiacentCity.add(c);
                                    break;
                                }
                        }
                    }
                    mappa.put(city, adiacentCity);
                }
            }
            //Councils
            Region [] regions = tm.getGameBoard().getRegions();
            LinkedList<TypeRegion> l1 = new LinkedList<>();
            l1.addLast(COAST);
            l1.addLast(HILLS);
            l1.addLast(MOUNTAINS);
            LinkedList<TypeCouncil> l2 = new LinkedList<>();
            l2.addLast(TypeCouncil.COAST);
            l2.addLast(TypeCouncil.HILLS);
            l2.addLast(TypeCouncil.MOUNTAINS);
            for (int i = 0; i < 3; i++){
                LinkedList<Councillor> councillors = new LinkedList<>();
                for(int j=0;j<4;j++){
                    int num = Util.randInt(1,6);
                    switch (num){
                        case 1: councillors.add(new Councillor(black)); break;
                        case 2: councillors.add(new Councillor(blue)); break;
                        case 3: councillors.add(new Councillor(orange)); break;
                        case 4:councillors.add(new Councillor(magenta)); break;
                        case 5: councillors.add(new Councillor(pink)); break;
                        default: councillors.add(new Councillor(white)); break;
                    }
                }
                Council council = new Council(councillors,l2.removeFirst());
                regions[i] = new Region(l1.removeFirst(),council);
            }
            //KingCouncil
            LinkedList<Councillor> kingCouncillors = new LinkedList<>();
            for(int j=0;j<4;j++){
                int num = Util.randInt(1,6);
                switch (num){
                    case 1: kingCouncillors.add(new Councillor(black)); break;
                    case 2: kingCouncillors.add(new Councillor(blue)); break;
                    case 3: kingCouncillors.add(new Councillor(orange)); break;
                    case 4: kingCouncillors.add(new Councillor(magenta)); break;
                    case 5: kingCouncillors.add(new Councillor(pink)); break;
                    default: kingCouncillors.add(new Councillor(white)); break;
                }
            }
            Council kingCouncil = new Council(kingCouncillors, TypeCouncil.KING);
            tm.getGameBoard().setKingCouncil(kingCouncil);
            //ReserveOfCouncillors
            for(int i=0; i<8; i++) {
                int rand = Util.randInt(1, 6);
                switch (rand){
                    case 1: tm.getGameBoard().addCouncillorToReserve(new Councillor(black)); break;
                    case 2: tm.getGameBoard().addCouncillorToReserve(new Councillor(blue)); break;
                    case 3: tm.getGameBoard().addCouncillorToReserve(new Councillor(orange)); break;
                    case 4: tm.getGameBoard().addCouncillorToReserve(new Councillor(magenta)); break;
                    case 5: tm.getGameBoard().addCouncillorToReserve(new Councillor(pink)); break;
                    default: tm.getGameBoard().addCouncillorToReserve(new Councillor(white)); break;
                }
            }
            //NobilityTrack
            NodeList nobilityTrackNode = doc.getElementsByTagName("NobilityTrack");
            Node sNode = nobilityTrackNode.item(0);
            if (sNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) sNode;
                NodeList squareNode= eElement.getElementsByTagName("Square");
                for(int tempp=0; tempp<squareNode.getLength(); tempp++){
                    Node square = squareNode.item(tempp);
                    if(square.getNodeType() == Node.ELEMENT_NODE){
                        Element sElement = (Element) square;
                        String position = sElement.getAttribute("Position");
                        StringTokenizer bonus = new StringTokenizer(sElement.getAttribute("Bonus"), ", ");
                        Bonus b = bonusGenerator(new StringTokenizer(reOrderBonus(bonus), ", "));
                        tm.getGameBoard().getNobilityTrack().getSquare(Integer.parseInt(position)).setBonus(b);
                    }
                }
            }

            //ColorCard:
            LinkedList<Color> colors = orderColorList(citiesColor);  //in colors abbiamo i colori ordinati in base alle volte in cui compaiono nella mappa
            Color king = colors.removeLast();

            for(City c : mappa.keySet()){
                if(c.getCityColor().equals(king)){
                    tm.getGameBoard().setKing(new King(c.getShortName()));
                    break;
                }
            }


            LinkedList<Bonus> bonuses = new LinkedList<>();
            bonuses.addLast(new VictoryDecorator(new BlankContainer(tm), 20));
            bonuses.addLast(new VictoryDecorator(new BlankContainer(tm), 12));
            bonuses.addLast(new VictoryDecorator(new BlankContainer(tm), 8));
            bonuses.addLast(new VictoryDecorator(new BlankContainer(tm), 5));
            for(int i=0;i<4;i++){
                tm.getGameBoard().getColorCards().add(new ColorCard(bonuses.get(i),colors.get(i)));
            }

            //KingCard
            LinkedList<Bonus> bonuses2 = new LinkedList<>();
            bonuses2.addLast(new VictoryDecorator(new BlankContainer(tm), 25));
            bonuses2.addLast(new VictoryDecorator(new BlankContainer(tm), 18));
            bonuses2.addLast(new VictoryDecorator(new BlankContainer(tm), 12));
            bonuses2.addLast(new VictoryDecorator(new BlankContainer(tm), 7));
            bonuses2.addLast(new VictoryDecorator(new BlankContainer(tm), 3));
            for(int i=0;i<5;i++){
                tm.getGameBoard().getKingCards().add(new KingCard(bonuses2.get(i)));
            }

            //PoliticCards
            for(int i=0; i<3*tm.getGameBoard().getNumPlayers(); i++){
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(new MultiColor(200)));
            }
            for(int i=0; i<4*tm.getGameBoard().getNumPlayers(); i++){
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(white));
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(orange));
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(black));
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(pink));
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(blue));
                tm.getGameBoard().getReserveOfPoliticCards().add(new PoliticCard(magenta));
            }
            tm.getGameBoard().getReserveOfPoliticCards().shuffle();

            //Region
            NodeList regionNodeList = doc.getElementsByTagName("Region");       //Per ogni regione
            for (int temp = 0; temp < regionNodeList.getLength(); temp++) {
                Node nNode = regionNodeList.item(temp);
                Deck<PermitCard> tempDeck = new Deck<>();
                if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String victoryQuantity = eElement.getAttribute("VictoryQuantity");
                    String permitQuantity = eElement.getAttribute("PermitQuantity");
                    regions[temp].setRegionPrize(new RegionCard(new VictoryDecorator(new BlankContainer(tm), Integer.parseInt(victoryQuantity.trim()))));
                    NodeList permitCardNodeList = eElement.getElementsByTagName("PermitCard");
                    for(int cont=0; cont<permitCardNodeList.getLength(); cont++){       //Per ogni nodo Permit
                        Node mNode = permitCardNodeList.item(cont);
                        if(mNode.getNodeType() == Node.ELEMENT_NODE){
                            Element bElement = (Element) mNode;
                            String whichBonus = bElement.getAttribute("Bonus");
                            StringTokenizer ts = new StringTokenizer(whichBonus, ", ");
                            Bonus b = bonusGenerator(ts);
                            int num1 = Util.randInt(1, 3);
                            char[] toPermit = new char[num1];
                            LinkedList<Character> allCharCoastCitiesClone = (LinkedList<Character>) allCharCoastCities.clone();
                            LinkedList<Character> allCharHillsCitiesClone = (LinkedList<Character>) allCharHillsCities.clone();
                            LinkedList<Character> allCharMountainsCitiesClone = (LinkedList<Character>) allCharMountainsCities.clone();
                            for(int i = 0;i<toPermit.length;i++){
                                int num2;
                                switch (regions[temp].getType()) {
                                    case COAST:
                                        num2 = Util.randInt(0,allCharCoastCities.size()-1-i);
                                        toPermit[i] = allCharCoastCitiesClone.remove(num2);     //TODO: Controllare che tutti i caratteri siano presenti nelle permit card
                                        break;
                                    case HILLS:
                                        num2 = Util.randInt(0,allCharHillsCities.size()-1-i);
                                        toPermit[i] = allCharHillsCitiesClone.remove(num2);
                                        break;
                                    default:
                                        num2 = Util.randInt(0,allCharMountainsCities.size()-1-i);
                                        toPermit[i] = allCharMountainsCitiesClone.remove(num2);
                                        break;
                                }
                            }
                            PermitCard p = new PermitCard(b,toPermit);
                            tempDeck.add(p);
                        }
                    }
                    regions[temp].setRegionDeck(tempDeck);
                }
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            LOG.log(Level.SEVERE, "Exception in MapLoad", e);
        }
        return mappa;
    }
    private Bonus bonusGenerator(StringTokenizer st){
        String quantity;
        Bonus b = new BlankContainer(tm);
        while (st.hasMoreTokens()) {
            switch (st.nextToken()) {
                case "Money":
                    quantity = st.nextToken();
                    b = new MoneyDecorator(b, Integer.parseInt(quantity));
                    break;
                case "Victory":
                    quantity = st.nextToken();
                    b = new VictoryDecorator(b, Integer.parseInt(quantity));
                    break;
                case "Assistant":
                    quantity = st.nextToken();
                    b = new AssistantDecorator(b, Integer.parseInt(quantity));
                    break;
                case "Nobility":
                    quantity = st.nextToken();
                    b = new NobilityDecorator(b, Integer.parseInt(quantity));
                    break;
                case "Politic":
                    quantity = st.nextToken();
                    b = new PoliticDecorator(b, Integer.parseInt(quantity));
                    break;
                case "DoubleCoin":
                    b = new DoubleCoinDecorator(b);
                    break;
                case "HDP":
                    b = new HandPermitCardDecorator(b);
                    break;
                case "Coin":
                    b = new CoinDecorator(b);
                    break;
                case "Action":
                    b = new ActionDecorator(b);
                    break;
                default:
                    b = new PermitCardDecorator(b);
                    break;
            }
        }
        return b;
    }

    private LinkedList<Color> orderColorList(LinkedList<Color> colors){
        HashMap<Color, Integer> map = new HashMap<>();
        for(Color c : colors){
            if(map.keySet().contains(c)){
                Integer temp = map.get(c) + 1;
                map.put(c, temp);
            }else{
                map.put(c, 0);
            }
        }

        //Integer num = new Integer(0);
        LinkedList<Color> temp = new LinkedList<>();
        temp.addAll(map.keySet());
        LinkedList<Color> toReturn = new LinkedList<>();
        while(!map.keySet().isEmpty()){
            Color max = temp.get(0);
            for(Color t : map.keySet()){
                if(map.get(t)>map.get(max)) max=t;
            }
            toReturn.addLast(max);
            map.remove(max);
            temp.remove(max);
        }
        return toReturn;
    }

    private String reOrderBonus(StringTokenizer st){
        StringBuilder specialBonus = new StringBuilder();
        StringBuilder normalBonus = new StringBuilder();
        StringBuilder actionBonus = new StringBuilder();
        while (st.hasMoreTokens()) {
            String type = st.nextToken();
            if(type.equals("DoubleCoin") || type.equals("HDP") || type.equals("Coin") || type.equals("Permit")){
                specialBonus.append(type);
                if(st.hasMoreTokens()){
                    specialBonus.append(", ");
                }
            }
            if(type.equals("Action")){
                actionBonus.append(type);
                if(st.hasMoreTokens()){
                    actionBonus.append(", ");
                }
            }
            if(type.equals("Money") || type.equals("Victory") || type.equals("Assistant") ||
                    type.equals("Nobility") || type.equals("Politic")){
                normalBonus.append(type);
                normalBonus.append(" ");
                normalBonus.append(st.nextToken());
                if(st.hasMoreTokens()) {
                    normalBonus.append(", ");
                }
            }
        }
        return normalBonus.append(specialBonus.append(actionBonus)).toString();
    }

}