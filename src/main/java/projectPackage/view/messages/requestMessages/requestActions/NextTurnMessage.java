package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class NextTurnMessage extends ActionRequestMsg {

	public NextTurnMessage(Token token) {
		super(token);
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
