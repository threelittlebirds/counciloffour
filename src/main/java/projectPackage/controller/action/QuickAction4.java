package projectPackage.controller.action;

import projectPackage.controller.exceptions.NotEnoughAssistants;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class QuickAction4 extends Action {

    public QuickAction4(TurnMachine tm) {
        super(tm);
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
        if(!enoughAssistant()) throw new NotEnoughAssistants();
        return tm.start(this);
    }

    private boolean enoughAssistant(){
        return tm.getGameBoard().getCurrPlayer().getNumAssistant() >= 3;
    }
}
