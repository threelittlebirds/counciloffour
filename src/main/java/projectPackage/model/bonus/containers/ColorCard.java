package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;
import projectPackage.model.utility.Util;

import java.awt.*;


public class ColorCard extends BonusContainer implements Card {

    private Color color;

    public ColorCard(Bonus bonus, Color color) {
        super(bonus);
        this.color = color;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("ColorCard,");
        super.activateBonus();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }



}
