package projectPackage.model.bonus.decorators.specialDecorator;


import projectPackage.model.bonus.Bonus;

import java.util.LinkedList;

public class DoubleCoinDecorator extends SpecialDecorator {

    public DoubleCoinDecorator(Bonus newBonus) {
        super(newBonus);
    }


    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        String city1;
        String city2;

        if (parameter instanceof LinkedList) {
            city1 = (String) ((LinkedList) parameter).get(0);
            city2 = (String) ((LinkedList) parameter).get(1);

            Bonus bonus1 = getGameBoard().getCity(city1.charAt(0)).getCoin().getBonus();
            Bonus bonus2 = getGameBoard().getCity(city2.charAt(0)).getCoin().getBonus();

            System.out.println("Activating bonus DOUBLE COIN BONUS\n");

            bonus1.activateBonus();
            bonus2.activateBonus();
        }


    }

    @Override
    public String toStringDecorator() {
        return "Obtain the coin of 2 cities\n";
    }

}
