package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class MainAction4Message extends ActionRequestMsg {
	
	private char whichCity;

	public MainAction4Message(Token token, char whichCity) {

		super(token);
		this.whichCity = whichCity;
	}

	public char getWhichCity() {
		return whichCity;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
