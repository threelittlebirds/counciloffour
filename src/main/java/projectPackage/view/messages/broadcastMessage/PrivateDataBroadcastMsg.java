package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

/**
 * Created by Med on 03/07/2016.
 */
public class PrivateDataBroadcastMsg implements BroadcastMessage {

    private TextTable currentSituation;

    private int player;

    public PrivateDataBroadcastMsg(TextTable currentSituation, int player) {
        this.currentSituation = currentSituation;
        this.player = player;
    }

    @Override
    public void display(MessageVisitor visitor) {
        //visitor.display(this);
    }
}
