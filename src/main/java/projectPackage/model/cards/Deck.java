package projectPackage.model.cards;


import projectPackage.model.utility.table.TextTable;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * This class models a Deck of {@link Card}.
 * Thank to this class it is possible to draw cards, shuffle the deck,
 * get the card you want, etc. When there are no more cards in a
 * deck the discard pile (if not empty) is automatically shuffled and used.
 * <p>
 * This class is generic and this ables to use it with any type of
 * Object that implements the Interface Card.
 *
 * @param <C> the element type
 */

public class Deck <C extends Card> implements Iterable<C>, Serializable {

    private LinkedList<C> deckCards;
    
    public Deck(){
    	deckCards = new LinkedList<>();
    }
    
    public boolean isEmpty(){
    	return deckCards.isEmpty();
    }

    public void shuffle() {
    	Collections.shuffle(deckCards);
    }
    
    public void add(C c){
    	deckCards.add(c);
    }

	public void add(LinkedList<C> list){
		deckCards.addAll(list);
	}
    
	public LinkedList<C> getDeckCards() {
		return deckCards;
	}

	void setDeckCards(LinkedList<C> deckCards) {
		this.deckCards = deckCards;
	}

	public C get(int index){
		return deckCards.get(index);
	}
	
	public C getCard(int position) {
		return deckCards.get(position);
	}
	
	public C getFirst(){
		return deckCards.getFirst();
	}
	
	C getLast(){
		return deckCards.getLast();
	}
	
	public C removeFirst(){
		return deckCards.removeFirst();
	}
	
	public C removeLast(){
		return deckCards.removeLast();
	}

	public C removeCard(int position) {
		return deckCards.remove(position);
	}

	public void addLast(C card){deckCards.addLast(card);}

    public void clear() {
        deckCards.clear();
    }

	public String toString(){
		String[] columnNames = new String [deckCards.size()];
		for(int i=0;i<columnNames.length;i++){
			columnNames[i] = "Permit card "+i;
		}
		Object[][] data = new Object [1][deckCards.size()];
		int i = 0;
		for(C c : deckCards){
			data[0][i] = c;
			i++;
		}
		TextTable tt = new TextTable(columnNames, data); // this adds the numbering on the left
		//tt.setAddRowNumbering(true); // sort by the first column
		//tt.setSort(0);
		tt.printTable();
		return "";
	}

	public Iterator<C> iterator(){
		return deckCards.iterator();
	}

	public int size() {
		return deckCards.size();
	}
}
