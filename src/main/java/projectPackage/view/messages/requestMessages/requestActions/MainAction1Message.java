package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class MainAction1Message extends ActionRequestMsg {
	
	private int whichCouncillor;
	private int whichCouncil;

	public MainAction1Message(Token token, int whichCouncillor, int whichCouncil) {
		super(token);
		this.whichCouncillor = whichCouncillor;
		this.whichCouncil = whichCouncil;
	}

	public int getWhichCouncillor() {
		return whichCouncillor;
	}

	public int getWhichCouncil() {
		return whichCouncil;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
