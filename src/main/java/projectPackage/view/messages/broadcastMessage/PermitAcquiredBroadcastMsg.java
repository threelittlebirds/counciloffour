package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by Carmelo on 27/06/2016.
 */
public class PermitAcquiredBroadcastMsg implements BroadcastMessage {

    private int player;

    public PermitAcquiredBroadcastMsg(int player) {
        this.player = player;
    }

    public int getPlayer() {
        return player;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
