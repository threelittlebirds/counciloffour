package projectPackage.model.track.square;

public class MoneySquare implements Square {

	private int position;

	public MoneySquare(int position) {
		super();
		this.position = position;
	}

	public int getPosition() {
		return position;
	}
	
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
	public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("* "+position+" *\n");
    	sb.append("*       *\n");
    	sb.append("*********\n");
    	return sb.toString();
	}

}
