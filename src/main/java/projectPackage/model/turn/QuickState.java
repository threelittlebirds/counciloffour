package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.model.GameBoard;
import projectPackage.model.Player;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.*;
import projectPackage.view.messages.responseMessages.responseActions.AskMainActionResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.GoToEndMessage;


public class QuickState extends TurnState {

	private String currPlayer = tm.getGameBoard().getCurrPlayer().getName();

	private GameBoard gameBoard;

    public QuickState(TurnMachine tm) {
		super(tm);
		gameBoard = tm.getGameBoard();
    }

	@Override
	public void start(Action a) {
		if(a instanceof EndAction) {
			tm.setJumpQuickAction(true);
		}
		else {
			tm.setQuickExecuted(true);
			if(a instanceof QuickAction1){
				quickAction1(a);
				broadcastMessage = createBroadcastMsg(1, false);
			}
			if(a instanceof QuickAction2){
				quickAction2(a);
				broadcastMessage = createBroadcastMsg(2, false);
			}
			if(a instanceof QuickAction3){
				quickAction3(a);
				broadcastMessage = createBroadcastMsg(3, false);
			}
			if(a instanceof QuickAction4){
				quickAction4(a);
				broadcastMessage = createBroadcastMsg(4, false);
			}
		}
		System.out.println(tm.getGameBoard().getCurrPlayer());

	}

	@Override
	public ResultMsgPair nextState() throws CloneNotSupportedException {
		if (tm.isQuick4() || !tm.isMainExecuted()) {
			tm.setCurrentState(new MainState(tm));
			responseMsg = new AskMainActionResponseMsg(tm.getGameBoard().printSideCouncillors(),tm.getGameBoard().printAllCouncils(),tm.getGameBoard().printRegionCouncils(),tm.getGameBoard().printPermitCardsOfRegions(),tm.getGameBoard().printPersonalPermitCards(),tm.getGameBoard().getCurrPlayer().getPermitDeck(),tm.getGameBoard().printBoard(),tm.getGameBoard().getNameOfCities());
		}else{
            tm.setCurrentState(new EndState(tm));
            return tm.start(new EndAction(tm));
        }
        /*
		else if(tm.getCounterMarket() == tm.getGameBoard().getNumPlayers()){
			tm.setCurrentState(new MarketState(tm));
			tm.getGameBoard().setCurrPlayer(tm.nextPlayer());
			broadcastMessage = new MarketBroadcastMsg(gameBoard.indexOfCurrentPlayer(), gameBoard.nextPlayer().getToken().getPlayerNumber(), gameBoard.printSeparatePermitToSell(),gameBoard.printSeparatePoliticToSell(),gameBoard.printAssistantsToSell(),gameBoard.getCurrPlayer().getNumAssistant());
			responseMsg = new GoToEndMessage(gameBoard.getCurrPlayer().getName());
		}
		*/
		resultMsgPair = new ResultMsgPair(responseMsg, broadcastMessage);

		return resultMsgPair;
	}

	private void quickAction1(Action a) {
		Player player = tm.getGameBoard().getCurrPlayer();
		player.acquireAssistant(1);
		tm.getGameBoard().moveOnMoneyTrack(-3);
		System.out.println("Quick Action 1 done");
	}

    private void quickAction2(Action a) {
		QuickAction2 quickAction2 = (QuickAction2) a;
		tm.getGameBoard().getCurrPlayer().payAssistant(1);
		int choice = quickAction2.getWhichRegion();
		tm.getGameBoard().getRegion(choice).replaceCards();
		System.out.println("Quick Action 2 done");
	}

	private void quickAction3(Action a) {
		tm.getGameBoard().getCurrPlayer().payAssistant(1);
		int region = ((QuickAction3) a).getWhichRegion();
		int councillor = ((QuickAction3) a).getWhichCouncillor();
		tm.getGameBoard().corruptCouncil(region, councillor);
		System.out.println("Quick Action 3 done");
	}

	private void quickAction4(Action a) {
		tm.setQuick4(true);
	}

	private BroadcastMessage createBroadcastMsg(int witchQuick, boolean isEnd) {
		return new QuickActionBroadcastMsg(gameBoard.indexOfCurrentPlayer(), currPlayer + " chose Quick Action " + witchQuick + "!", isEnd);
	}

}

