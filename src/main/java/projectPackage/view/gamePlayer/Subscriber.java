package projectPackage.view.gamePlayer;

import projectPackage.sharedInterface.ISubscriber;
import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.broadcastMessage.GameFinishedBroadcastMsg;

import java.rmi.RemoteException;

/**
 * The SubscriberInterface implementation that will be exposed to the broker to publish
 * message to the client.
 */
class Subscriber implements ISubscriber {

    private ViewUpdater viewUpdater;

    private boolean gameFinished = false;

    /**
     * Create a new subscriber "handler".
     *
     * @param viewUpdater the view updater to display received broadcast messages
     */
    Subscriber(ViewUpdater viewUpdater) {

        this.viewUpdater = viewUpdater;

    }

    @Override
    public void sendMessage(BroadcastMessage message) throws RemoteException {

        viewUpdater.update(message);

        if (message instanceof GameFinishedBroadcastMsg) gameFinished = true;

    }

    public boolean isGameFinished() {
        return gameFinished;
    }

}
