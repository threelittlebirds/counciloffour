package projectPackage.model.bonus;

import projectPackage.model.GameBoard;
import projectPackage.model.turn.TurnMachine;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public interface Bonus extends Serializable{

    void activateBonus() throws CloneNotSupportedException;

    GameBoard getGameBoard();

    TurnMachine getTurnMachine();

    boolean hasNobilityDecorator();

    void setNobilityDecorator(boolean value);

    int getNumberOfBonuses();

    LinkedList<String> getSeparatedBonuses();

}
