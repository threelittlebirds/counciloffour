package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.util.LinkedList;


/**
 * Created by Carmelo on 19/06/2016.
 */
public class AskToSellMessage implements ResponseMsg {

    private int moneyPosition;
    private LinkedList<TextTable> separatePermitCards;
    private LinkedList<TextTable> separatePoliticCards;
    private TextTable assistants;
    private int numAssistants;

    public AskToSellMessage(LinkedList<TextTable> separatePermitCards, LinkedList<TextTable> separatePoliticCards, TextTable assistants, int numAssistants, int moneyPosition) {
        this.moneyPosition = moneyPosition;
        this.separatePermitCards = separatePermitCards;
        this.separatePoliticCards = separatePoliticCards;
        this.assistants = assistants;
        this.numAssistants = numAssistants;
    }

    public int getMoneyPosition() {
        return moneyPosition;
    }

    public int getNumAssistants() {
        return numAssistants;
    }

    public LinkedList<TextTable> getSeparatePermitCards() {
        return separatePermitCards;
    }

    public LinkedList<TextTable> getSeparatePoliticCards() {
        return separatePoliticCards;
    }

    public TextTable getAssistants() {
        return assistants;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
