package projectPackage.model.bonus.decorators.specialDecorator;


import projectPackage.model.Region;
import projectPackage.model.TypeRegion;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.PermitCard;

public class PermitCardDecorator extends SpecialDecorator {

    public PermitCardDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {

        Region region;
        PermitCard pc;
        int choice = (int) parameter;
        if (choice == 1 || choice == 2) {
            region = getGameBoard().getRegion(TypeRegion.COAST);
        }
        else if (choice == 3 || choice == 4) {
            region = getGameBoard().getRegion(TypeRegion.HILLS);
        }
        else {
            region = getGameBoard().getRegion(TypeRegion.MOUNTAINS);
        }

        int pos = choice%2;
        if (pos ==1) pc = region.getFirstPermitCard();
        else pc = region.getSecondPermitCard();

        System.out.println("Activating bonus ON GAME PERMIT BONUS\n");

        pc.activateBonus();

    }

    @Override
    public String toStringDecorator() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("Activate a face-up Permit Card on the gameboard.\n");
        return sb.toString();
    }
}
