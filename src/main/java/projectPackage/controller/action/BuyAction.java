package projectPackage.controller.action;

import projectPackage.controller.exceptions.NotEnoughMoneyForBuy;
import projectPackage.model.turn.TurnMachine;
import projectPackage.model.utility.Pair;
import projectPackage.view.Token;
import projectPackage.view.messages.ResultMsgPair;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Carmelo on 19/06/2016.
 */
public class BuyAction extends Action {

    private HashMap<Token,LinkedList<LinkedList<Pair>>> staffToBuy;

    public BuyAction(HashMap<Token, LinkedList<LinkedList<Pair>>> staffToBuy, TurnMachine tm) {
        super(tm);
        this.staffToBuy = staffToBuy;
    }



    public HashMap<Token, LinkedList<LinkedList<Pair>>> getStaffToBuy() {
        return staffToBuy;
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
        if(!enougthMoneyForBuy()) throw new NotEnoughMoneyForBuy();
        return tm.start(this);
    }

    private boolean enougthMoneyForBuy(){
        int totalPrize = 0;
        for(Token t : staffToBuy.keySet()){
            for(Pair pair : staffToBuy.get(t).get(0)){
                totalPrize = totalPrize + pair.getPrice();
            }
            for(Pair pair : staffToBuy.get(t).get(1)){
                totalPrize = totalPrize + pair.getPrice();
            }
            Pair pair = staffToBuy.get(t).get(2).getFirst();
            totalPrize = totalPrize + pair.getPrice();
        }
        return tm.getGameBoard().getCurrPlayer().getMoneyPosition().getPosition() >= totalPrize;
    }
}
