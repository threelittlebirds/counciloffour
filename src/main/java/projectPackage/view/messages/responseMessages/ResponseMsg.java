package projectPackage.view.messages.responseMessages;

import projectPackage.view.messages.MessageVisitor;

import java.io.Serializable;

/**
 * This interface represents a Response message from the server to the client,
 * subsequent to a client request.
 */
public interface ResponseMsg extends Serializable {

	/**
	 * Visit this message to display the result.
	 *
	 * @param visitor the visitor
	 */
	void display(MessageVisitor visitor);

}
