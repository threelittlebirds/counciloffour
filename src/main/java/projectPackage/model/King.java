package projectPackage.model;


import java.io.Serializable;

public class King implements Serializable{
	
	private char city;
	
	public King(char c){
		this.city = c;
	}
	
	public char getPosition(){
		return city;
	}
	
	public void setPosition(char c){
		this.city=c;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("***************\n");
    	sb.append("*   "+city+"  *\n");
    	sb.append("***************\n");
    	return sb.toString();
	}
	
}
