package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by carme on 29/06/2016.
 */
public class GeneralInfoBroadcastMsg implements BroadcastMessage {

    private final String message;
    private final int playerTurn;

    public GeneralInfoBroadcastMsg(int playerTurn, String message) {
        this.message = message;
        this.playerTurn = playerTurn;
    }

    public String getMessage() {
        return message;
    }

    public int getPlayerTurn() {
        return playerTurn;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
