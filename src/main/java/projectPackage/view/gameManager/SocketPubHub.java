package projectPackage.view.gameManager;

import projectPackage.sharedInterface.IBroker;

import java.net.Socket;
import java.util.concurrent.ExecutorService;

/**
 * This SocketPubServer extends a {@link SocketHub} to manage connection requests for
 * the publisher-subscriber component. A {@link Broker} is used instead of a
 * RequestHandler and newHandler method creates a new SubscriberHandler instead of a
 * ClientHandler.
 */
public class SocketPubHub extends SocketHub {

    private final IBroker broker;

    /**
     * Create a new Publisher socket server.
     *
     * @param port            the port where to listen for incoming connections
     * @param executorService the executor service to manage additional threads
     * @param broker          the broker interface of the pub-sub component
     */
    public SocketPubHub(int port, ExecutorService executorService, IBroker broker) {

        super(port, executorService);
        this.broker = broker;

    }

    @Override
    protected SocketHandler createHandler(Socket socket) {

        return new SubscriberHandler(socket, broker);

    }

}

