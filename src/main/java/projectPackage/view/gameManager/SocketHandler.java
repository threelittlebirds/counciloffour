package projectPackage.view.gameManager;

import java.net.Socket;

/**
 * Created by Med on 27/06/2016.
 */

abstract class SocketHandler implements Runnable{

    private final Socket socket;

    /**
     * Abstract constructor for a new SocketHandler.
     *
     * @param socket the Socket connection
     */
    SocketHandler(Socket socket) {

        this.socket = socket;
    }

    /**
     * Get the Socket connection.
     *
     * @return the Socket connection
     */
    protected Socket getSocket() {

        return socket;

    }

}
