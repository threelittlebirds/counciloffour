package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

public class AskActionMessage implements ResponseMsg {

    private TextTable currentMap;

    public AskActionMessage(TextTable currentMap) {
        this.currentMap = currentMap;
    }

    public TextTable getCurrentMap() {
        return currentMap;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

}
