package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;


public class KingCard extends BonusContainer implements Card {

    public KingCard(Bonus bonus) {
        super(bonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("KingCard,");
        super.activateBonus();

    }

}
