package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.model.GameBoard;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.GeneralInfoBroadcastMsg;
import projectPackage.view.messages.responseMessages.responseActions.AskMainActionResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.AskQuickActionResponseMsg;

public class BeginState extends TurnState {

	private final int MAIN = 1;
	private final int QUICK = 2;
	GameBoard gameBoard;

    public BeginState(TurnMachine tm) {
		super(tm);
		gameBoard = tm.getGameBoard();
    }

	@Override
	public void start(Action a) {
		BeginAction ba = (BeginAction) a;
		tm.getGameBoard().drawPoliticCard();
		if(ba.getChoiceAction() == 1) {
			tm.setMainOrQuick(true);
		}
		else {
			tm.setMainOrQuick(false);
		}

	}

	@Override
	public ResultMsgPair nextState() throws CloneNotSupportedException {
		if(tm.isMainOrQuick()) {
			tm.setCurrentState(new MainState(tm));
			responseMsg = new AskMainActionResponseMsg(gameBoard.printSideCouncillors(),gameBoard.printAllCouncils(),gameBoard.printRegionCouncils(),gameBoard.printPermitCardsOfRegions(),gameBoard.printPersonalPermitCards(),gameBoard.getCurrPlayer().getPermitDeck(),gameBoard.printBoard(),gameBoard.getNameOfCities());
			broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), tm.getGameBoard().getCurrPlayer().getName() + " chose Main Action!");
			resultMsgPair = new ResultMsgPair(responseMsg, broadcastMessage);
		}
		else  {
			tm.setCurrentState(new QuickState(tm));
			responseMsg = new AskQuickActionResponseMsg(false,gameBoard.printPermitCardsOfRegions(),gameBoard.printSideCouncillors(),gameBoard.printAllCouncils());
			broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), tm.getGameBoard().getCurrPlayer().getName() + " chose Quick Action");
			resultMsgPair = new ResultMsgPair(responseMsg, broadcastMessage);
		}
		return resultMsgPair;
	}
	
	


}
