package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class MainQuickRequestMsg extends ActionRequestMsg {
	
	private int choiceAction;

	public MainQuickRequestMsg(Token token, int choiceAction) {
		super(token);
		this.choiceAction = choiceAction;
	}

	public int getChoiceAction() {
		return choiceAction;
	}


	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
