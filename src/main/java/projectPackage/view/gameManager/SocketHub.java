package projectPackage.view.gameManager;

import projectPackage.sharedInterface.IRequestHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketHub extends Thread {

    private static final Logger LOG = Logger.getLogger(SocketHub.class.getName());

    private final int port;
    private final ExecutorService executorService;
    private boolean isStopped;

    // RequestHandler is used only in the server implementation (the pub-sub uses a
    // broker)
    private IRequestHandler gamesController;

    /**
     * Create a new SocketHub.
     *
     * @param port the port where to listen
     * @param executorService an executor service to handle the threads
     * @param requestHandler the interface to handle client requests
     */
    public SocketHub(int port, ExecutorService executorService, IRequestHandler
            requestHandler) {

        this(port, executorService);
        this.gamesController = requestHandler;

    }

    /**
     * Create a new SocketHub.
     *
     * @param port the port where to listen
     * @param executorService an executor service to handle the threads
     */
    protected SocketHub(int port, ExecutorService executorService) {

        this.port = port;
        this.executorService = executorService;
        this.isStopped = false;

    }

    /**
     * Get a new Client handler for client-server reqeusts.
     *
     * @param socket the socket used for the connection
     * @return the new client handler
     */
    protected SocketHandler createHandler(Socket socket) {

        return new ClientHandler(socket, gamesController);

    }

    /**
     * Stop the server component.
     */
    public synchronized void stopServer() {

        // Attempts to stop all actively executing tasks, halts the processing of
        // waiting tasks, and returns a list of the tasks that were awaiting execution.
        executorService.shutdownNow();
        this.isStopped = true;

    }

    public void run() {

        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(getPort());

            while (!isStopped()) {

                Socket socket;
                socket = serverSocket.accept();
                SocketHandler socketHandler = createHandler(socket);
                getExecutorService().execute(socketHandler);

            }

            serverSocket.close();

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "IOException in socket handler", e);
        }

    }

    /**
     * Get the port where the socket is listening to.
     *
     * @return the socket port
     */
    protected int getPort() {

        return port;

    }

    /**
     * Get the ExecutorService.
     *
     * @return the ExecutorService
     */
    protected ExecutorService getExecutorService() {

        return executorService;

    }

    /**
     * Get the RequestHandler.
     *
     * @return the RequestHandler
     */
    protected IRequestHandler getRequestHandler() {

        return gamesController;

    }

    /**
     * Check if the server is stopped or not.
     *
     * @return true, if the server is stopped
     */
    protected boolean isStopped() {

        return isStopped;

    }

    /**
     * Set the server stopped status
     *
     * @param isStopped true, to set the server as stopped
     */
    protected void setStopped(boolean isStopped) {

        this.isStopped = isStopped;

    }

}

