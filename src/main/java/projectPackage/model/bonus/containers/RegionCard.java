package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;


public class RegionCard extends BonusContainer implements Card {

    public RegionCard(Bonus bonus) {
        super(bonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("RegionCard,");
        super.activateBonus();
    }


}
