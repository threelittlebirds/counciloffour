package projectPackage.controller.action;

import java.util.LinkedList;

import projectPackage.model.Player;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class InitAction extends Action {
	
	private LinkedList<Player> players;

	public InitAction(LinkedList<Player> players,TurnMachine tm) {
		super(tm);
		this.players = players;
	}

	public LinkedList<Player> getPlayers() {
		return players;
	}

	@Override
	public ResultMsgPair execute() throws CloneNotSupportedException {
		return tm.start(this);
	}

}
