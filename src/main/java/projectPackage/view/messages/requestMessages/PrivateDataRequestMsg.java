package projectPackage.view.messages.requestMessages;

import projectPackage.view.Token;

/**
 * A message to request private data about the player.
 */
public class PrivateDataRequestMsg extends RequestMessage {

    /**
     * Create a new PrivateDateRequestMsg message.
     *
     * @param token the token that identifies the client
     */
    public PrivateDataRequestMsg(Token token) {

        super(token);

    }

}
