package projectPackage.view.gamePlayer.Cli;

import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.Deck;
import projectPackage.model.utility.Pair;
import projectPackage.model.utility.table.TextTable;
import projectPackage.view.Token;
import projectPackage.view.messages.requestMessages.*;
import projectPackage.view.messages.requestMessages.requestActions.*;
import projectPackage.view.messages.responseMessages.ResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.*;

import java.util.*;

/**
 * This class interpret command line commands and converts them into request messages.
 */
public class CliParser {


    final static int MAIN = 1;
    final static int QUICK = 2;

    /**
     * Suppress the default constructor for noninstantiability.
     */
    private CliParser() {

        throw new AssertionError();

    }

    /**
     * Interpret command line commands and converts them into request messages.
     *
     * @param token the client token to generate the request
     * @param cmd   the command string
     * @return the corresponding action, null if the command was not well formed
     */
    public static RequestMessage parseString(Token token, String cmd, ResponseMsg msg) {
        cmd=cmd.toUpperCase();
        if (cmd.toUpperCase().matches("^(MAP\\s(ORIGINAL|AKRAGAS|BARANZANGELES|QUARTO|SCAMPIA))$")) {

            String param = cmd.substring(4) + ".xml";
            return new ChangeMapRequestMsg(token, param);

        }

        if (cmd.toLowerCase().matches("^chat\\s(.+)$")) {

            String param = cmd.replaceFirst("chat ", "");
            return new SendChatMsg(token, param);
        }

        if (cmd.toLowerCase().matches("^get info$")) {

            return new PrivateDataRequestMsg(token);
        }

        if (cmd.toLowerCase().matches("^get log$")) {

            return new PublicLogRequestMsg(token);

        }

        if (cmd.toUpperCase().matches("^MAIN$")) {
            return new MainQuickRequestMsg(token, MAIN);
        } else if (cmd.toUpperCase().matches("^QUICK$")) {
            return new MainQuickRequestMsg(token, QUICK);
        }


        if (msg instanceof AskMainActionResponseMsg) {
            AskMainActionResponseMsg mainMsg = (AskMainActionResponseMsg) msg;
            if (cmd.toUpperCase().matches("(MAIN|QUICK|SELL|BUY|JUMP)")){
                return null;
            }
            //First Main Action
            if (cmd.toUpperCase().matches("MAIN1$")) {
                mainMsg.getSideCouncillors().printTable();
                mainMsg.getCouncils().printTable();
                int sideChoose;
                do {
                    System.out.println("Choose one of the available councillors at the side of the board\n" +
                            "Type a number from 1 to 8!");
                    sideChoose = new Scanner(System.in).nextInt();
                } while (!String.valueOf(sideChoose).matches("[12345678]"));
                String councilSelected;
                do {
                    System.out.println("Choose a councils:\n" +
                            "Type COAST for COAST's Council\n" +
                            "Type HILLS for HILLS's Council\n" +
                            "Type MOUNTAINS for MOUNTAINS's Council\n" +
                            "Type KING for KING's Council");
                    councilSelected = new Scanner(System.in).nextLine();
                } while (!councilSelected.toUpperCase().matches("(COAST|MOUNTAINS|HILLS|KING)"));
                return new MainAction1Message(token, sideChoose, getIntFromCouncil(councilSelected));
            }
            //Second Main Action
            if (cmd.toUpperCase().matches("MAIN2$")) {
                mainMsg.getRegionCouncils().printTable();
                mainMsg.getPermitCardsOfRegions().forEach(TextTable::printTable);
                String permitSelected;
                do {
                    System.out.println("Choose a Permit Card in front of one council:\n" +
                            "Type COAST1 or COAST2 for COAST's Council first or second Permit Card\n" +
                            "Type HILLS1 or HILLS2 for HILLS's Council first or second Permit Card\n" +
                            "Type MOUNTAINS1 or MOUNTAINS2 for MOUNTAINS's Council first or second Permit Card\n");
                    permitSelected = new Scanner(System.in).nextLine().toUpperCase();
                } while (!permitSelected.matches("(COAST1|COAST2|MOUNTAINS1|MOUNTAINS2|HILLS1|HILLS2)"));
                int regionSelected = getIntFromCouncil(permitSelected.substring(0, permitSelected.length() - 1));
                int whichPermitCard = Integer.parseInt(permitSelected.substring(permitSelected.length() - 1, permitSelected.length()));
                return new MainAction2Message(token, regionSelected, whichPermitCard);
            }
            //Third Main Action
            if (cmd.toUpperCase().matches("MAIN3$")) {
                System.out.println("Map: \n");
                mainMsg.getCities().printTable();
                Deck<PermitCard> privatePermitCard = mainMsg.getPersonalPermitCards();
                if(privatePermitCard.isEmpty()){
                    System.out.println("\nYou don't have Permit Card!\n" +
                                "Which main action do you want to do?\n" +
                                "Type MAIN1 for: Elect a councillor\n" +
                                "Type MAIN2 for: Acquire a business permit tile\n" +
                                "Type MAIN3 for: Build an emporium using permit tile\n" +
                                "Type MAIN4 for: Build an emporium with the help of the King\n");
                }else {
                    System.out.println("Your Permit Cards face up: \n");
                    mainMsg.getPersPermitCards().printTable();
                    String s = "";
                    s = s + "[";
                    for (int i = 1; i <= privatePermitCard.size(); i++) {
                        s = s + "i";
                    }
                    s = s + "]";
                    int selectedPermitCard;
                    do {
                        System.out.println("Choose one of the Permit Cards face up in front of you\n" +
                                "Type a number from 1 to " + privatePermitCard.size() + "!\n");
                        selectedPermitCard = new Scanner(System.in).nextInt()-1;
                    } while (!String.valueOf(selectedPermitCard).matches(s));
                    PermitCard tempPermit = privatePermitCard.get(selectedPermitCard - 1);
                    char[] cities = tempPermit.getCities();
                    LinkedList<Character> citiesTemp = new LinkedList<>();
                    for (char city : cities) {
                        citiesTemp.add(city);
                    }
                    char selectedCity;
                    do {
                        System.out.println("The cities on the Permit Card which you just chose are:\n");
                        System.out.println("[");
                        for (int i = 0; i < cities.length; i++) {
                            String t = "";
                            t = t + cities[i];
                            if (i != cities.length - 1) t = t + ", ";
                            System.out.println(t);
                        }
                        System.out.println("]\n");
                        System.out.println("Choose one of the cities which are indicated on the Permit Card\n");
                        selectedCity = new Scanner(System.in).nextLine().charAt(0);
                    } while (!citiesTemp.contains(selectedCity));

                return new MainAction3Message(token, selectedPermitCard, selectedCity);
                }
            }
            //Fourth Main Action
            if (cmd.toUpperCase().matches("MAIN4$")) {
                LinkedList<String> allCities = mainMsg.getAllCities();
                System.out.println("You are using Council of the King!");
                System.out.println("Map: \n");
                mainMsg.getCities().printTable();
                String selectedCity;
                do {
                    System.out.println("Choose the city where you want to build an emporium\n");
                    selectedCity = new Scanner(System.in).nextLine().toUpperCase();
                } while (!allCities.contains(selectedCity));
                return new MainAction4Message(token, selectedCity.charAt(0));
            }
        }

        if (msg instanceof AskQuickActionResponseMsg){
            if (cmd.toUpperCase().matches("(MAIN|QUICK|SELL|BUY|JUMP)")){
                return null;
            }
            AskQuickActionResponseMsg quickMsg = (AskQuickActionResponseMsg) msg;
            //First Quick Action
            if(quickMsg.isMainExecuted()) {
                if (cmd.toUpperCase().matches("END$")) {
                    return new EndRequestMsg(token);
                }
            }
            if (cmd.toUpperCase().matches("QUICK1$")) {
                return new QuickAction1Message(token);
            }
            //Second Quick Action
            if (cmd.toUpperCase().matches("QUICK2$")) {
                String whichRegion;
                for(TextTable tt : quickMsg.getPermitCards()){
                    tt.printTable();
                }
                do {
                    System.out.println("Which region do you want to change permit tiles?\n" +
                            "Type COAST for COAST Region\n" +
                            "Type HILLS for HILLS Region\n" +
                            "Type MOUNTAINS for MOUNTAINS Region\n");
                    whichRegion = new Scanner(System.in).nextLine();
                }while (!whichRegion.matches("(COAST|MOUNTAINS|HILLS)"));
                return new QuickAction2Message(token, getIntFromCouncil(whichRegion));
            }
            //Third Quick Action
            if (cmd.toUpperCase().matches("QUICK3$")) {
                quickMsg.getSideCouncils().printTable();
                quickMsg.getCouncils().printTable();
                int sideChoose;
                do {
                    System.out.println("Choose one of the available councillors at the side of the board" +
                            "Type a number from 1 to 8!\n");
                    sideChoose = new Scanner(System.in).nextInt();
                } while (!String.valueOf(sideChoose).matches("[12345678]"));
                String councilSelected;
                do {
                    System.out.println("Choose a council:\n" +
                            "Type COAST for COAST's Council\n" +
                            "Type HILLS for HILLS's Council\n" +
                            "Type MOUNTAINS for MOUNTAINS's Council\n" +
                            "Type KING for KING's Council\n");
                    councilSelected = new Scanner(System.in).nextLine();
                } while (!councilSelected.matches("(COAST|MOUNTAINS|HILLS|KING)"));
                return new QuickAction3Message(token, getIntFromCouncil(councilSelected), sideChoose);
            }
            //Fourth Quick Action
            if (cmd.toUpperCase().matches("QUICK4$")) {
                return new QuickAction4Message(token);
            }
        }
        if (msg instanceof AskToSellMessage){
            if (cmd.toUpperCase().matches("(MAIN|QUICK|SELL|BUY|JUMP)")){
                return null;
            }
            if (cmd.toUpperCase().matches("^YES$")) {
                AskToSellMessage sellMsg = (AskToSellMessage) msg;
                LinkedList<TextTable> permitCards = sellMsg.getSeparatePermitCards();
                LinkedList<TextTable> politicCards = sellMsg.getSeparatePoliticCards();
                TextTable assistants = sellMsg.getAssistants();
                int numAssistants = sellMsg.getNumAssistants();
                LinkedList<Pair> politicToSell = new LinkedList<>();
                LinkedList<Pair> permitToSell = new LinkedList<>();
                Pair assistantsToSell = null;
                int i = 1;
                for (TextTable tt : permitCards) {
                    tt.printTable();
                    char c;
                    do {
                        System.out.println("Do you want to sell this Permit Card?  (Y/N)");
                        c = new Scanner(System.in).nextLine().trim().toUpperCase().charAt(0);
                    } while (!String.valueOf(c).matches("[YN]"));
                    if (String.valueOf(c).matches("[Y]")) {
                        System.out.print("Insert price: ");
                        int price = new Scanner(System.in).nextInt();
                        permitToSell.add(new Pair(i, price));
                    }
                    i++;
                }
                i = 1;
                for (TextTable tt : politicCards) {
                    tt.printTable();
                    char c;
                    do {
                        System.out.println("Do you want to sell this Politic Card?  (Y/N)");
                        c = new Scanner(System.in).nextLine().trim().toUpperCase().charAt(0);
                    } while (!String.valueOf(c).matches("[YN]"));
                    if (String.valueOf(c).matches("[Y]")) {
                        System.out.print("Insert price: ");
                        int price = new Scanner(System.in).nextInt();
                        politicToSell.add(new Pair(i, price));
                    }
                    i++;
                }
                System.out.println("Your Assistants are: \n");
                assistants.printTable();
                char c;
                do {
                    System.out.println("Do you want to sell some assistants? (Y/N)");
                    c = new Scanner(System.in).nextLine().trim().toUpperCase().charAt(0);
                } while (!String.valueOf(c).matches("[YN]"));
                int numberAssistants;
                if (String.valueOf(c).matches("[Y]")) {
                    do {
                        System.out.println("How many Assistants do you want to sell?");
                        numberAssistants = new Scanner(System.in).nextInt();
                    } while (numAssistants < 1 || numberAssistants > numAssistants);
                    System.out.print("Insert prize for Assistant: ");
                    int prize = new Scanner(System.in).nextInt();
                    assistantsToSell = new Pair(numAssistants, prize);
                }
                return new SellFilledRequestMsg(token, permitToSell, politicToSell, assistantsToSell);
            }else{
                if (cmd.toUpperCase().matches("^NO$")) {
                    return new EndRequestMsg(token);
                }
            }
        }
        if (msg instanceof AskToBuyMessage) {
            if (cmd.toUpperCase().matches("(MAIN|QUICK|SELL|BUY|JUMP)")){
                return null;
            }
            if (cmd.toUpperCase().matches("^YES$")) {

                AskToBuyMessage buyMsg = (AskToBuyMessage) msg;
                HashMap<Token, LinkedList<LinkedList<Pair>>> staffToBuy = new HashMap<>();
                HashMap<Token, TextTable> permitToBuy = buyMsg.getPermitToBuy();
                HashMap<Token, Integer> numberPermitToBuy = buyMsg.getNumPermitToBuy();
                HashMap<Token, LinkedList<Integer>> prizePermitToBuy = buyMsg.getPrizePermitToBuy();
                HashMap<Token, TextTable> politicToBuy = buyMsg.getPoliticToBuy();
                HashMap<Token, Integer> numberPoliticToBuy = buyMsg.getNumPoliticToBuy();
                HashMap<Token, LinkedList<Integer>> prizePoliticToBuy = buyMsg.getPrizePoliticToBuy();
                HashMap<Token, TextTable> assistantToBuy = buyMsg.getAssistantToBuy();
                HashMap<Token, Integer> numberAssistantToBuy = buyMsg.getNumAssistantToBuy();
                HashMap<Token, Integer> prizeAssistantToBuy = buyMsg.getPrizeAssistantToBuy();
                HashMap<Token, LinkedList<Pair>> permitBought = new HashMap<>();
                HashMap<Token, LinkedList<Pair>> politicBought = new HashMap<>();
                HashMap<Token, LinkedList<Pair>> assistantBought = new HashMap<>();
                for (Token t : permitToBuy.keySet()) {
                    TextTable tt = permitToBuy.get(t);
                    tt.printTable();
                    LinkedList<Integer> list = new LinkedList<>();
                    boolean flag = true;
                    do {
                        System.out.print("Insert number of permit cards that you want to buy separated by comma: ");
                        String s = new Scanner(System.in).nextLine();
                        StringTokenizer st = new StringTokenizer(s, ", ");
                        while (st.hasMoreTokens()) {
                            list.add(Integer.parseInt(st.nextToken()));
                        }
                        if (list.size() > numberPermitToBuy.get(t)) flag = false;
                        for (Integer i : list) {
                            if (i < 1 || i > numberPermitToBuy.get(t)) {
                                flag = false;
                                break;
                            }
                        }
                    } while (!flag);
                    LinkedList<Pair> permitList = new LinkedList<>();
                    for (Integer i : list) {
                        Pair p = new Pair(i, prizePermitToBuy.get(t).get(i));
                        permitList.addLast(p);
                    }
                    permitBought.put(t, permitList);
                }
                for (Token t : politicToBuy.keySet()) {
                    TextTable tt = politicToBuy.get(t);
                    tt.printTable();
                    LinkedList<Integer> list = new LinkedList<>();
                    boolean flag = true;
                    do {
                        System.out.print("Insert number of politic cards that you want to buy separated by comma: ");
                        String s = new Scanner(System.in).nextLine();
                        StringTokenizer st = new StringTokenizer(s, ", ");
                        while (st.hasMoreTokens()) {
                            list.add(Integer.parseInt(st.nextToken()));
                        }
                        if (list.size() > numberPoliticToBuy.get(t)) flag = false;
                        for (Integer i : list) {
                            if (i < 1 || i > numberPoliticToBuy.get(t)) {
                                flag = false;
                                break;
                            }
                        }
                    } while (!flag);
                    LinkedList<Pair> politicList = new LinkedList<>();
                    for (Integer i : list) {
                        Pair p = new Pair(i, prizePoliticToBuy.get(t).get(i));
                        politicList.addLast(p);
                    }
                    politicBought.put(t, politicList);
                }
                for (Token t : assistantToBuy.keySet()) {
                    TextTable tt = assistantToBuy.get(t);
                    tt.printTable();
                    int num;
                    do {
                        System.out.print("How many assistants do you want to buy? \n");
                        num = new Scanner(System.in).nextInt();
                    } while (num < 1 || num > numberAssistantToBuy.get(t));
                    Pair assistant = new Pair(num, prizeAssistantToBuy.get(t));
                    LinkedList<Pair> list = new LinkedList<>();
                    list.addLast(assistant);
                    assistantBought.put(t, list);
                }
                for (Token t : permitBought.keySet()) {
                    LinkedList<LinkedList<Pair>> list = new LinkedList<>();
                    list.addLast(permitBought.get(t));
                    staffToBuy.put(t, list);
                }
                for (Token t : politicBought.keySet()) {
                    if (!staffToBuy.keySet().contains(t)) {
                        LinkedList<LinkedList<Pair>> list = new LinkedList<>();
                        list.addLast(new LinkedList<>());
                        list.addLast(politicBought.get(t));
                        staffToBuy.put(t, list);
                    } else {
                        staffToBuy.get(t).addLast(politicBought.get(t));
                    }
                }
                for (Token t : assistantBought.keySet()) {
                    if (!staffToBuy.keySet().contains(t)) {
                        LinkedList<LinkedList<Pair>> list = new LinkedList<>();
                        list.addLast(new LinkedList<>());
                        list.addLast(new LinkedList<>());
                        list.addLast(assistantBought.get(t));
                        staffToBuy.put(t, list);
                    } else {
                        staffToBuy.get(t).addLast(assistantBought.get(t));
                    }
                }

                return new BuyFilledRequestMsg(token, staffToBuy);
            }else if (cmd.toUpperCase().matches("^NO$")) {
                return new EndRequestMsg(token);
            }


        }
        if (msg instanceof BonusToFillMessage) {
            if (cmd.toUpperCase().matches("(MAIN|QUICK|SELL|BUY|JUMP)")){
                return null;
            }
            BonusToFillMessage bonusToFillMessage =(BonusToFillMessage) msg;
            if(cmd.toUpperCase().matches("ACTIVATE$")) {

                LinkedList<Object> list = new LinkedList<>();

                for (String s : bonusToFillMessage.getBonusList()) {


                    if (s.equals("Obtain a coin")) {
                        String coinResponse;
                        boolean isValid = false;
                        System.out.println("You obtain the bonus of a reward token from a city" +
                                "in which you have an emporium." +
                                "You cannot choose one of the tokens " +
                                "which advance you along the nobility track.\n");
                        bonusToFillMessage.getCoinTable().printTable();

                        while (true) {

                            System.out.println("Choose the city of the reward token:\n");
                            coinResponse = new Scanner(System.in).nextLine();


                            for (String city : bonusToFillMessage.getCitiesOfPlayer()) {
                                if (coinResponse.toUpperCase().matches(city.toUpperCase())) {
                                    isValid = true;
                                    break;
                                }
                            }

                            if (isValid) {
                                list.addFirst(coinResponse);
                                break;
                            } else {
                                System.out.println("This city is not valid.\n");
                            }


                        }


                    }
                    if (s.equals("Obtain 2 coins")) {
                        boolean isValid = false;
                        LinkedList<String> cities = new LinkedList<>();
                        System.out.println("You obtain the bonus of a reward token from a city" +
                                "in which you have an emporium.\n" +
                                "You cannot choose one of the tokens " +
                                "which advance you along the nobility track.\n" +
                                "Choose the 2 cities of the reward token:\n");
                        bonusToFillMessage.getCoinTable().printTable();

                        while (true) {

                            int i = 0;

                            while (i < 2) {

                                System.out.println("Choose city:\n");

                                String choice = new Scanner(System.in).nextLine();

                                for (String city : bonusToFillMessage.getCitiesOfPlayer()) {
                                    if (choice.toUpperCase().matches(city.toUpperCase())) {
                                        isValid = true;
                                        break;
                                    }
                                }

                                if (isValid) {
                                    cities.addFirst(choice);
                                    i++;
                                } else {
                                    System.out.println("This city is not valid.\n");
                                }
                            }
                            list.addFirst(cities);
                            break;

                        }

                    }

                    if (s.equals("Activate one of your Permit Cards")) {

                        System.out.println("You receive the bonus of one of the permit tiles " +
                                "which you previously bought.\n" +
                                "You can take also a face-down tile.\n");
                        bonusToFillMessage.getPlayerPermitTable().printTable();

                        while (true) {

                            System.out.println("Choose a permit card:\n");
                            int permitChoice = new Scanner(System.in).nextInt();
                            if (permitChoice <= bonusToFillMessage.getPlayerPermitCards()) {
                                list.addFirst(permitChoice);
                                break;
                            } else System.out.println("Invalid Card");

                        }
                    }

                    if (s.equals("Activate a face-up Permit Card on the gameboard.")) {
                        System.out.println("You can take a face-up permit tile " +
                                "without paying the cost");

                        for (TextTable tt : bonusToFillMessage.getGamePermitTable()) {
                            tt.printTable();
                        }

                        while (true) {

                            System.out.println("Choose a permit card:\n");
                            int permitChoice = new Scanner(System.in).nextInt();
                            if (permitChoice <= 6) {
                                list.addFirst(permitChoice);
                                break;
                            } else System.out.println("Invalid Card");

                        }

                    }

                }

            }
        }
        if (cmd.toUpperCase().matches("^SELL$")) {
            return new SellRequestMsg(token);
        }
        if (cmd.toUpperCase().matches("^BUY$")) {
            return new BuyRequestMsg(token);
        }
        if (cmd.toUpperCase().matches("^JUMP$")) {
            return new EndRequestMsg(token);
        }
        return null;

    }


    private static int getIntFromCouncil(String region){
        switch (region.toUpperCase()){
            case "COAST" : return 1;
            case "HILLS" : return 2;
            case "MOUNTAIN" : return 3;
            case "KING" : return 4;
        }
        return -1;
    }
    /*


    private static String choiceWhichMainAction(){
        String i;
        do {
            System.out.println("Which main action do you want to do?\n" +
                "Type MAIN1 for: Elect a councillor\n" +
                "Type MAIN2 for: Acquire a business permit tile\n" +
                "Type MAIN3 for: Build an emporium using permit tile\n" +
                "Type MAIN4 for: Build an emporium with the help of the King\n");
            Scanner scanner = new Scanner(System.in);
            i = scanner.nextLine().toUpperCase();
        } while (!i.matches("(MAIN1|MAIN2|MAIN3|MAIN4)"));

        return i;
    }

    private static TypeRegion getTypeFromString(String region){
        switch (region.toUpperCase()){
            case "COAST" : return TypeRegion.COAST;
            case "HILLS" : return TypeRegion.HILLS;
            case "MOUNTAIN" : return TypeRegion.MOUNTAINS;
        }
        return TypeRegion.COAST;
    }
    */
}
