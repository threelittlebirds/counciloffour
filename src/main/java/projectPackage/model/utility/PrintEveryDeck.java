package projectPackage.model.utility;

import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.PoliticCard;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * Created by Carmelo on 19/06/2016.
 */
public final class PrintEveryDeck implements Serializable {
    //private LinkedList<PoliticCard> politicCards = new LinkedList<>();
    //private LinkedList<PermitCard> permitCards = new LinkedList<>()
    private static PrintStream out = new PrintStream(System.out);
    public static void printSetPoliticCard(Set<PoliticCard> politicCards){
        StringBuilder sb = new StringBuilder(300);
        sb.append("Politic Cards: \n");
        sb.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        for(PoliticCard p : politicCards){
            sb.append("++   " + Util.getStringFromColor(p.getCardColor())+ "    ");
        }
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        out.println(sb);
    }
    public static void printSetPermitCard(Collection<PermitCard> permitCards){
        StringBuilder sb = new StringBuilder(300);
        sb.append("Permit Cards: \n");
        sb.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        for(PermitCard p : permitCards){
            sb.append("++   " + p.getCities() + "    ");
        }
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        out.println(sb);
    }
    public static void printSetPrice(Collection<Integer> price){
        StringBuilder sb = new StringBuilder(300);
        for(Integer i : price){
            sb.append("++   " + i + "    ");
        }
        out.println(sb);
    }
    public static void printSetPair(Collection<Pair> pairs){
        StringBuilder sb = new StringBuilder(200);
        for(Pair p : pairs){
            sb.append(p.toString());
            sb.append("  ");
        }
        out.println(sb);
    }
}
