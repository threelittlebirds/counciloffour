package projectPackage.controller.action;

import projectPackage.controller.exceptions.*;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class QuickAction1 extends Action {

    public QuickAction1(TurnMachine tm) {
        super(tm);
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
        if(!enoughMoney()) throw new NotEnoughMoney();
        return tm.start(this);
    }

    private boolean enoughMoney(){
        return tm.getGameBoard().getCurrPlayer().getMoneyPosition().getPosition() >= 3;
    }
}
