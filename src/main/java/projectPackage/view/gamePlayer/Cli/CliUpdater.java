package projectPackage.view.gamePlayer.Cli;


import projectPackage.view.gamePlayer.ViewUpdater;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 *
 */
public class CliUpdater implements ViewUpdater {

    MessageVisitor visitor;

    public CliUpdater() {

        visitor = new CliMessageVisitor();

    }

    @Override
    public void update(BroadcastMessage msg) {

        msg.display(visitor);

    }

    @Override
    public void update(ResponseMsg msg) {

        msg.display(visitor);

    }

    @Override
    public void setPlayerNum(int playerNum) {

        visitor.setPlayerNum(playerNum);

    }

    @Override
    public int getPlayerNum() {

        return visitor.getPlayerNum();

    }

}
