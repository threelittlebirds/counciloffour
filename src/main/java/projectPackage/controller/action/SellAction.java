package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.model.utility.Pair;
import projectPackage.view.messages.ResultMsgPair;

import java.util.LinkedList;

/**
 * Created by Carmelo on 19/06/2016.
 */
public class SellAction extends Action {

    private LinkedList<Pair> permitToSell;
    private LinkedList<Pair> politicToSell;
    private Pair assintantToSell;


    public SellAction(LinkedList<Pair> permitToSell, LinkedList<Pair> politicToSell, Pair assintantToSell, TurnMachine tm) {
        super(tm);
        this.permitToSell = permitToSell;
        this.politicToSell = politicToSell;
        this.assintantToSell = assintantToSell;
    }

    public LinkedList<Pair> getPermitToSell() {
        return permitToSell;
    }

    public LinkedList<Pair> getPoliticToSell() {
        return politicToSell;
    }

    public Pair getAssintantToSell() {
        return assintantToSell;
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
        return tm.start(this);
    }
}
