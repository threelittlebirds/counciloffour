package projectPackage.view.gameManager;

import projectPackage.controller.GamesController;
import projectPackage.sharedInterface.IPublisher;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Entry point to start a GameManager.
 */
public class GameManager {

    private static final Logger LOG = Logger.getLogger(GameManager.class.getName());

    public static void main(String[] args) {

        HubConnectionFactory serverInitializer = new HubConnectionFactory();
        IPublisher publisherInterface = serverInitializer.getPublisherInterface();

        GamesController mainController = new GamesController(publisherInterface);

        serverInitializer.setRequestHandler(mainController);
        serverInitializer.startServers();

        LOG.log(Level.INFO, "Game manager started, listening for RMI and Socket " +
                "incoming connections.");

    }

}
