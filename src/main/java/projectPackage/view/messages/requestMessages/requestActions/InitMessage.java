package projectPackage.view.messages.requestMessages.requestActions;


import java.util.*;
import projectPackage.controller.action.*;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.model.*;
import projectPackage.view.Token;

public class InitMessage extends ActionRequestMsg {

	private LinkedList<Player> players;

	public InitMessage(Token token, LinkedList<Player> players) {
		super(token);
		this.players = players;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}
	
	public void addPlayer(Player p){
		players.add(p);
	}

	public LinkedList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(LinkedList<Player> players) {
		this.players = players;
	}

}
