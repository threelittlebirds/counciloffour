package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class BeginAction extends Action {
	
	private int choiceAction;
	

	public BeginAction(int choiceAction,TurnMachine tm) {
		super(tm);
		this.choiceAction = choiceAction;
	}


	@Override
	public ResultMsgPair execute() throws CloneNotSupportedException {
		return tm.start(this);
	}


	public int getChoiceAction() {
		return choiceAction;
	}

}
