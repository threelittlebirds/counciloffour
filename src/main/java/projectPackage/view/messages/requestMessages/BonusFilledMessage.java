package projectPackage.view.messages.requestMessages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.model.bonus.Bonus;
import projectPackage.view.Token;
import projectPackage.view.messages.requestMessages.requestActions.ActionRequestMsg;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Med on 04/07/2016.
 */
public class BonusFilledMessage extends ActionRequestMsg {


    private LinkedList<Object> list;

    public BonusFilledMessage(Token token, LinkedList<Object> list) {
        super(token);
        this.list = list;
    }

    @Override
    public Token getToken() {
        return super.getToken();
    }

    public LinkedList<Object> getList() {
        return list;
    }


    @Override
    public Action createAction(ActionFactoryVisitorImpl visitor) {
        return visitor.visit(this);
    }
}
