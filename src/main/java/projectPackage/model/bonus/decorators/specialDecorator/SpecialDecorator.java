package projectPackage.model.bonus.decorators.specialDecorator;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.decorators.BonusDecorator;
import projectPackage.model.utility.MapLoad;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class SpecialDecorator extends BonusDecorator implements Serializable{

    private static final Logger LOG = Logger.getLogger(MapLoad.class.getName());

    transient Object parameter;

    public SpecialDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    protected abstract void activateBonusDecorator() throws CloneNotSupportedException;

    @Override
    public abstract String toStringDecorator();

    public void setParameters(LinkedList<Object> parameters) {

        if (this instanceof ActionDecorator) {
            ((SpecialDecorator) tempBonus).setParameters(parameters);
        }
        else {
            try {
                this.parameter = parameters.removeFirst();
            } catch (NoSuchElementException e) {
                LOG.log(Level.SEVERE, "Parameters list is empty\n", e);
            }
            if (tempBonus instanceof SpecialDecorator)
                ((SpecialDecorator) tempBonus).setParameters(parameters);

        }
    }

    public Object getParameter() {
        return parameter;
    }
}
