package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by Med on 05/07/2016.
 */
public class BonusActivatorBroadcast implements BroadcastMessage {

    private final String message;

    private final String namePlayer;

    private final int numPlayer;

    public BonusActivatorBroadcast(int numPlayer, String namePlayer) {
        this.numPlayer = numPlayer;
        this.namePlayer = namePlayer;
        this.message = "Player "+numPlayer+" "+namePlayer+" has obtained a bonus!\n";
    }


    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

    public String getMessage() {
        return message;
    }

    public int getNumPlayer() {
        return numPlayer;
    }
}
