package projectPackage.model.turn;

import projectPackage.controller.action.Action;
import projectPackage.model.GameBoard;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.AskMainQuickBroadcastMsg;
import projectPackage.view.messages.broadcastMessage.AskToBuyBroadcastMsg;
import projectPackage.view.messages.broadcastMessage.AskToSellBroadcastMsg;
import projectPackage.view.messages.broadcastMessage.GeneralInfoBroadcastMsg;
import projectPackage.view.messages.responseMessages.responseActions.GoToEndMessage;

public class EndState extends TurnState {

	private int numPlayers;
	private int counterBuySell;
	private final String currPlayer;
	GameBoard gameBoard;

    public EndState(TurnMachine tm) {
		super(tm);
		numPlayers = tm.getGameBoard().getNumPlayers();
		counterBuySell = tm.getCounterBuySell();
		currPlayer = tm.getGameBoard().getCurrPlayer().getName();
		gameBoard = tm.getGameBoard();

    }

	@Override
	public void start(Action a) throws CloneNotSupportedException {
		tm.setMainExecuted(false);
		tm.setQuickExecuted(false);
		tm.setJumpQuickAction(false);
		tm.setMainOrQuick(false);
		//if(a instanceof SellAction){
		//	isSellAction = true;
		//}
		if(tm.getGameBoard().hasBuiltInAllCities() && tm.getLastPlayerOfMach() == -1){   //END OF GAME
			tm.setLastPlayerOfMach(tm.getGameBoard().indexOfLastPlayer());
		}
		if(tm.getGameBoard().indexOfCurrentPlayer() == tm.getGameBoard().indexOfLastPlayer()){
			tm.getGameBoard().setIsFinished(true);
			tm.getGameBoard().getUltimatePointsFromBonus();
			tm.getGameBoard().getUltimatePointsFromPermitCards();
			tm.getGameBoard().getUltimatePointsFromNobilityPosition();
			System.out.println("Ranking:");
			tm.getGameBoard().printPlayers();
		}
	}

	@Override
	public ResultMsgPair nextState() throws CloneNotSupportedException {
		if(tm.getCounterMarket() == numPlayers){
			tm.setCurrentState(new MarketState(tm));
			if(counterBuySell<numPlayers){		// accade quando non tutti i giocatori hanno effettuato la vendita e l'acquisto
				responseMsg = new GoToEndMessage(tm.getGameBoard().getCurrPlayer().getName());
				tm.getGameBoard().setCurrPlayer(tm.nextPlayer());
				broadcastMessage = new AskToSellBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), tm.getGameBoard().printSeparatePermitToSell(),tm.getGameBoard().printSeparatePoliticToSell(),tm.getGameBoard().printAssistantsToSell(),tm.getGameBoard().getCurrPlayer().getNumAssistant());
				//broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), currPlayer + " finished is turn!");
				//responseMsg = new AskToSellMessage(gameBoard.printSeparatePermitToSell(),gameBoard.printSeparatePoliticToSell(),gameBoard.printAssistantsToSell(),gameBoard.getCurrPlayer().getNumAssistant());

			} else {
				if(counterBuySell<(numPlayers*2)){		// accade quando non tutti i giocatori hanno effettuato l'acquisto ma tutti hannno effettuato la vendita
					responseMsg = new GoToEndMessage(tm.getGameBoard().getCurrPlayer().getName());
					tm.getGameBoard().setCurrPlayer(tm.nextRandomPlayer());
					broadcastMessage = new AskToBuyBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), tm.getGameBoard().printAllPermitToBuy(),tm.getGameBoard().printAllPoliticToBuy(),tm.getGameBoard().printAllAssistantToBuy(),tm.getGameBoard().numberOfPermitCards(), tm.getGameBoard().numberOfPoliticCards(), tm.getGameBoard().numberOfAssistants(), tm.getGameBoard().prizeOfPermitCards(), tm.getGameBoard().prizeOfPoliticCards(),tm.getGameBoard().prizeOfAssistants());

					//broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), currPlayer + " finished is turn!");
					//responseMsg = new AskToBuyMessage(gameBoard.printAllPermitToBuy(),gameBoard.printAllPoliticToBuy(),gameBoard.printAllAssistantToBuy(),gameBoard.numberOfPermitCards(), gameBoard.numberOfPoliticCards(),gameBoard.numberOfAssistants(),gameBoard.prizeOfPermitCards(),gameBoard.prizeOfPoliticCards(),gameBoard.prizeOfAssistants());
					if(tm.getCounterBuySell() == numPlayers*2){
						gameBoard.initializeRandomPlayers();
						tm.setCounterMarket(1);
						tm.setCounterBuySell(0);
					}
				}
			}
		}
		else {
			tm.setCurrentState(new BeginState(tm));
			responseMsg = new GoToEndMessage(tm.getGameBoard().getCurrPlayer().getName());
			tm.getGameBoard().setCurrPlayer(tm.nextPlayer());
			broadcastMessage = new AskMainQuickBroadcastMsg(false, tm.getGameBoard().indexOfCurrentPlayer(), tm.getGameBoard().printPlayersSituation(), tm.getGameBoard().printMap());
		}
		resultMsgPair = new ResultMsgPair(responseMsg, broadcastMessage);
		return resultMsgPair;
	}




   
}
