package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.util.LinkedList;


public class BonusToFillMessage implements ResponseMsg {

    private final TextTable coinTable;

    private final LinkedList<TextTable> gamePermitTable;

    private final TextTable playerPermitTable;

    private final int playerPermitCards;

    private final LinkedList<String> citiesOfPlayer;

    private final LinkedList<String> bonusList;

    public BonusToFillMessage(TextTable coinTable, LinkedList<TextTable> gamePermitTable, TextTable playerPermitTable,
                              int playerPermitCards, LinkedList<String> citiesOfPlayer, LinkedList<String> bonusList) {

        this.coinTable = coinTable;
        this.gamePermitTable = gamePermitTable;
        this.playerPermitTable = playerPermitTable;
        this.playerPermitCards = playerPermitCards;
        this.citiesOfPlayer = citiesOfPlayer;
        this.bonusList = bonusList;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

    public LinkedList<String> getBonusList() {
        return bonusList;
    }

    public TextTable getCoinTable() {
        return coinTable;
    }

    public TextTable getPlayerPermitTable() {
        return playerPermitTable;
    }

    public LinkedList<TextTable> getGamePermitTable() {
        return gamePermitTable;
    }

    public LinkedList<String> getCitiesOfPlayer() {
        return citiesOfPlayer;
    }

    public int getPlayerPermitCards() {
        return playerPermitCards;
    }

}
