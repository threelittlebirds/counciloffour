package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;



/**
 * Created by carme on 20/06/2016.
 */
public class SellRequestMsg extends ActionRequestMsg {

    public SellRequestMsg(Token token) {
        super(token);
    }

    @Override
    public Action createAction(ActionFactoryVisitorImpl visitor) {
        return visitor.visit(this);
    }

}
