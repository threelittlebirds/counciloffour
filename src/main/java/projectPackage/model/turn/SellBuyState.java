package projectPackage.model.turn;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.BuyAction;
import projectPackage.controller.action.EndAction;
import projectPackage.controller.action.SellAction;
import projectPackage.model.Player;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.utility.Pair;
import projectPackage.view.Token;
import projectPackage.view.messages.ResultMsgPair;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by carme on 07/07/2016.
 */
public class SellBuyState extends TurnState {




    public SellBuyState(TurnMachine tm) {
        super(tm);
    }

    @Override
    public void start(Action a) throws CloneNotSupportedException {
        if(a instanceof BuyAction){
            BuyAction buyAction = (BuyAction) a;
            buyAction(buyAction);
        }
        if(a instanceof SellAction){

            SellAction sellAction = (SellAction) a;
            sellAction(sellAction);
        }

    }

    @Override
    public ResultMsgPair nextState() throws CloneNotSupportedException {
        tm.setCurrentState(new EndState(tm));
        return tm.start(new EndAction(tm));
    }

    private void buyAction(BuyAction buyAction) throws CloneNotSupportedException {
        HashMap<Token, LinkedList<LinkedList<Pair>>> toBuy = buyAction.getStaffToBuy();
        int toPay = toPay(toBuy); // pago tutto
        tm.getGameBoard().moveOnMoneyTrack(-toPay);
        // sposto merce acquistata e incremento moneta del seller
        for(Token t : toBuy.keySet()){
            Player p = tm.getGameBoard().getPlayer(t);
            for(Pair pair : toBuy.get(t).get(0)){
                PermitCard permitCard = p.removePermitCardSold(pair.getNum());
                tm.getGameBoard().getCurrPlayer().addPermitCard(permitCard);
                tm.getGameBoard().moveOnMoneyTrack(p,pair.getPrice());
            }
            for(Pair pair : toBuy.get(t).get(1)){
                PoliticCard politicCard = p.removePoliticCardSold(pair.getNum());
                tm.getGameBoard().getCurrPlayer().addPoliticCard(politicCard);
                tm.getGameBoard().moveOnMoneyTrack(p,pair.getPrice());
            }
            Pair pair = toBuy.get(t).get(2).getFirst();
            tm.getGameBoard().getCurrPlayer().removeAssistantSold(pair.getNum());
            p.acquireAssistant(pair.getNum());
            tm.getGameBoard().moveOnMoneyTrack(p,pair.getPrice()*pair.getNum());
        }

    }
    private void sellAction(SellAction sellAction){
        LinkedList<Pair> permitInSelling = sellAction.getPermitToSell();
        LinkedList<Pair> politicInSelling = sellAction.getPoliticToSell();
        Pair assistantInSelling = sellAction.getAssintantToSell();
        for(Pair pair : permitInSelling){
            tm.getGameBoard().getCurrPlayer().addPermitCardToSell(pair.getNum(),pair.getPrice());
        }
        for(Pair pair : politicInSelling){
            tm.getGameBoard().getCurrPlayer().addPoliticCardToSell(pair.getNum(),pair.getPrice());
        }
        tm.getGameBoard().getCurrPlayer().addAssistantsToSell(assistantInSelling.getNum(),assistantInSelling.getPrice());
    }

    private int toPay(HashMap<Token, LinkedList<LinkedList<Pair>>> staffToBuy){
        int totalPrize = 0;
        for(Token t : staffToBuy.keySet()){
            for(Pair pair : staffToBuy.get(t).get(0)){
                totalPrize = totalPrize + pair.getPrice();
            }
            for(Pair pair : staffToBuy.get(t).get(1)){
                totalPrize = totalPrize + pair.getPrice();
            }
            Pair pair = staffToBuy.get(t).get(2).getFirst();
            totalPrize = totalPrize + pair.getPrice();
        }
        return totalPrize;
    }

}
