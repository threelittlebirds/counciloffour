package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class QuickAction3Message extends ActionRequestMsg {

	private int whichRegion;
	private int whichCouncillor;

	public QuickAction3Message(Token token, int whichRegion, int whichCouncillor) {
		super(token);
		this.whichRegion = whichRegion;
		this.whichCouncillor = whichCouncillor;
	}

	public int getWhichRegion() {
		return whichRegion;
	}

	public void setWhichRegion(int whichRegion) {
		this.whichRegion = whichRegion;
	}

	public int getWhichCouncillor() {
		return whichCouncillor;
	}

	public void setWhichCouncillor(int whichCouncillor) {
		this.whichCouncillor = whichCouncillor;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
