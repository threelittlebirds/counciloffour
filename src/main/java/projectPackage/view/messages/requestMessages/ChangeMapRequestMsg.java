package projectPackage.view.messages.requestMessages;

import projectPackage.view.Token;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.requestMessages.RequestMessage;

/**
 * Created by carme on 26/06/2016.
 */
public class ChangeMapRequestMsg extends RequestMessage {

    private String mapName;

    public ChangeMapRequestMsg(Token token, String mapName) {
        super(token);
        this.mapName = mapName;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

}
