package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 * Created by Med on 16/06/2016.
 */
public class WhichOfYourCitiesMessage implements ResponseMsg {

    public WhichOfYourCitiesMessage(boolean isDoubleDec) {


        this.isDoubleDec = isDoubleDec;

    }

    private char choice;

    private char secondChoice;

    private boolean isDoubleDec;

    public char getChoice() {
        return choice;
    }

    public void setChoice(char choice) {
        this.choice = choice;
    }

    public boolean isDoubleDec() {
        return isDoubleDec;
    }

    public void setDoubleDec(boolean aDouble) {
        isDoubleDec = aDouble;
    }

    public char getSecondChoice() {
        return secondChoice;
    }

    public void setSecondChoice(char secondChoice) {
        this.secondChoice = secondChoice;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
