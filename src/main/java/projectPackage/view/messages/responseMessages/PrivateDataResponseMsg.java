package projectPackage.view.messages.responseMessages;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;


/**
 *
 */
public class PrivateDataResponseMsg implements ResponseMsg {

    private final TextTable currentSituation;

    public PrivateDataResponseMsg (TextTable currentSituation) {
        this.currentSituation = currentSituation;
    }

    public TextTable getCurrentSituation() {
        return currentSituation;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
