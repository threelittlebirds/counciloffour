package projectPackage.model;

import projectPackage.model.bonus.containers.BonusContainer;
import projectPackage.model.bonus.containers.NobilitySquare;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.Deck;
import projectPackage.model.cards.MultiColor;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.track.square.MoneySquare;
import projectPackage.model.track.square.VictorySquare;
import projectPackage.model.utility.Pair;
import projectPackage.model.utility.Util;
import projectPackage.model.utility.table.TextTable;
import projectPackage.view.Token;

import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.Iterator;
import java.util.LinkedList;


public class Player implements Serializable {

    private String name;
    private Color color;
    private int numAssistants;
    private Token token;
    private MoneySquare moneyPosition;
    private VictorySquare victoryPosition;
    private NobilitySquare nobilityPosition;
    private Deck<PermitCard> permitDeck;
    private Deck<PoliticCard> politicDeck;
    private Deck<PermitCard> graveDeck;
    private static int cont = 1;
    private LinkedList<BonusContainer> bonusCards;

    private HashMap<PermitCard, Integer> permitInSelling;
    private LinkedList<PermitCard> permitInSellingTemp; // quella da stampare
    private HashMap<PoliticCard, Integer> politicInSelling;
    private LinkedList<PoliticCard> politicInSellingTemp; // quella da stampare
    private Pair assistantInSelling;

    public Player(Token token,Color c) {

        this.token = token;
        this.name = this.token.getPlayerName();
		this.color = c;
        permitDeck = new Deck<>();
        politicDeck = new Deck<>();
        graveDeck = new Deck<>();
        cont++;
        bonusCards = new LinkedList<>();
        permitInSelling = new HashMap<>();
        permitInSellingTemp = new LinkedList<>();
        politicInSelling = new HashMap<>();
        politicInSellingTemp = new LinkedList<>();
    }

    public void addPermitCardToSell(int permitCard, int prize){
        PermitCard p = permitDeck.removeCard(permitCard-1);
        permitInSelling.put(p,prize);
        permitInSellingTemp.add(p);
    }

    public void addPoliticCardToSell(int politicCard, int prize){
        PoliticCard p = politicDeck.removeCard(politicCard-1);
        politicInSelling.put(p,prize);
        politicInSellingTemp.add(p);
    }

    public void addAssistantsToSell(int numAssistants, int prize){
        setNumAssistant(this.numAssistants-numAssistants);
        assistantInSelling = new Pair(numAssistants,prize);
    }

    public PermitCard removePermitCardSold(int permitCard){
        PermitCard p = permitInSellingTemp.remove(permitCard-1);
        int prize = permitInSelling.get(p);
        permitInSelling.remove(p);
        return p;
    }

    public PoliticCard removePoliticCardSold(int politicCard){
        PoliticCard p = politicInSellingTemp.remove(politicCard-1);
        int prize = politicInSelling.get(p);
        politicInSelling.remove(p);
        return p;
    }

    public void removeAssistantSold(int numAssistants){
        this.numAssistants = this.numAssistants - numAssistants;
    }

    public HashMap<PermitCard, Integer> getPermitInSelling() {
        return permitInSelling;
    }

    public LinkedList<PermitCard> getPermitInSellingTemp() {
        return permitInSellingTemp;
    }

    public HashMap<PoliticCard, Integer> getPoliticInSelling() {
        return politicInSelling;
    }

    public LinkedList<PoliticCard> getPoliticInSellingTemp() {
        return politicInSellingTemp;
    }

    public Token getToken() {
        return token;
    }


    public LinkedList<BonusContainer> getBonusCards() {
        return bonusCards;
    }

    public void setBonusCards(LinkedList<BonusContainer> bonusCards) {
        this.bonusCards = bonusCards;
    }

    public Deck<PermitCard> getPermitDeck() { return permitDeck; }

	public void setPermitDeck(Deck<PermitCard> permitDeck) {
		this.permitDeck = permitDeck;
	}

	public Deck<PermitCard> getGraveDeck() {
		return graveDeck;
	}

	public void setGraveDeck(Deck<PermitCard> graveDeck) {
		this.graveDeck = graveDeck;
	}

    public Pair getAssistantInSelling() {
        return assistantInSelling;
    }

    public void activateUltimateBonuses() throws CloneNotSupportedException{
        for(BonusContainer b : bonusCards){
            b.activateBonus();
        }
    }

	public void addPermitCard(PermitCard pc) throws CloneNotSupportedException {
		permitDeck.add(pc);
        pc.activateBonus();
	}

    public void addPoliticCard(PoliticCard pc){
        politicDeck.add(pc);
    }

	public Color getColor() {
		return color;
	}

	public String getName(){
    	return name;
    }
    
    public NobilitySquare getNobilityPosition() {
        return nobilityPosition;
    }

    public void setNobilityPosition(NobilitySquare nobilityPosition) {
        this.nobilityPosition = nobilityPosition;
    }

    public VictorySquare getVictoryPosition() {
        return victoryPosition;
    }

    public void setVictoryPosition(VictorySquare victoryPosition) {
        this.victoryPosition = victoryPosition;
    }

    public MoneySquare getMoneyPosition() {
        return moneyPosition;
    }

    public void setMoneyPosition(MoneySquare moneyPosition) {
        this.moneyPosition = moneyPosition;
    }
    
    
    public int getNumAssistant() {
        return numAssistants;
    }

    public void setNumAssistant(int numAssistant) {
        this.numAssistants = numAssistant;
    }
    
    
    public Deck<PoliticCard> getPoliticDeck() {
		return politicDeck;
	}

	public void setPoliticDeck(Deck<PoliticCard> politicDeck) {
		this.politicDeck = politicDeck;
	}

    public void payAssistant(int number) {
        numAssistants = numAssistants-number;
    }

    public void acquireAssistant (int number) {
        numAssistants = numAssistants+number;
    }

    public int getNumMulticolor() {
        int num = 0;
        for (PoliticCard pc : politicDeck) {
            if (pc.getCardColor() instanceof MultiColor) num++;
        }
        return num;
    }
    
    public int hashCode(){
    	final int molt = 41; //numero primo
    	int i = molt*name.hashCode()+molt*color.hashCode()+cont;
    	return i;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Player)) return false;
        if(obj == this) return true;
        Player p = (Player) obj;
        return p.token.getUuid().equals(this.token.getUuid());
    }
     /*
    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("Name: "+name+"\n");
    	sb.append("Values of player:\n");
    	sb.append("Color = "+ Util.getStringFromColor(color)+"\n");
    	sb.append("Num assistants = "+numAssistants+"\n");
    	sb.append("Money position = "+moneyPosition.getPosition()+"\n");
    	sb.append("Victory position = "+victoryPosition.getPosition()+"\n");
    	sb.append("Nobility position = "+nobilityPosition.getPosition()+"\n");
    	sb.append("Yours politic cards:\n");
        StringBuilder sb2 = new StringBuilder(400);
        sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        for(PoliticCard p : politicDeck.getDeckCards()){
            sb2.append("++   " + Util.getStringFromColor(p.getCardColor())+ "   ");
            //sb.append(p);
    		//sb.append("   ");
    	}
        sb2.append("++            ++            ++            ++            ++            ++            ++                                         ++\n");
        sb2.append("++            ++            ++            ++            ++            ++            ++                                         ++\n");
        sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    	sb.append(sb2);
        sb.append("\n");
    	sb.append("Yours permit cards face up:\n");
    	for(PermitCard p : permitDeck.getDeckCards()){
    		sb.append(p);
    		sb.append("   ");
    	}
    	sb.append("\n");
    	sb.append("Yours permit cards face down:\n");
    	for(PermitCard p : graveDeck.getDeckCards()){
    		sb.append(p);
    		sb.append("   ");
    	}
        return sb.toString();
    }
    */

    public LinkedList<PoliticCard> removeMulticolor(int cont) {
        LinkedList<PoliticCard> list = new LinkedList<>();
        int needed = 4-cont;
        Iterator<PoliticCard> iterator = politicDeck.iterator();
        while (iterator.hasNext()) {
            PoliticCard pc = iterator.next();
            if (pc.getCardColor() instanceof MultiColor) {
                list.add(pc);
                iterator.remove();
                needed--;
                if (needed == 0) break;
            }
        }
        return list;

    }

    public TextTable printPermitDeck(){
        String[] columnNames = new String [permitDeck.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = "Permit card "+(i+1);
        }
        int maxNumberOfBounuses = 3;
        for(PermitCard p : permitDeck){
            if(p.getBonus().getNumberOfBonuses() > maxNumberOfBounuses) maxNumberOfBounuses = p.getBonus().getNumberOfBonuses();
        }
        Object[][] data = new Object [maxNumberOfBounuses+2][columnNames.length];
        int i = 0;
        for(PermitCard p : permitDeck){
            String s = "";
            for(int j=0;j<p.getCities().length;j++){
                s = s + p.getCities()[j];
                if(j!=p.getCities().length-1) s = s + ",";
            }
            data[0][i] = s;
            data[1][i] = "";
            int k = 2;
            for (String string : p.getBonus().getSeparatedBonuses()) {
                data[k][i] = string;
                k++;
            }
            i++;
        }
        TextTable tt = new TextTable(columnNames, data);
        return tt;
    }

    public TextTable printGraveDeck() {
        String[] columnNames = new String [graveDeck.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = "Permit card used "+(i+1);
        }
        int maxNumberOfBounuses = 3;
        for(PermitCard p : graveDeck){
            if(p.getBonus().getNumberOfBonuses() > maxNumberOfBounuses) maxNumberOfBounuses = p.getBonus().getNumberOfBonuses();
        }
        Object[][] data = new Object [maxNumberOfBounuses+2][columnNames.length];
        int i = 0;
        for(PermitCard p : graveDeck){
            String s = "";
            for(int j=0;j<p.getCities().length;j++){
                s = s + p.getCities()[j];
                if(j!=p.getCities().length-1) s = s + ",";
            }
            data[0][i] = s;
            data[1][i] = "";
            int k = 2;
            for (String string : p.getBonus().getSeparatedBonuses()) {
                data[k][i] = string;
                k++;
            }
            i++;
        }
        TextTable tt = new TextTable(columnNames, data);
        return tt;
    }

    public int numOfAllPermit(){
        return permitDeck.size()+graveDeck.size();
    }

    public TextTable printAllPermitCards(){
        LinkedList<PermitCard> allPermit = new LinkedList<>();
        for(PermitCard pc : permitDeck){
            allPermit.addLast(pc);
        }
        for(PermitCard pc :graveDeck){
            allPermit.addLast(pc);
        }
        String[] columnNames = new String [allPermit.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = "Permit card "+(i+1);
        }
        int maxNumberOfBounuses = 3;
        for(PermitCard p : allPermit){
            if(p.getBonus().getNumberOfBonuses() > maxNumberOfBounuses) maxNumberOfBounuses = p.getBonus().getNumberOfBonuses();
        }
        Object[][] data = new Object [maxNumberOfBounuses+2][columnNames.length];
        int i = 0;
        for(PermitCard p : allPermit){
            String s = "";
            for(int j=0;j<p.getCities().length;j++){
                s = s + p.getCities()[j];
                if(j!=p.getCities().length-1) s = s + ",";
            }
            data[0][i] = s;
            data[1][i] = "";
            int k = 2;
            for (String string : p.getBonus().getSeparatedBonuses()) {
                data[k][i] = string;
                k++;
            }
            i++;
        }
        TextTable tt = new TextTable(columnNames, data);
        return tt;
    }


}
