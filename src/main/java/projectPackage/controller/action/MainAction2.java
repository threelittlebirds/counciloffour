package projectPackage.controller.action;

import projectPackage.controller.exceptions.*;
import projectPackage.model.*;
import projectPackage.model.cards.*;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class MainAction2 extends Action {

	private int whichCouncilRegion;
	private int whichPermitCard;


	public MainAction2(int whichCouncilRegion, int whichPermitCard, TurnMachine tm) {
		super(tm);
		this.whichCouncilRegion = whichCouncilRegion;
		this.whichPermitCard = whichPermitCard;
	}

	public int getWhichCouncilRegion() {
		return whichCouncilRegion;
	}

	public int getWhichPermitCard() {
		return whichPermitCard;
	}

	@Override
	public ResultMsgPair execute() throws CloneNotSupportedException {
		if(!enoughPoliticCards()) throw new NotEnoughPoliticCards();
		if(!thereIsPermitCard()) throw new MissingPermitCard();
		return tm.start(this);
	}

	@SuppressWarnings("unchecked")
	private boolean enoughPoliticCards(){
		Player currPlayer = tm.getGameBoard().getCurrPlayer();
		Council council = tm.getGameBoard().getCouncil(whichCouncilRegion);
		Deck<PoliticCard> politicCards = currPlayer.getPoliticDeck();//sono in aliasing? se si devo fare clone
		int cont = 0;
		for(Councillor c : council) {
			for(PoliticCard pc : politicCards){
				if(pc.getCardColor().equals(c.getColor())){
					cont++;
					break;
				}
			}
		}
		int multiCont = currPlayer.getNumMulticolor();
		int toPay = 0;
		while (cont<4 && multiCont>0) {
			cont++;
			multiCont--;
			toPay++;
		}
		if(cont == 0) return false;
		if(cont == 1 && currPlayer.getMoneyPosition().getPosition() < 10+toPay) return false;
		if(cont == 2 && currPlayer.getMoneyPosition().getPosition() < 7+toPay) return false;
		return !(cont == 3 && currPlayer.getMoneyPosition().getPosition() < 4 + toPay);
	}

	private boolean thereIsPermitCard(){
		if(this.whichPermitCard == 1 && tm.getGameBoard().getRegion(this.whichCouncilRegion).getFirstPermitCard() == null) return false;
		else if(tm.getGameBoard().getRegion(this.whichCouncilRegion).getSecondPermitCard() == null) return false;
		return true;
	}



}
