package projectPackage.model.bonus.decorators.specialDecorator;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.Coin;

public class CoinDecorator extends SpecialDecorator {

    public CoinDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {

        String city = (String) parameter;
        char shortCity = city.charAt(0);

        Coin b = getGameBoard().getCity(shortCity).getCoin();

        System.out.println("Activating bonus COIN BONUS\n");

        b.activateBonus();

    }

    @Override
    public String toStringDecorator() {
        return "Obtain the coin\n";
    }



}
