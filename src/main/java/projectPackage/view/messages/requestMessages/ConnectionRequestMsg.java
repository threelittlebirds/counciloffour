package projectPackage.view.messages.requestMessages;

import projectPackage.view.Token;

/**
 * Created by carme on 27/06/2016.
 */
/**
 * A message to request a connection to the server.
 */
public class ConnectionRequestMsg extends RequestMessage {

    private final String name;
    /**
     * Create a new ConnectionRequest request message.
     *
     * @param token the token that identifies the client
     */
    public ConnectionRequestMsg(Token token, String name) {

        super(token);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
