package projectPackage.model.bonus.decorators;

import projectPackage.model.bonus.Bonus;

public class AssistantDecorator extends BonusDecorator {

    private int number;

    public AssistantDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    protected void activateBonusDecorator() {
        getGameBoard().getCurrPlayer().acquireAssistant(number);
        System.out.println("Activating bonus ASSISTANT+"+number+"\n");
    }


    @Override
    public String toStringDecorator() {
        return "ASSISTANT +"+number;
    }

}

