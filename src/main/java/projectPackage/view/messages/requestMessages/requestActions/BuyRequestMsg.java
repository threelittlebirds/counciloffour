package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;
import projectPackage.model.utility.Pair;
import projectPackage.view.messages.MessageVisitor;

import java.util.HashMap;
import java.util.LinkedList;


public class BuyRequestMsg extends ActionRequestMsg {


    public BuyRequestMsg(Token token) {
        super(token);
    }

    @Override
    public Action createAction(ActionFactoryVisitorImpl visitor) {
        return visitor.visit(this);
    }

}
