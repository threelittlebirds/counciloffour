package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

import java.util.LinkedList;

/**
 * Created by Carmelo on 27/06/2016.
 */
public class AskToSellBroadcastMsg implements BroadcastMessage {

    private final int nextPlayer;
    private LinkedList<TextTable> separatePermitCards;
    private LinkedList<TextTable> separatePoliticCards;
    private TextTable assistants;
    private int numAssistants;

    public AskToSellBroadcastMsg(int nextPlayer, LinkedList<TextTable> separatePermitCards, LinkedList<TextTable> separatePoliticCards, TextTable assistants, int numAssistants) {
        this.nextPlayer = nextPlayer;
        this.separatePermitCards = separatePermitCards;
        this.separatePoliticCards = separatePoliticCards;
        this.assistants = assistants;
        this.numAssistants = numAssistants;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public int getNumAssistants() {
        return numAssistants;
    }

    public LinkedList<TextTable> getSeparatePermitCards() {
        return separatePermitCards;
    }

    public LinkedList<TextTable> getSeparatePoliticCards() {
        return separatePoliticCards;
    }

    public TextTable getAssistants() {
        return assistants;
    }


    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
