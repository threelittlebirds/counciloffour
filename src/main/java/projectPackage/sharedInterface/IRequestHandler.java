package projectPackage.sharedInterface;

import projectPackage.view.Token;
import projectPackage.view.messages.requestMessages.RequestMessage;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This remote interface handles the server component by receiving request messages from a
 * client and returning an appropriate response message.
 */
public interface IRequestHandler extends Remote {

        /**
         * Ask the server for a new connection by sending a ConnectRequestMsg with a null
         * Token (otherwise it would be a reconnection). A new Token will be returned, the
         * client must use it to identify himself.
         * @param name the name of the player
         * @return a new Token to identify the client
         *
         * @throws RemoteException if a problem occurs during the connection or if it times
         *                         out
         */
        Token connect(String name) throws RemoteException;

        /**
         * Send a request to be processed on the server and wait for a response.
         *
         * @param request the request to be processed by the server
         * @return the response obtained from the server
         *
         * @throws RemoteException if a problem occurs during the connection or if it times
         *                         out
         */
        ResponseMsg processRequest(RequestMessage request) throws RemoteException;


}
