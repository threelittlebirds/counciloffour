package projectPackage.view.messages.requestMessages;


import projectPackage.view.Token;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.requestMessages.RequestMessage;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 * Created by carme on 27/06/2016.
 */
public class InfoRequestMsg extends RequestMessage {

    private String info;

    public InfoRequestMsg(Token token, String info) {
        super(token);
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
