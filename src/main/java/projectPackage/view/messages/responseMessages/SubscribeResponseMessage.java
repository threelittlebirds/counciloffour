package projectPackage.view.messages.responseMessages;


import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.requestMessages.RequestMessage;

/**
 * Created by carme on 27/06/2016.
 */
public class SubscribeResponseMessage implements ResponseMsg {

    private final boolean success;

    public SubscribeResponseMessage(boolean success) {
        this.success = success;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

    public boolean isValid(RequestMessage msg){
        return true;
    }
}
