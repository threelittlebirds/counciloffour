package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.util.LinkedList;

public class AskQuickActionResponseMsg implements ResponseMsg {

    private boolean mainExecuted;
    private LinkedList<TextTable> permitCards; //quick2
    private TextTable sideCouncils; //quick3
    private TextTable councils; //quick3


    public AskQuickActionResponseMsg(boolean mainExecuted, LinkedList<TextTable> permitCards, TextTable sideCouncils, TextTable councils) {
        this.mainExecuted = mainExecuted;
        this.permitCards = permitCards;
        this.sideCouncils = sideCouncils;
        this.councils = councils;
    }

    public boolean isMainExecuted() {
        return mainExecuted;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }


    public LinkedList<TextTable> getPermitCards() {
        return permitCards;
    }

    public TextTable getSideCouncils() {
        return sideCouncils;
    }

    public TextTable getCouncils() {
        return councils;
    }
}
