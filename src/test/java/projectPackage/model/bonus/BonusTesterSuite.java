package projectPackage.model.bonus;



import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import projectPackage.model.bonus.decorators.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BonusDecoratorTest.class,
        SpecialDecoratorTest.class
})

public class BonusTesterSuite {


}
