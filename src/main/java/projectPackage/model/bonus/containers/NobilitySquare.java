package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.track.square.Square;
import projectPackage.model.utility.table.TextTable;

import javax.swing.table.TableModel;

public class NobilitySquare extends BonusContainer implements Square {

    private int position;

    public NobilitySquare(Bonus bonus, int position) {
        super(bonus);
        this.position = position;
    }
    public NobilitySquare(int position){
        super(null);
        this.position = position;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("NobilitySquare,");
        super.activateBonus();
    }

    public int getPosition(){
    	return position;
    }

    public String toString(){
        String[] columnNames = { "NobilitySquare "+position };
        int numberOfBonuses = bonus.getNumberOfBonuses();
        Object[][] data = new Object [++numberOfBonuses][columnNames.length];
        data[0][0] = "";
        //data[1][0] = bonus.toString();
        int i = 1;
        for (String string : bonus.getSeparatedBonuses()) {
            data[i][0] = string;
            i++;
        }
        TextTable tt = new TextTable(columnNames, data); // this adds the numbering on the left
        //tt.setAddRowNumbering(true); // sort by the first column
        //tt.setSort(0);
        tt.printTable();
        return "";
    }

}
