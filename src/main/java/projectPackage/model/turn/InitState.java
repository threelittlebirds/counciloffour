package projectPackage.model.turn;

import projectPackage.controller.action.*;

import projectPackage.model.GameBoard;
import projectPackage.view.messages.*;
import projectPackage.view.messages.responseMessages.responseActions.AskActionMessage;

class InitState extends TurnState {
	
	InitState(TurnMachine tm) {
		super(tm);
	}

	@Override
	public void start(Action a) {
		GameBoard gameBoard = tm.getGameBoard();
    	gameBoard.setCurrPlayer(tm.getGameBoard().getPlayers().get(0));
		gameBoard.printBoard().printTable();
	}

	@Override
	public ResultMsgPair nextState() throws CloneNotSupportedException{
		tm.setCurrentState(new BeginState(tm));
		responseMsg = new AskActionMessage(tm.getGameBoard().printBoard());
		resultMsgPair = new ResultMsgPair(responseMsg, null);
		return resultMsgPair;
	}
}
