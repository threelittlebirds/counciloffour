package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.model.turn.TurnState;
import projectPackage.view.messages.ResultMsgPair;

/**
 * Created by Med on 20/05/2016.
 */

public abstract class Action {
	
	protected TurnMachine tm;

    public Action(TurnMachine tm) {
		super();
		this.tm = tm;
	}

	public abstract ResultMsgPair execute() throws CloneNotSupportedException;
    

}
