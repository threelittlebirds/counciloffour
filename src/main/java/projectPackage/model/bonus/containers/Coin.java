package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.utility.table.TextTable;


public class Coin extends BonusContainer {

    public Coin(Bonus bonus) {
        super(bonus);
    }

	@Override
	public void activateBonus() throws CloneNotSupportedException {
		System.out.print("Coin,");
		super.activateBonus();
	}

	public String toString(){
		String[] columnNames = { "Coin" };
		int numberOfBonuses = bonus.getNumberOfBonuses();
		Object[][] data = new Object [++numberOfBonuses][columnNames.length];
		data[0][0] = "";
		//data[1][0] = bonus.toString();
		int i = 1;
		for (String string : bonus.getSeparatedBonuses()) {
			data[i][0] = string;
			i++;
		}
		TextTable tt = new TextTable(columnNames, data); // this adds the numbering on the left
		//tt.setAddRowNumbering(true); // sort by the first column
		//tt.setSort(0);
		tt.printTable();
		return "";
	}

}
