package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class MainAction2Message extends ActionRequestMsg {
	
	private int whichCouncilRegion;
	private int whichPermitCard;

	public MainAction2Message(Token token, int whichCouncilRegion, int whichPermitCard) {
		super(token);
		this.whichCouncilRegion = whichCouncilRegion;
		this.whichPermitCard = whichPermitCard;
	}
	
	public int getWhichCouncilRegion() {
		return whichCouncilRegion;
	}

	public int getWhichPermitCard() {
		return whichPermitCard;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
