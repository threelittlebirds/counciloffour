package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

import java.util.List;

/**
 * Created by Med on 03/07/2016.
 */
public class PublicLogBroadcastMsg implements BroadcastMessage {

    private int player;

    private List<BroadcastMessage> publicLog;

    public PublicLogBroadcastMsg(int player, List<BroadcastMessage> publicLog) {
        this.player = player;
        this.publicLog = publicLog;
    }

    @Override
    public void display(MessageVisitor visitor) {

    }
}
