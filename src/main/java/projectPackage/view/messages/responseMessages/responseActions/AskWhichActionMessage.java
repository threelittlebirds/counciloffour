package projectPackage.view.messages.responseMessages.responseActions;


import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

public class AskWhichActionMessage implements ResponseMsg {
	
	private int choiceAction;

	public AskWhichActionMessage( int choiceAction) {

		this.choiceAction = choiceAction;
	}

	public int getChoiceAction() {
		return choiceAction;
	}


	@Override
	public void display(MessageVisitor visitor) {
		visitor.display(this);
	}
}
