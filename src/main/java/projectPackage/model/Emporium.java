package projectPackage.model;

import java.awt.Color;
import java.io.Serializable;

import projectPackage.model.utility.ColorUtils;

class Emporium implements Serializable{
	
	private final Color color;
	
	Emporium(Color c){
		this.color = c;
	}

	public Color getColor() {
		return color;
	}

	public boolean equals(Object o) {
        if(!(o instanceof Emporium)) return false;
        if(o==this) return true;
        Emporium e = (Emporium)o;
		return e.color.equals(this.color);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder(200);
		sb.append("*******************************\n");
		sb.append("*                             *\n");
		sb.append("*                             *\n");
		if(color != null) sb.append("*          ").append(ColorUtils.getColorNameFromColor(color)).append("          *\n");
		else sb.append("*          Empty         *\n");
		sb.append("*                             *\n");
		sb.append("*                             *\n");
		sb.append("*******************************\n");
		return sb.toString();
	}

	@Override
	public int hashCode() {

		final int molt = 17;
		return molt*color.hashCode();

	}
}