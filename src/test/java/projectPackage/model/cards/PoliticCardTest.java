package projectPackage.model.cards;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

/**
 * Created by Med on 02/07/2016.
 */

public class PoliticCardTest {

    @Test
    public void getCardColor() throws Exception {

        PoliticCard pc = new PoliticCard(Color.GREEN);
        Color c = Color.GREEN;
        assertEquals(c, pc.getCardColor());
    }


}