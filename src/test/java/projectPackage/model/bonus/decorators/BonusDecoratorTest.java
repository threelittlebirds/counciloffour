package projectPackage.model.bonus.decorators;

import org.junit.Before;
import org.junit.Test;
import projectPackage.model.Player;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.BlankContainer;
import projectPackage.model.bonus.containers.NobilitySquare;
import projectPackage.model.track.square.MoneySquare;
import projectPackage.model.track.square.Square;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.Token;

import java.awt.*;
import java.util.LinkedList;
import java.util.UUID;

import static org.junit.Assert.*;


public class BonusDecoratorTest {

    Bonus b;
    TurnMachine tm;
    LinkedList<Player> players;
    Player currPlayer;

    @Before
    public void setUp() throws Exception {

        players = new LinkedList<>();
        players.add(new Player(new Token(UUID.randomUUID()), Color.BLACK));
        players.add(new Player(new Token(UUID.randomUUID()), Color.RED));
        players.add(new Player(new Token(UUID.randomUUID()), Color.GREEN));
        players.add(new Player(new Token(UUID.randomUUID()), Color.BLUE));
        tm = new TurnMachine("ORIGINAL.xml",players);

        b = new AssistantDecorator(new MoneyDecorator(new PoliticDecorator(new VictoryDecorator(new NobilityDecorator(new BlankContainer(tm),1),2),3),4),5);

        tm.getGameBoard().setCurrPlayer(tm.getGameBoard().getPlayers().getFirst());
        currPlayer = tm.getGameBoard().getCurrPlayer();

    }


    @Test
    public void activateBonus() throws Exception {

        int money = currPlayer.getMoneyPosition().getPosition();
        int assistant = currPlayer.getNumAssistant();
        int politic = currPlayer.getPoliticDeck().size();
        int nobility = currPlayer.getNobilityPosition().getPosition();
        int victory = currPlayer.getVictoryPosition().getPosition();

        b.activateBonus();

        int newMoney = currPlayer.getMoneyPosition().getPosition();
        int newAssistant = currPlayer.getNumAssistant();
        int newPolitic = currPlayer.getPoliticDeck().size();
        int newNobility = currPlayer.getNobilityPosition().getPosition();
        int newVictory = currPlayer.getVictoryPosition().getPosition();

        assertEquals(assistant+5, newAssistant);
        assertEquals(money+4, newMoney);
        assertEquals(politic+3, newPolitic);
        assertEquals(victory+2, newVictory);
        assertEquals(nobility+1, newNobility);


    }

    @Test
    public void hasNobilityDecorator() throws Exception {

        assertTrue(b.hasNobilityDecorator());

    }

    @Test
    public void setNobilityDecorator() throws Exception {

        b.setNobilityDecorator(false);
        assertFalse(b.hasNobilityDecorator());

    }

    @Test
    public void getNumberOfBonuses() throws Exception {

        assertEquals(5, b.getNumberOfBonuses());
    }

    @Test
    public void getSeparatedBonuses() throws Exception {

        LinkedList<String> list = b.getSeparatedBonuses();

        String assistant = "ASSISTANT +5";
        String money = "MONEY +4";
        String politic = "POLITIC CARDS +3";
        String victory = "VICTORY +2";
        String nobility= "NOBILITY +1";

        assertEquals(nobility, list.removeFirst());
        assertEquals(victory, list.removeFirst());
        assertEquals(politic, list.removeFirst());
        assertEquals(money, list.removeFirst());
        assertEquals(assistant, list.removeFirst());
    }

    @Test
    public void getTempBonus() throws Exception {

        BonusDecorator bonus = (BonusDecorator) b;

        assertEquals(MoneyDecorator.class, bonus.getTempBonus().getClass());

    }


}