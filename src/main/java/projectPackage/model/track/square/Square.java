package projectPackage.model.track.square;

import java.io.Serializable;

/**
 * Created by Med on 13/05/2016.
 */
public interface Square extends Serializable {

	int getPosition();
}
