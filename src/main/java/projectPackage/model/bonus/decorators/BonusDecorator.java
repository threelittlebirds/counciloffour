package projectPackage.model.bonus.decorators;

import projectPackage.model.GameBoard;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.BlankContainer;
import projectPackage.model.turn.TurnMachine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public abstract class BonusDecorator implements Bonus, Serializable {

    protected Bonus tempBonus;

    @Override
    public TurnMachine getTurnMachine() {
        return tempBonus.getTurnMachine();
    }

    public GameBoard getGameBoard() {
        return tempBonus.getGameBoard();
    }

    public BonusDecorator(Bonus newBonus) {

        this.tempBonus = newBonus;

    }

    @Override
    public  void activateBonus() throws CloneNotSupportedException {
        tempBonus.activateBonus();
        activateBonusDecorator();
    }

    protected abstract void activateBonusDecorator() throws CloneNotSupportedException;


    public boolean hasNobilityDecorator() {
        return tempBonus.hasNobilityDecorator();
    }

    @Override
    public void setNobilityDecorator(boolean value) {
        tempBonus.setNobilityDecorator(value);
    }

    public abstract String toStringDecorator();

    @Override
    public String toString() {
        String s = "";
        s = s + tempBonus.toString();
        s = s + toStringDecorator();
        return s;
    }

    @Override
    public int getNumberOfBonuses() {
        int n = tempBonus.getNumberOfBonuses();
        return ++n;
    }

    @Override
    public LinkedList<String> getSeparatedBonuses() {
        LinkedList<String> list = new LinkedList<>();
        list.addAll(tempBonus.getSeparatedBonuses());
        list.add(toStringDecorator());
        return list;
    }

    public Bonus getTempBonus() {
        return tempBonus;
    }
}
