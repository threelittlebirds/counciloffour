package projectPackage.model;

import projectPackage.model.utility.Util;

import java.awt.*;
import java.io.Serializable;

/**
 * This class represent a Councillor which has his color
 */
public class Councillor implements Serializable{

    private Color color;

    public Councillor(Color color) {
		super();
		this.color = color;
	}

	public Color getColor() {
        return color;
    }

    
    public String toString(){
		String sb = "********************\n" +
				"*     " + Util.getStringFromColor(color) + "    *\n" +
				"********************\n";
		return sb;
    }
}
