package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by Carmelo on 27/06/2016.
 */
public class QuickActionBroadcastMsg implements BroadcastMessage{

    private final int player;

    private final String message;

    private final boolean end;

    public QuickActionBroadcastMsg(int player, String message, boolean end) {
        this.player = player;
        this.message = message;
        this.end = end;
    }

    public int getPlayer() {
        return player;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

    public boolean isEnd() {
        return end;
    }
}
