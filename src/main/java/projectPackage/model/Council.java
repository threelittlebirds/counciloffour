package projectPackage.model;

import projectPackage.model.utility.Util;
import projectPackage.model.utility.table.TextTable;

import java.io.Serializable;
import java.util.*;


public class Council implements Iterable<Councillor>, Serializable {

    private LinkedList<Councillor> councillors;
    private TypeCouncil typeOfCouncil;

    public Council(LinkedList<Councillor> councillors, TypeCouncil typeOfCouncil) {
		super();
		this.councillors = councillors;
		this.typeOfCouncil = typeOfCouncil;
	}

    public void addFirst(Councillor c){
    	councillors.addFirst(c);
    }
    
    public void addLast(Councillor c){
    	councillors.addLast(c);
    }
    
    public TypeCouncil getTypeOfCouncil() {
		return typeOfCouncil;
	}
    
    public Councillor getFirst(){
    	return councillors.getFirst();
    }
    
    public Councillor getLast(){
    	return councillors.getLast();
    }
    
    public Councillor removeFirst(){
    	return councillors.removeFirst();
    }
    
    public Councillor removeLast(){
    	return councillors.removeLast();
    }
    
	public void setCouncillors(LinkedList<Councillor> councillors) {
		this.councillors = councillors;
	}

	public LinkedList<Councillor> getCouncillors() {
        return councillors;
    }

    public Councillor corrupt(Councillor councillor) {
        Councillor old = councillors.removeLast();
        councillors.addFirst(councillor);
        return old;
    }
    
    public String toString(){
        String[] columnNames = new String[councillors.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = ""+i;
        }
        Object[][] data = new Object [3][columnNames.length];
        for(int i=0;i<councillors.size();i++){
            data[0][i] = "";
            data[1][i] = Util.getStringFromColor(councillors.get(i).getColor());
            data[2][i] = "";
        }
        TextTable tt = new TextTable(columnNames, data);
        tt.printTable();
        return "";
    }

    @Override
    public Iterator<Councillor> iterator() {
        return councillors.iterator();
    }

}
