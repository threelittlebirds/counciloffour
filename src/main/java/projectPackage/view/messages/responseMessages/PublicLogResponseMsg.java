package projectPackage.view.messages.responseMessages;

import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.requestMessages.RequestMessage;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class PublicLogResponseMsg implements ResponseMsg {

    private final ArrayList<BroadcastMessage> log;

    public PublicLogResponseMsg(List<BroadcastMessage> log) {

        this.log = new ArrayList<>(log);

    }

    public List<BroadcastMessage> getLog() {

        return log;

    }

    @Override
    public void display(MessageVisitor visitor) {

        visitor.display(this);

    }

}