package projectPackage.view.gamePlayer.Cli;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.Token;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.broadcastMessage.*;
import projectPackage.view.messages.responseMessages.InvalidResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.*;
import projectPackage.view.messages.responseMessages.*;

/**
 * A visitor class for the Visitor Pattern to visit incoming messages and display them on
 * a Command Line Interface.
 */
public class CliMessageVisitor implements MessageVisitor {

    private int playerNum;

    public CliMessageVisitor() {

        playerNum = -1;

    }

    public int getPlayerNum() {

        return playerNum;

    }

    public void setPlayerNum(int playerNum) {

        this.playerNum = playerNum;

    }

    private void cli(String string) {

        System.out.println(string);

    }

    ////////////////////////////////////// BROADCAST \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @Override
    public void display(AskToSellBroadcastMsg msg) {
        if ((playerNum == msg.getNextPlayer())) {
            msg.getSeparatePermitCards().forEach(TextTable::printTable);
            msg.getSeparatePoliticCards().forEach(TextTable::printTable);
            msg.getAssistants().printTable();
            cli("Do you want to sell somethings?\n" +
                    "Type SELL to start selling or JUMP to jump sell phase!\n");


        }
    }

    @Override
    public void display(ChatBroadcastMsg msg) {

        String sb = "Player" +
                msg.getPlayer() +
                ": " +
                msg.getMessage();

        cli(sb);

    }

    @Override
    public void display(GeneralInfoBroadcastMsg msg) {

        if (!(playerNum == msg.getPlayerTurn())) {
            cli(msg.getMessage());
        }

    }

    @Override
    public void display(MainActionBroadcastMsg msg) {

        if (!(playerNum == msg.getPlayer())) {
            cli("Player " + msg.getMainSelected());
        }


    }

    @Override
    public void display(MarketBroadcastMsg msg) {  //UNUSED

        /*
        if (playerNum == msg.getNextPlayer()) {
            msg.getSeparatePermitCards().forEach(TextTable::printTable);
            msg.getSeparatePoliticCards().forEach(TextTable::printTable);
            msg.getAssistants().printTable();
            cli("Do you want to sell somethings?\n" +
                    "Type YES to start selling or NO to jump sell phase!\n");
        }
        */

    }

    @Override
    public void display(PermitAcquiredBroadcastMsg msg) {   //UNUSED

        if (playerNum == msg.getPlayer()) {
            return;
        }


    }

    public void display(AskToBuyBroadcastMsg msg){
        if (playerNum == msg.getNextPlayer()) {
            for (Token t : msg.getPermitToBuy().keySet()) {
                if (t.getPlayerNumber() == playerNum) {
                    msg.getPermitToBuy().get(t).printTable();
                    msg.getPoliticToBuy().get(t).printTable();
                    msg.getAssistantToBuy().get(t).printTable();
                }
            }
            cli("Do you want to sell somethings?\n" +
                    "Type YES to start selling or NO to jump sell phase!\n");
        }
    }

    @Override
    public void display(QuickActionBroadcastMsg msg) {

        if(msg.isEnd()){
            if (playerNum == msg.getPlayer()) {
                cli("Turn END!");
            }
        }else{
            if (playerNum == msg.getPlayer()) {
                cli("Quick Action done!");
            }else{
                cli(msg.getMessage());
            }
        }


    }

    @Override
    public void display(BonusActivatorBroadcast msg) {

        cli(msg.getMessage());

    }


    @Override
    public void display(AskMainQuickBroadcastMsg msg) {

        msg.getMap().forEach(TextTable::printTable);
        for (Integer i : msg.getPlayerSituation().keySet()) {
            if (playerNum == i) {
                for (TextTable tt : msg.getPlayerSituation().get(i)) {
                    tt.printTable();
                }
            }
        }
        if(playerNum == msg.getNextPlayer()){
            cli("Type MAIN  to do first the main action\n" +
                    "Type QUICK to do first the quick action\n");
        }

    }

    @Override
    public void display(FinishTurnMainBroadcastMsg msg) {
        if (playerNum == msg.getPlayerTurn()) {
            cli("Main Action done!");
        }
        if (msg.isEnd()) {
            if (playerNum == msg.getPlayerTurn()) {
                cli("Turn END!");
            }
            TextTable tt = msg.getMap().removeFirst();
            tt.setSort(1);
            tt.printTable();
            msg.getMap().forEach(TextTable::printTable);
            for (Integer i : msg.getPlayerSituation().keySet()) {
                if (playerNum == i) {
                    for (TextTable ttt : msg.getPlayerSituation().get(i)) {
                        ttt.printTable();
                    }
                }
            }
            if (playerNum == msg.getNextPlayer()) {

                cli("Type MAIN  to do first the main action\n" +
                        "Type QUICK to do first the quick action\n");
            }
        }
    }

    @Override
    public void display (FinishTurnQuickBroadcastMsg msg){
        if (playerNum == msg.getPlayerTurn()) {
            cli("Quick Action done!");
        }
        TextTable tt = msg.getMap().removeFirst();
        tt.setSort(1);
        tt.printTable();
        msg.getMap().forEach(TextTable::printTable);
        for (Integer i : msg.getPlayerSituation().keySet()) {
            if (playerNum == i) {
                for (TextTable ttt : msg.getPlayerSituation().get(i)) {
                        ttt.printTable();
                }
            }
        }
        if (playerNum == msg.getNextPlayer()) {

            cli("Type MAIN  to do first the main action\n" +
                    "Type QUICK to do first the quick action\n");
        }
    }


    @Override
    public void display(GameStartedBroadcastMsg msg) {

        StringBuilder sb = new StringBuilder();

        sb.append("Game");
        sb.append(msg.getGameNumber());
        sb.append(" started with map ");
        sb.append(msg.getMapName());
        sb.append(" and there are ");
        sb.append(msg.getNumberOfPlayers());
        sb.append(" players.");
        sb.append("\nThe turn starts from Player");
        sb.append(msg.getPlayerTurn());
        sb.append(".\n");
        cli(sb.toString());
        TextTable tt = msg.getMap().removeFirst();
        tt.setSort(1);
        tt.printTable();
        msg.getMap().forEach(TextTable::printTable);
        for (Integer i : msg.getPlayerSituation().keySet()){
            if (playerNum == i) {
                for (TextTable ttt : msg.getPlayerSituation().get(i)){
                    ttt.printTable();
                }
            }
        }
        if (playerNum == msg.getPlayerTurn()) {
            cli("Type MAIN  to do first the main action\n" +
                    "Type QUICK to do first the quick action\n");
        }

    }

    @Override
    public void display(GameFinishedBroadcastMsg msg) {
        cli("Game Finished! This is the winner and the looser!");
        msg.getWinnerLoser().printTable();

    }


    ////////////////////////////////////// RESPONSES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @Override
    public void display(AskActionMessage msg) {

        /*
        TextTable tt = msg.getCurrentMap();
        tt.setSort(0);
        tt.printTable();
            cli("Do you want to do first the MAIN action or the QUICK action?\n" +
                    "Type MAIN for Main action\n" +
                    "Type QUICK for Quick action");
                    */


    }

    @Override
    public void display(AskMainActionResponseMsg msg) {

        cli("Which main action do you want to do?\n" +
                "Type MAIN1 for: Elect a councillor\n" +
                "Type MAIN2 for: Acquire a business permit tile\n" +
                "Type MAIN3 for: Build an emporium using permit tile\n" +
                "Type MAIN4 for: Build an emporium with the help of the King\n");

    }

    @Override
    public void display(AskQuickActionResponseMsg msg) {
        cli("Which quick action do you want to choose?\n" +
                "Type QUICK1 for: Engage an assistant\n" +
                "Type QUICK2 for: Change permit tile\n" +
                "Type QUICK3 for: Send an assistant to elect a councillor\n" +
                "Type QUICK4 for: Perform an additional main action\n");
        if(msg.isMainExecuted()) cli("Type END for: Finish your turn!\n");
    }


    @Override
    public void display(InfoResponseMsg msg) {

        cli(msg.getGenericInfo());
    }

    @Override
    public void display(AskToBuyMessage msg) {
        cli("You are " + msg.getMoneyPosition() + " in the Money Track!");
        for (Token t : msg.getPermitToBuy().keySet()){
            if(t.getPlayerNumber() == playerNum){
                msg.getPermitToBuy().get(t).printTable();
                msg.getPoliticToBuy().get(t).printTable();
                msg.getAssistantToBuy().get(t).printTable();
            }
        }
        cli("Are you sure??\n" +
                "Type YES to start buying or NO to jump buy phase!\n");
    }

    @Override
    public void display(AskToSellMessage msg) {
        cli("You are " + msg.getMoneyPosition() + " in the Money Track!");
        //msg.getSeparatePermitCards().forEach(TextTable::printTable);
        //msg.getSeparatePoliticCards().forEach(TextTable::printTable);
        //msg.getAssistants().printTable();
        cli("Are you sure???\n" +
                "Type YES to start selling or NO to jump sell phase!\n");
    }

    @Override
    public void display(AskWhichActionMessage msg) {

        //TODO AskWhichActionMessage

    }

    @Override
    public void display(GoToEndMessage msg) {

        cli("\nFinish turn!\n");

    }

    @Override
    public void display(HandPermitDecoratorMessage msg) {

        //TODO HandPermitDecoratorMessage
    }

    @Override
    public void display(PermitDecoratorMessage msg) {

        //TODO PermitDecoratorMessage

    }

    @Override
    public void display(WhichOfYourCitiesMessage msg) {

        StringBuilder sb = new StringBuilder();

        //TODO WhichOfYourCitiesMessage

    }

    @Override
    public void display(ConnectionResponseMessage msg) {

        cli("Connected successfully to the server!\n");

    }

    @Override
    public void display(SubscribeResponseMessage msg) {

        // Do nothing

    }

    @Override
    public void display(PrivateDataResponseMsg msg) {

        TextTable tt = msg.getCurrentSituation();
        cli("This is your situation: \n");
        tt.setSort(0);
        tt.printTable();

    }

    @Override
    public void display(PublicLogResponseMsg msg) {

        if (msg.getLog().isEmpty()) {
            cli("Public log is currently empty.");
            return;
        }

        cli("######################### PRINTING LOG START #########################");
        cli("########################## PRINTING LOG END ##########################");

        for (BroadcastMessage message : msg.getLog()) {
            message.display(this);
        }

    }

    @Override
    public void display(InvalidResponseMsg msg) {

        StringBuilder sb = new StringBuilder();

        sb.append("ERROR: ");
        sb.append(msg.getReason());

        cli(sb.toString());

    }

    @Override
    public void display(BonusToFillMessage msg) {

        cli("*********************************************\n");
        cli("**                                         **\n");
        cli("**           CONGRATULATION!!!             **\n");
        cli("**         You've won a BONUS!!            **\n");
        cli("**                                         **\n");
        cli("**    Type 'Activate' to activate IT!      **\n");
        cli("**                                         **\n");
        cli("*********************************************\n");

    }
}

