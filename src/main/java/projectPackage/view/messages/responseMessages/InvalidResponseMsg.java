package projectPackage.view.messages.responseMessages;

import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 * This response message is sent to the client if his request was not valid. It
 * could contain a reason (mainly for debugging purposes).
 */
public class InvalidResponseMsg implements ResponseMsg{

    private final String reason;

    /**
     * Creates a new InvalidResponseMsg without a reason.
     */
    public InvalidResponseMsg() {

        this("");

    }

    /**
     * Creates a new InvalidResponseMsg containing a reason.
     *
     * @param reason the reason why the request was invalid
     */
    public InvalidResponseMsg(String reason) {

        this.reason = reason;

    }

    /**
     * Get the reason why the message is invalid.
     *
     * @return the reason why the request was invalid
     */
    public String getReason() {

        return reason;
    }

    @Override
    public void display(MessageVisitor visitor) {

        visitor.display(this);

    }
}
