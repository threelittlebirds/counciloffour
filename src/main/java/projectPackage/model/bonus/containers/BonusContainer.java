package projectPackage.model.bonus.containers;


import projectPackage.model.bonus.Bonus;
import java.io.Serializable;


public abstract class BonusContainer implements Serializable {

    public BonusContainer() {
    }

    protected Bonus bonus;

    public BonusContainer(Bonus b) {
        this.bonus = b;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public void activateBonus() throws CloneNotSupportedException {
        this.bonus.activateBonus();
    }
}
