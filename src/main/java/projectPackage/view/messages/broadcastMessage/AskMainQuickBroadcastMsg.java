package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * This is the broadcast message that Server send in the next state of every turn
 */
public class AskMainQuickBroadcastMsg implements BroadcastMessage {

    private final int nextPlayer;
    private final boolean isMarket;
    private final HashMap<Integer,LinkedList<TextTable>> playerSituation;
    private final LinkedList<TextTable> map;

    public AskMainQuickBroadcastMsg(boolean isMarket, int nextPlayer, HashMap<Integer, LinkedList<TextTable>> playerSituation, LinkedList<TextTable> map) {
        this.playerSituation = playerSituation;
        this.isMarket = isMarket;
        this.nextPlayer = nextPlayer;
        this.map = map;
    }

    public HashMap<Integer, LinkedList<TextTable>> getPlayerSituation() {
        return playerSituation;
    }

    public LinkedList<TextTable> getMap() {
        return map;
    }


    public int getNextPlayer() {
        return nextPlayer;
    }

    public boolean isMarket() {
        return isMarket;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
