package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class QuickAction2Message extends ActionRequestMsg {

	private int whichRegion;

	public QuickAction2Message(Token token, int whichRegion) {

		super(token);
		this.whichRegion = whichRegion;
	}

	public int getWhichRegion() {
		return whichRegion;
	}

	public void setWhichRegion(int whichRegion) {
		this.whichRegion = whichRegion;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
