package projectPackage.model.bonus.decorators;

import org.junit.Before;
import org.junit.Test;
import projectPackage.model.Player;
import projectPackage.model.TypeRegion;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.BlankContainer;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.bonus.decorators.specialDecorator.*;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.Token;

import java.awt.*;
import java.util.LinkedList;
import java.util.UUID;

import static org.junit.Assert.*;


public class SpecialDecoratorTest {

    Bonus b;
    SpecialDecorator bonus;
    TurnMachine tm;
    LinkedList<Player> players;
    Player currPlayer;

    LinkedList<Object> list;

    @Before
    public void setUp() throws Exception {

        players = new LinkedList<>();
        players.add(new Player(new Token(UUID.randomUUID()), Color.BLACK));
        players.add(new Player(new Token(UUID.randomUUID()), Color.RED));
        players.add(new Player(new Token(UUID.randomUUID()), Color.GREEN));
        players.add(new Player(new Token(UUID.randomUUID()), Color.BLUE));
        tm = new TurnMachine("ORIGINAL.xml", players);

        b = new HandPermitCardDecorator(new PermitCardDecorator(new CoinDecorator(new DoubleCoinDecorator(new BlankContainer(tm)))));
        bonus = (SpecialDecorator) b;

        tm.getGameBoard().setCurrPlayer(tm.getGameBoard().getPlayers().getFirst());
        currPlayer = tm.getGameBoard().getCurrPlayer();

        list = new LinkedList<>();

        char[] cities = {'a', 'b'};
        PermitCard pc1 = new PermitCard(new MoneyDecorator(new BlankContainer(tm), 1), cities);
        PermitCard pc2 = new PermitCard(new VictoryDecorator(new BlankContainer(tm), 1), cities);
        tm.getGameBoard().getRegion(TypeRegion.COAST).setFirstPermitCard(pc1);
        currPlayer.getPermitDeck().add(pc2);

        LinkedList<String> doubleList = new LinkedList<>();
        doubleList.addLast("ARKON");
        doubleList.addLast("CASTRUM");
        list.addLast(0);
        list.addLast(1);
        list.addLast("BURGEN");
        list.addLast(doubleList);

    }

    @Test
    public void activateBonusDecorator() throws Exception {

        int money = currPlayer.getMoneyPosition().getPosition();
        int victory = currPlayer.getVictoryPosition().getPosition();

        bonus.setParameters(list);
        bonus.activateBonus();

        int newMoney = currPlayer.getMoneyPosition().getPosition();
        int newVictory = currPlayer.getVictoryPosition().getPosition();


        assertEquals(money+4, newMoney);
        assertEquals(victory+6, newVictory);

    }

    @Test
    public void setParameters() throws Exception {

        bonus.setParameters(list);

        Object handPermitParameter = bonus.getParameter();
        bonus = (SpecialDecorator) bonus.getTempBonus();
        Object permitParameter = bonus.getParameter();
        bonus = (SpecialDecorator) bonus.getTempBonus();
        Object coinParameter = bonus.getParameter();
        bonus = (SpecialDecorator) bonus.getTempBonus();
        Object doubleCoinParameter = bonus.getParameter();

        LinkedList<String> tempList = new LinkedList<>();
        tempList.addLast("ARKON");
        tempList.addLast("CASTRUM");


        assertEquals(0, handPermitParameter);
        assertEquals(1, permitParameter);
        assertEquals(tempList, doubleCoinParameter);
        assertEquals("BURGEN", coinParameter);

    }

    @Test
    public void getSeparatedBonuses() throws Exception {

        LinkedList<String> list = b.getSeparatedBonuses();

        String coint = "Obtain the coin\n";
        String doubleCoin = "Obtain the coin of 2 cities\n";
        String permit = "Activate a face-up Permit Card on the gameboard.\n";
        String handPermit = "Activate one of your Permit Cards\n";

        assertEquals(doubleCoin, list.removeFirst());
        assertEquals(coint, list.removeFirst());
        assertEquals(permit, list.removeFirst());
        assertEquals(handPermit, list.removeFirst());

    }


}