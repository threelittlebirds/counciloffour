package projectPackage.view.messages;

import projectPackage.view.messages.broadcastMessage.*;
import projectPackage.view.messages.responseMessages.InvalidResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.*;
import projectPackage.view.messages.responseMessages.*;

/**
 * A visitor class for the Visitor Pattern to visit incoming messages and display them on
 * a Command Line Interface.
 */
public interface MessageVisitor {

    ////////////////////////////////////// BROADCAST \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /**
     * Display the content of an AskToSellBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(AskToSellBroadcastMsg msg);

    /**
     * Display the content of an UseSptItemResponseMsg.
     *
     * @param msg the received message
     */
    void display(GameStartedBroadcastMsg msg);

    /**
     * Display the content of an AskMainQuickBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(AskMainQuickBroadcastMsg msg);

    /**
     * Display the content of an InfoResponseMsg.
     *
     * @param msg the received message
     */
    void display(InfoResponseMsg msg);


    /**
     * Display the content of a ChatBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(ChatBroadcastMsg msg);

    /**
     * Display the content of a MainActionBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(MainActionBroadcastMsg msg);

    /**
     * Display the content of a PermitAcquiredBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(PermitAcquiredBroadcastMsg msg);

    /**
     * Display the content of an QuickActionBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(QuickActionBroadcastMsg msg);

    /**
     * Display the content of a MarketBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(MarketBroadcastMsg msg);


    /**
     * Display the content of a GameFinishedBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(GameFinishedBroadcastMsg msg);


    /**
     * Display the content of a GeneralInfoBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(GeneralInfoBroadcastMsg msg);

    ////////////////////////////////////// REQUESTS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /**
     * Display the content of an AskActionMessage.
     *
     * @param msg the received message
     */
    void display(AskActionMessage msg);


    /**
     * Display the content of a AskMainActionResponseMsg.
     *
     * @param msg the received message
     */
    void display(AskMainActionResponseMsg msg);

    /**
     * Display the content of a AskQuickActionResponseMsg.
     *
     * @param msg the received message
     */
    void display(AskQuickActionResponseMsg msg);

    /**
     * Display the content of a AskToBuyMessage.
     *
     * @param msg the received message
     */
    void display(AskToBuyMessage msg);

    /**
     * Display the content of a AskToSellMessage.
     *
     * @param msg the received message
     */
    void display(AskToSellMessage msg);

    /**
     * Display the content of a AskWhichActionMessage.
     *
     * @param msg the received message
     */
    void display(AskWhichActionMessage msg);

    /**
     * Display the content of an GoToEndMessage.
     *
     * @param msg the received message
     */
    void display(GoToEndMessage msg);

    /**
     * Display the content of an HandPermitDecoratorMessage.
     *
     * @param msg the received message
     */
    void display(HandPermitDecoratorMessage msg);

    /**
     * Display the content of an PermitDecoratorMessage.
     *
     * @param msg the received message
     */
    void display(PermitDecoratorMessage msg);

    /**
     * Display the content of an WhichOfYourCitiesMessage.
     *
     * @param msg the received message
     */
    void display(WhichOfYourCitiesMessage msg);


    /////////////////////////////// RESPONSES (non-actions) \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /**
     * Display the content of a ConnectionResponseMsg.
     *
     * @param msg the received message
     */
    void display(ConnectionResponseMessage msg);

    /**
     * Display the content of a InvalidResponseMsg.
     *
     * @param msg the received message
     */
    void display(InvalidResponseMsg msg);

    /**
     * Display the content of a SubscribeResponseMsg.
     *
     * @param msg the received message
     */
    void display(SubscribeResponseMessage msg);


    /**
     * Display the content of a PrivateDataResponseMsg.
     *
     * @param msg the received message
     */
    void display(PrivateDataResponseMsg msg);

    /**
     * Display the content of a PublicLogResponseMsg.
     *
     * @param msg the received message
     */
    void display(PublicLogResponseMsg msg);

    /**
     *
     *
     *
     */
    void display(BonusToFillMessage msg);

    /**
     * Set the player number of this client.
     *
     * @param playerNum the player number of this client
     */
    void setPlayerNum(int playerNum);

    /**
     * Get the player number of this client.
     *
     * @return the player number of this client
     */
    int getPlayerNum();

    /**
     * Display the content of a FinishTurnMainBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(FinishTurnMainBroadcastMsg msg);

    /**
     * Display the content of a FinishTurnQuickBroadcastMsg.
     *
     * @param msg the received message
     */
    void display(FinishTurnQuickBroadcastMsg msg);

    /**
     * Display the content of a BonusActivatorBroadcast.
     *
     * @param msg the received message
     */
    void display(BonusActivatorBroadcast msg);

    void display(AskToBuyBroadcastMsg msg);
}
