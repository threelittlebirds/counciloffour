package projectPackage.controller;

import projectPackage.controller.action.*;
import projectPackage.controller.exceptions.*;
import projectPackage.model.GameBoard;
import projectPackage.model.Player;
import projectPackage.model.turn.FinishedState;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.turn.TurnMachine;
import projectPackage.model.utility.ColorUtils;
import projectPackage.model.utility.table.TextTable;
import projectPackage.sharedInterface.IPublisher;
import projectPackage.view.Token;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.*;
import projectPackage.view.messages.requestMessages.*;
import projectPackage.view.messages.requestMessages.requestActions.MainAction2Message;
import projectPackage.view.messages.responseMessages.*;
import projectPackage.view.messages.requestMessages.requestActions.ActionRequestMsg;
import projectPackage.view.messages.responseMessages.responseActions.AskMainActionResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.BonusToFillMessage;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The controller for a single game, it manages the requests obtained by the GameManager
 * (that dispatches messages to the correct game) and manages the execution of actions by
 * using a TurnMachine.
 */
public class Controller {

    private static final Logger LOG = Logger.getLogger(Controller.class.getName());

    private static int numberOfGames = 1;
    private final int gameID;
    private TurnMachine turnMachine;
    private ActionFactoryVisitorImpl visitor;
    private String mapSelected = "ORIGINAL.xml";
    private ResultMsgPair tempResult;
    private LinkedList<Bonus> specialBonus;

    private final List<Token> players;

    private final IPublisher publisherInterface;

    private final List<BroadcastMessage> publicLog;

    private boolean [] quickDone;

    /**
     * Create a new gameController, the game is initially instantiated with a standard
     * decks factory, a standard players factory and the map Galilei (can be changed by
     * clients).
     *
     * @param publisherInterface the publisher interface to notify subscribed clients
     */
    public Controller(IPublisher publisherInterface) {

        // Make a copy, minimize mutability
        this.players = new LinkedList<>();
        this.publisherInterface = publisherInterface;

        gameID = numberOfGames;
        numberOfGames++;

        // The log of executed public actions
        publicLog = new LinkedList<>();

        // Create a new GAMEx topic.
        publisherInterface.addTopic(getTopic());

        quickDone = new boolean[4];

    }

    /**
     * Change the map (zone) where the game will be played, can be done only before
     * starting the game.
     *
     * @param request the map change request message
     * @return the appropriate response message
     */
    private ResponseMsg changeMap(ChangeMapRequestMsg request) {

        // Only if the game did not already started
        if (turnMachine == null) {

            if (!(players.indexOf(request.getToken()) == 0)) {
                return new InvalidResponseMsg("Only the first player can change map.");
            }

            try {
                mapSelected=request.getMapName();
                return new InfoResponseMsg("Map changed to " + mapSelected);
            } catch (Exception e) {
                LOG.log(Level.WARNING, "There was a problem changing the map.");
            }
        }

        return new InvalidResponseMsg("Can't change map.");

    }

    /**
     * Add a player to the game (only if the game did not already started).
     *
     * @param token the token of the player to add
     */
    void addPlayer(Token token) {

        if (turnMachine == null) {
            players.add(token);
            // Subscribe the client to the topic of this game
            publisherInterface.subscribeClientToTopic(getTopic(), token);
        }

    }

    /**
     * Initialize the game model and start the turn machine.
     */
    ResultMsgPair initGame() {

        ResultMsgPair result=null;
        LinkedList<Player> allPlayers = new LinkedList<>();
        //Generate casual colors for Players
        LinkedList<Color> availableColors = new LinkedList<>();
        ArrayList<ColorUtils.ColorName> colors = ColorUtils.initColorList();
        for(ColorUtils.ColorName c : colors){
            Color col = new Color(c.getR(),c.getG(),c.getB());
            availableColors.add(col);
        }
        //Generate every Player
        for (Token t : players) {
            Player p = new Player(t,availableColors.remove(0));
            allPlayers.add(p);
        }
        turnMachine = new TurnMachine(mapSelected, allPlayers);
        visitor = new ActionFactoryVisitorImpl(turnMachine);
        turnMachine.getGameBoard().drawInitPoliticCards();
        HashMap<Integer, LinkedList<TextTable>> playersSituation = turnMachine.getGameBoard().printPlayersSituation();
        LinkedList<TextTable> map = turnMachine.getGameBoard().printMap();
        specialBonus = turnMachine.getGameBoard().getSpecialBonus();
        //Send a GameStartedBroadcastMessage to all players
        publisherInterface.send(new GameStartedBroadcastMsg(getGameID(), players.size(), mapSelected, 0,
                playersSituation, map), getTopic());

        //Set the player to the game and start
        try {
            result = turnMachine.start(new InitAction(allPlayers, turnMachine));
        }catch (CloneNotSupportedException e){
            LOG.log(Level.SEVERE, "Failing to start the game", e);
        }
        return result;
    }

    /**
     * Get the progressive ID of this game.
     *
     * @return the ID of this game
     */
    private int getGameID() {

        return gameID;

    }

    /**
     * Get the topic of this game for publisher-subscriber communication.
     *
     * @return the main topic of this game
     */
    private String getTopic() {

        return "GAME" + Integer.toString(gameID);

    }

    /**
     * Finish a game by setting the turnMachine to a finished state and broadcasting an
     * end game message to everyone.
     */
    private void finishGame() {

        TextTable winnerLoser = turnMachine.getGameBoard().printPlayers();

        turnMachine.setCurrentState(new FinishedState(turnMachine));

        publisherInterface.send(new GameFinishedBroadcastMsg(winnerLoser), getTopic());

    }


    /**
     * Check if the game is finished.
     *
     * @return true, if the game is finished
     * */

    private boolean isGameFinished() {

        return turnMachine.getGameBoard().isFinished();

    }



    /**
     * Check if it is the turn of the player with the given token
     *
     * @param token the token of the player to check
     * @return true, if it is his turn
     */
    private boolean isPlayerTurn(Token token) {

        return turnMachine.getGameBoard().getCurrPlayer().getToken().equals(token);

    }

    /**
     * Handle a request from the client, a Message is passed to a Factory that
     * uses the Visitor pattern to create an appropriate atomic Action (command
     * pattern), then the action is passed to a FSM (state pattern) to be
     * executed.
     *
     * @param request the request message from the client
     * @return the result of the execution of the message (response & broadcast)
     */
    private ResultMsgPair executeAction(ActionRequestMsg request) {

        Action action;

        try {

            // Create the appropriate action using the Visitor Pattern
            action = request.createAction(visitor);

            // Execute the action and return a result message pair (private
            // response + public broadcast)

            return action.execute();

        }
        catch (InvalidMsgException | CloneNotSupportedException e) {
            if(e instanceof InvalidMsgException) LOG.log(Level.WARNING, "Handled an invalid request.", e);
            else LOG.log(Level.WARNING, "Clone not supported exception in Controller", e);

            // Notify the client that the message is not valid
            return new ResultMsgPair(new InvalidResponseMsg("Invalid request content."), null);

        }

        /*
        catch(Exception e){
            if(e instanceof InvalidMsgException) LOG.log(Level.WARNING, "Handled an invalid request.", e);
            if(e instanceof CloneNotSupportedException) LOG.log(Level.WARNING, "Clone not supported exception in Controller", e);
            //exceptions(e,action);

            return new ResultMsgPair(new InvalidResponseMsg("Invalid request content."), null);

        }
        */
    }

    public void exceptions(RuntimeException e, Action a){
        if(a instanceof MainAction1) {
            System.out.println("Can't be exception here!");
        }
        if(a instanceof MainAction2) {
            if (e instanceof NotEnoughPoliticCards) {
                System.out.println("You don't have enough Politic Cards for corrupt this council! Change Council!");
            }
            if (e instanceof MissingPermitCard) {
                System.out.println("Permit Card not found! Choose an other Permit Card!");
            }
        }
        if(a instanceof MainAction3) {
            if (e instanceof AlreadyBuiltException) {
                System.out.println("You have already built in this city! Choose an other city!");
            }
            if (e instanceof NotEnoughAssistants) {
                System.out.println("You can't choose this city because you don't have enough assistants! Choose an other city!");
            }
        }
        if(a instanceof MainAction4) {
            if (e instanceof NotEnoughPoliticCards) {
                System.out.println("You can't do this action because you don't have enough Politic Cards!");
            }
            if (e instanceof NotEnoughMoneyForMove) {
                System.out.println("You don't have enough money for move the king in this city! Change city!");
            }
            if (e instanceof AlreadyBuiltException) {
                System.out.println("You have already built in this city! Choose an other city!");
            }
            if (e instanceof NotEnoughAssistants) {
                System.out.println("You can't choose this city because you don't have enough assistants! Choose an other city!");
            }
        }
        if(a instanceof QuickAction1) {
            if (e instanceof NotEnoughMoney) {
                System.out.println("You can't do this action because you don't have enough money!");
                quickDone[0] = true;
            }
            quickDone[0] = true;
        }
        if(a instanceof QuickAction2) {
            if (e instanceof NotEnoughAssistants) {
                System.out.println("You can't do this action because you don't have enough assistants!");
            }
            quickDone[1] = true;
        }
        if(a instanceof QuickAction3) {
            if (e instanceof NotEnoughAssistants) {
                System.out.println("You can't do this action because you don't have enough assistants!");
            }
            quickDone[2] = true;
        }
        if(a instanceof QuickAction4) {
            if (e instanceof NotEnoughAssistants) {
                System.out.println("You can't do this action because you don't have enough assistants!");
            }
            quickDone[3] = true;
        }
    }

    public boolean allQuickActionTried(){
        boolean flag = true;
        for(int i=0;i<quickDone.length;i++){
            if(!quickDone[i]) flag = false;
        }
        return flag;
    }

    /**
     * Fetch the private data of a single player upon request.
     *
     * @param request the request message
     * @return the appropriate response containing player's private data
     */
    private ResponseMsg fetchPrivateData(PrivateDataRequestMsg request) {



        if (turnMachine == null) {
            return new InvalidResponseMsg("The game is not started yet.");
        }

        Token playerToken = request.getToken();

        TextTable textTable = turnMachine.getGameBoard().printPlayer(playerToken);


        return new PrivateDataResponseMsg(textTable);

    }

    /**
     * Handles a chat message by sending an acknowledgment to th sender and broadcasting
     * it to the other players of the game.
     *
     * @param request the chat request
     * @return the acknowledgment
     */
    private ResponseMsg handleChatMessage(SendChatMsg request) {

        int clientNum = players.indexOf(request.getToken());
        BroadcastMessage broadcast = new ChatBroadcastMsg(request.getToken().getPlayerName(), clientNum, request.getChatMessage());
        publisherInterface.send(broadcast, getTopic());
        return new InfoResponseMsg("Chat message delivered.");

    }

    /**
     * Handle a generic request coming from a client, and generate a response to be sent
     * back to the player and, if necessary, broadcast a message to all the clients
     * subscribed to a certain topic.
     *
     * @param request the request
     * @return the appropriate response
     */
    ResponseMsg handleRequest(RequestMessage request) {


        // ########################################### Requests that can always be handled

        if (request instanceof SendChatMsg) {
            return handleChatMessage((SendChatMsg) request);
        }

        if (request instanceof ChangeMapRequestMsg) {
            return changeMap((ChangeMapRequestMsg) request);
        }

        // ################################## Requests that require the game to be started

        if (turnMachine.getGameBoard() == null) {
            return new InvalidResponseMsg("Please wait for the game to start.");
        }

        if (request instanceof PrivateDataRequestMsg) {
            return fetchPrivateData((PrivateDataRequestMsg) request);
        }

        if (request instanceof PublicLogRequestMsg) {
            return new PublicLogResponseMsg(publicLog);
        }

        // ############################# Requests that require the player playing the turn

        if (!isPlayerTurn(request.getToken())) {
            return new InvalidResponseMsg("Please wait your turn to play.");
        }

        if (request instanceof ActionRequestMsg) {

            ResultMsgPair result = executeAction((ActionRequestMsg) request);


            if (result.getPrivateMessage() instanceof SuccessfulBonusActivationMessage) {
                result = tempResult;
            }

            if (!specialBonus.isEmpty()) {

                tempResult = result;

                result = new ResultMsgPair(new BonusToFillMessage(turnMachine.getGameBoard().printCitiesOfPlayerExceptNobilityDec(),
                        turnMachine.getGameBoard().printPermitCardsOfRegions(),
                        turnMachine.getGameBoard().printAllPersonalPermit(),
                        turnMachine.getGameBoard().numOfAllPermit(),
                        turnMachine.getGameBoard().nameOfCitiesOfPlayerExceptNobilityDec(),
                        specialBonus.getFirst().getSeparatedBonuses())
                        , new BonusActivatorBroadcast(turnMachine.getGameBoard().getCurrPlayer().getToken().getPlayerNumber() ,
                        turnMachine.getGameBoard().getCurrPlayer().getName()));


            }

            if (turnMachine.getActionBonus() > 0) {
                tempResult = result;
                GameBoard gameBoard = turnMachine.getGameBoard();
                BroadcastMessage broadcastMessage = new GeneralInfoBroadcastMsg(gameBoard.indexOfCurrentPlayer(), gameBoard.getCurrPlayer().getName() + " had an addition main action.");
                ResponseMsg responseMsg = new AskMainActionResponseMsg(gameBoard.printSideCouncillors(),gameBoard.printAllCouncils(),
                        gameBoard.printRegionCouncils(),gameBoard.printPermitCardsOfRegions(),
                        gameBoard.printPersonalPermitCards(),gameBoard.getCurrPlayer().getPermitDeck(),
                        gameBoard.printBoard(),gameBoard.getNameOfCities());
                result = new ResultMsgPair(responseMsg, broadcastMessage);
            }
            if (isGameFinished()) {
                finishGame();
            }
            else{
                if (result.getBroadcastMessage()!= null) {
                    // Publish a public message
                    publisherInterface.send(result.getBroadcastMessage(), getTopic());
                    // Add it to the public log
                    publicLog.add(result.getBroadcastMessage());
                }
                return result.getPrivateMessage();
            }


        }

        // BTW this Should never be reached
        return null;

    }


}
