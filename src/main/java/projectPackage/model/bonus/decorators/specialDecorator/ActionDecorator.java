package projectPackage.model.bonus.decorators.specialDecorator;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.turn.MainState;


public class ActionDecorator extends SpecialDecorator {

    public ActionDecorator(Bonus newBonus) {
        super(newBonus);
    }


    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {

        /*
        GameBoard gameBoard = getGameBoard();
        ResponseMsg m = new AskMainActionResponseMsg(gameBoard.printSideCouncillors(),gameBoard.printAllCouncils(),
                gameBoard.printRegionCouncils(),gameBoard.printPermitCardsOfRegions(),gameBoard.printPersonalPermitCards(),
                gameBoard.getCurrPlayer().getPermitDeck(),gameBoard.printMap(),gameBoard.getNameOfCities());
        */

        getTurnMachine().setCurrentState(new MainState(getTurnMachine()));
        getTurnMachine().addActionBonus();
        System.out.println("Activating bonus ADDITIONAL MAIN ACTION\n");

    }



    @Override
    public String toStringDecorator() {
        return "Additional main action bonus";
    }



}
