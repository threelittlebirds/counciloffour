package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;
import projectPackage.view.messages.requestMessages.RequestMessage;

/**
 * This is the class for request messages from the client to the server that
 * involves performing an responseActions on the game. Subclasses must implement the
 * createAction method so the correct Action can be created
 * polymorphically without knowing the type of request message thanks to the
 * visitor pattern.
 */
public abstract class ActionRequestMsg extends RequestMessage {

    public ActionRequestMsg(Token token) {
        super(token);
    }

    public abstract Action createAction(ActionFactoryVisitorImpl visitor);
}
