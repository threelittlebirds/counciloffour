package projectPackage.model.bonus.decorators;


import projectPackage.model.bonus.Bonus;

public class PoliticDecorator extends BonusDecorator {

    private int number;

    public PoliticDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    protected void activateBonusDecorator() {
        for (int i=0; i<number; i++) {
            getGameBoard().drawPoliticCard();
        }
        System.out.println("Activating bonus POLITIC+"+number+"\n");
    }

    @Override
    public String toStringDecorator() {
        return "POLITIC CARDS +"+number;
    }


}
