package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

import java.util.HashMap;
import java.util.Map;

/**
 * A message to broadcast the ending of a game.
 */
public class GameFinishedBroadcastMsg implements BroadcastMessage {

    private final TextTable winnerLoser;

    /**
     * Create a new GameFinishedBroadcastMsg.
     *
     * @param winnerLoser TextTable that show to all player winner and losers
     *
     */
    public GameFinishedBroadcastMsg(TextTable winnerLoser) {

        this.winnerLoser = winnerLoser;

    }

    /**
     * Get the TextTable that show to all player winner and losers
     *
     * @return the TextTable
     */
    public TextTable getWinnerLoser() {
        return winnerLoser;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
