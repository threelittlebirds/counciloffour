package projectPackage.view.gamePlayer;

import projectPackage.view.gamePlayer.Cli.CliParser;
import projectPackage.view.gamePlayer.Cli.CliUpdater;
import projectPackage.sharedInterface.IRequestHandler;
import projectPackage.view.messages.requestMessages.PrivateDataRequestMsg;
import projectPackage.view.messages.requestMessages.PublicLogRequestMsg;
import projectPackage.view.messages.requestMessages.RequestMessage;
import projectPackage.view.messages.requestMessages.SendChatMsg;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Entry point for a game player.
 */
public class GamePlayer {

    private static final Logger LOG = Logger.getLogger(GamePlayer.class.getName());

    private static ResponseMsg response; //risposta dal server

    private static ResponseMsg oldResponse;
    public static void main(String[] args) {

        ViewUpdater view = new CliUpdater();
        Scanner scanner = new Scanner(System.in);

        String connectionType;
        String name;

        do {
            System.out.println("Enter RMI or SOCKET to choose the connection type:");
            connectionType = scanner.nextLine();
        } while (!connectionType.toUpperCase().matches("^(RMI|SOCKET)$"));

        System.out.println("Welcome! What's your name?\n");
        name = new Scanner(System.in).nextLine();

        try {

            PlayerConnectionFactory playerConnectionFactory;

            if (connectionType.matches("^(RMI)$")) {
                playerConnectionFactory = new RMIFactory("localhost", view, name);
            } else {
                playerConnectionFactory = new SocketFactory("localhost", view, name);
            }



            IRequestHandler requestHandler = playerConnectionFactory.getRequestHandler();



            while (!playerConnectionFactory.getSubscriber().isGameFinished()) {

                String cmd = new Scanner(System.in).nextLine().toUpperCase();


                RequestMessage request = CliParser.parseString(playerConnectionFactory
                        .getToken(), cmd, response);

                if(request instanceof SendChatMsg || request instanceof PrivateDataRequestMsg ||
                        request instanceof PublicLogRequestMsg) oldResponse = response;

                if(request!= null) {

                    response =  requestHandler.processRequest(request);
                    view.update(response);

                    if(oldResponse != null){
                        view.update(oldResponse);
                        response = oldResponse;
                        oldResponse = null;
                    }

                }
                else System.out.println("ERROR: Invalid command");

            }


        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Can't establish an Socket connection.");
        } catch (NotBoundException e) {
            LOG.log(Level.SEVERE, "Can't establish an RMI connection.");
        }

    }

}
