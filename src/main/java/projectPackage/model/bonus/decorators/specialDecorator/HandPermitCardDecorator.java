package projectPackage.model.bonus.decorators.specialDecorator;


import projectPackage.model.Player;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.PermitCard;


public class HandPermitCardDecorator extends SpecialDecorator {

    public HandPermitCardDecorator(Bonus newBonus) {
        super(newBonus);
    }


    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException{

        int choice = (int) parameter;

        Player currPlayer = getGameBoard().getCurrPlayer();
        PermitCard pc;
        if (choice > getGameBoard().getCurrPlayer().getPermitDeck().size()) {
            choice = choice - getGameBoard().getCurrPlayer().getPermitDeck().size();
            pc = currPlayer.getGraveDeck().getCard(choice);
        }
        else {
            pc = currPlayer.getPermitDeck().getCard(choice);
        }

        System.out.println("Activating bonus HAND OR GRAVE PERMIT BONUS\n");

        pc.activateBonus();

    }

    @Override
    public String toStringDecorator() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("Activate one of your Permit Cards\n");
        return sb.toString();
    }

}
