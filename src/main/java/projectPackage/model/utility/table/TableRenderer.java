package projectPackage.model.utility.table;

import java.io.OutputStream;
import java.io.Writer;

public interface TableRenderer {
	void render(OutputStream ps, int indent);

	void render(Writer w, int indent);
}
