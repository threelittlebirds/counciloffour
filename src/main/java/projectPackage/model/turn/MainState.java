package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.model.*;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.broadcastMessage.FinishTurnMainBroadcastMsg;
import projectPackage.view.messages.broadcastMessage.GeneralInfoBroadcastMsg;
import projectPackage.view.messages.broadcastMessage.MainActionBroadcastMsg;
import projectPackage.view.messages.responseMessages.SuccessfulBonusActivationMessage;
import projectPackage.view.messages.responseMessages.responseActions.AskMainActionResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.AskQuickActionResponseMsg;
import projectPackage.view.messages.responseMessages.responseActions.GoToEndMessage;

public class MainState extends TurnState {

	private String currPlayer = tm.getGameBoard().getCurrPlayer().getName();

	private final int MAIN1 = 1;
	private final int MAIN2 = 2;
	private final int MAIN3 = 3;
	private final int MAIN4 = 4;

	private GameBoard gameBoard;
	private boolean isExecutingActionBonus = false;

    public MainState(TurnMachine tm) {
		super(tm);
		this.gameBoard = tm.getGameBoard();
    }


    @Override
    public void start(Action a) throws CloneNotSupportedException {
		if (!(tm.isQuick4() || tm.isExecutingActionBonus())) { //Player wants to be here!
			tm.setMainExecuted(true);
		}
		else if (tm.isExecutingActionBonus()) {
			tm.removeActionBonus();
		}
    	if(a instanceof MainAction1) {
    		mainAction1(a);
			broadcastMessage = createBroadcastMsg(MAIN1);
		}
    	if(a instanceof MainAction2 ){
    		mainAction2(a);
			broadcastMessage = createBroadcastMsg(MAIN2);
    	}
    	if(a instanceof MainAction3) {
    		mainAction3(a);
			broadcastMessage = createBroadcastMsg(MAIN3);
    	}
    	if(a instanceof MainAction4) {
    		mainAction4(a);
			broadcastMessage = createBroadcastMsg(MAIN4);
    	}
		System.out.println(tm.getGameBoard().getCurrPlayer());
    }


	@Override
	public ResultMsgPair nextState() throws  CloneNotSupportedException {
		if (tm.isQuick4()) {		//if you have done the Quick Action 4
			tm.setQuick4(false);
			if (!tm.isMainExecuted()) {
				responseMsg = new AskMainActionResponseMsg(gameBoard.printSideCouncillors(),gameBoard.printAllCouncils(),gameBoard.printRegionCouncils(),gameBoard.printPermitCardsOfRegions(),gameBoard.printPersonalPermitCards(),gameBoard.getCurrPlayer().getPermitDeck(),gameBoard.printBoard(),gameBoard.getNameOfCities());

				//broadcast setting in action
			}
			else {
				tm.setCurrentState(new EndState(tm));
				return tm.start(new EndAction(tm));
				/*
				tm.setCurrentState(new EndState(tm));
				responseMsg = new GoToEndMessage(gameBoard.getCurrPlayer().getName());
				broadcastMessage = new FinishTurnMainBroadcastMsg(gameBoard.indexOfCurrentPlayer(), gameBoard.nextPlayer().getToken().getPlayerNumber(), gameBoard.printPlayersSituation(), gameBoard.printMap(), true);
				tm.start(new EndAction(tm));
				*/
			}
		}
		else if (tm.isExecutingActionBonus()) {
			responseMsg = new SuccessfulBonusActivationMessage();
            tm.setExecutingActionBonus(false);

		}
		else if (!tm.isQuickExecuted() && !tm.isJumpQuickAction()) {
			tm.setCurrentState(new QuickState(tm));
			responseMsg = new AskQuickActionResponseMsg(true,gameBoard.printPermitCardsOfRegions(),gameBoard.printSideCouncillors(),gameBoard.printAllCouncils());
		}
		else {
			tm.setCurrentState(new EndState(tm));
			return tm.start(new EndAction(tm));
			/*
			tm.setCurrentState(new EndState(tm));
			responseMsg = new GoToEndMessage(gameBoard.getCurrPlayer().getName());
			broadcastMessage = new FinishTurnMainBroadcastMsg(gameBoard.indexOfCurrentPlayer(), gameBoard.nextPlayer().getToken().getPlayerNumber(), gameBoard.printPlayersSituation(), gameBoard.printMap(), true);
			tm.start(new EndAction(tm));
			*/
		}
		resultMsgPair = new ResultMsgPair(responseMsg, broadcastMessage);
		return resultMsgPair;
	}

	private BroadcastMessage createBroadcastMsg(int witchMain) {
		if (!tm.isQuick4()) {
			if (!(tm.isExecutingActionBonus())) {
				broadcastMessage = new MainActionBroadcastMsg(currPlayer + " chose Main Action " + witchMain + "!", gameBoard.indexOfCurrentPlayer());
			}else broadcastMessage = new MainActionBroadcastMsg(currPlayer + " chose Main Action " + witchMain + " thanks to a Bonus!", gameBoard.indexOfCurrentPlayer());

		}else broadcastMessage = new MainActionBroadcastMsg(currPlayer + " chose Main Action " + witchMain + " thanks to Quick Action 4!", gameBoard.indexOfCurrentPlayer());
		return broadcastMessage;
	}

	private void mainAction1(Action a) {
		MainAction1 ma = (MainAction1)a;
		int whichCouncillor = ma.getWhichCouncillor();
		int whichCouncil = ma.getWhichCouncil();

		// corrompo il consiglio
		tm.getGameBoard().corruptCouncil(whichCouncil, whichCouncillor-1);

		//guadagnare 4 monete:
		tm.getGameBoard().moveOnMoneyTrack(4);

		//bisogna modificare anche il percorso ricchezza nel GameBoard?
		System.out.println("Main Action 1 done");
	}

	private void mainAction2(Action a) throws CloneNotSupportedException {
		MainAction2 ma = (MainAction2)a;
		int regionCouncil = ma.getWhichCouncilRegion();
		int permitCard = ma.getWhichPermitCard();
		tm.getGameBoard().satisfyCouncil(tm.getGameBoard().getCouncil(regionCouncil).getTypeOfCouncil());

		//Selezione Permit Card:
		tm.getGameBoard().obtainCard(regionCouncil, permitCard);
		System.out.println("Main Action 2 done");
	}
	
	private void mainAction3(Action a) throws CloneNotSupportedException {
		MainAction3 ma = (MainAction3) a;
		Player player = tm.getGameBoard().getCurrPlayer();
		PermitCard pc = player.getPermitDeck().removeCard(ma.getWhichPrivatePermitCard());
		player.getGraveDeck().add(pc);
		tm.getGameBoard().buildEmporium(ma.getWhichCharacter());// gia compreso il consumo di aiutanti per ogni altro emporio costruito degli altri giocatori
		System.out.println("Main Action 3 done");
	}

	private void mainAction4(Action a) throws CloneNotSupportedException {
		MainAction4 ma = (MainAction4) a;
		City destination = tm.getGameBoard().getCity(ma.getWhichCity());
		tm.getGameBoard().satisfyCouncil(TypeCouncil.KING);
		System.out.println("Main Action 4 done");
		int links = tm.getGameBoard().shortestPathOfKing(destination);
		tm.getGameBoard().getKing().setPosition(destination.getShortName());
		tm.getGameBoard().moveOnMoneyTrack(-links*2);
		tm.getGameBoard().buildEmporium(destination.getShortName());

	}

}
