package projectPackage.controller.action;

import projectPackage.controller.exceptions.AlreadyBuiltException;
import projectPackage.controller.exceptions.NotEnoughAssistants;
import projectPackage.model.Player;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

public class MainAction3 extends Action {

	private int whichPrivatePermitCard;
	private char whichCharacter;


	public MainAction3(int whichPrivatePermitCard, char whichCharacter, TurnMachine tm) {
		super(tm);
		this.whichPrivatePermitCard = whichPrivatePermitCard;
		this.whichCharacter = whichCharacter;
	}


	public int getWhichPrivatePermitCard() {
		return whichPrivatePermitCard;
	}

	public char getWhichCharacter() {
		return whichCharacter;
	}

	@Override
	public ResultMsgPair execute() throws CloneNotSupportedException {
		if (alreadyBuilt()) throw new AlreadyBuiltException();
		if(!enoughAssistant()) throw new NotEnoughAssistants();
		return tm.start(this);
	}

	private boolean alreadyBuilt(){
		return tm.getGameBoard().alreadyBuilt(whichCharacter);
	}

	private boolean enoughAssistant(){
		Player currPlayer = tm.getGameBoard().getCurrPlayer();
		return currPlayer.getNumAssistant() >= tm.getGameBoard().getCity(whichCharacter).numberOfOthersEmporiums(currPlayer.getColor());
	}


}