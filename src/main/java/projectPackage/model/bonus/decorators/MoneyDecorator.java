package projectPackage.model.bonus.decorators;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.track.square.MoneySquare;
import projectPackage.model.track.square.Square;


public class MoneyDecorator extends BonusDecorator  {

    private int number;

    public MoneyDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    public void activateBonusDecorator (){
        getGameBoard().moveOnMoneyTrack(number);
        System.out.println("Activating bonus MONEY+"+number+"\n");
    }

    @Override
    public String toStringDecorator() {
        return "MONEY +"+number;
    }
}
