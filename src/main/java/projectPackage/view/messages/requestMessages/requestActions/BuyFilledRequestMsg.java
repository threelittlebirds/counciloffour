package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.model.utility.Pair;
import projectPackage.view.Token;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by carme on 07/07/2016.
 */
public class BuyFilledRequestMsg extends ActionRequestMsg {

    /**
     * This HashMap is used for storage in each row a token, that
     * identifies the Seller, and the LinkedList that is used for
     * storage all things that the current player have bought.
     * LinkedList at position 0 : all Permit Card bought
     * LinkedList at position 1 : all Politic Card bought
     * LinkedList at position 2 : all Assistants bought
     */
    private HashMap<Token,LinkedList<LinkedList<Pair>>> staffToBuy;

    public BuyFilledRequestMsg(Token token, HashMap<Token, LinkedList<LinkedList<Pair>>> staffToBuy) {
        super(token);
        this.staffToBuy = staffToBuy;
    }

    public HashMap<Token, LinkedList<LinkedList<Pair>>> getStaffToBuy() {
        return staffToBuy;
    }

    public BuyFilledRequestMsg(Token token) {
        super(token);
    }

    @Override
    public Action createAction(ActionFactoryVisitorImpl visitor) {
        return visitor.visit(this);
    }
}
