package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

/**
 * A message to broadcast a text chat message.
 */
public class ChatBroadcastMsg implements BroadcastMessage {

    private final String player;
    private final int playerNum;
    private final String message;

    /**
     * Create a new ChatBroadcastMessage.
     *
     * @param player the number of the player sending the message
     * @param message the text of the message
     */
    public ChatBroadcastMsg(String player, int playerNum, String message) {

        this.player = player;
        this.playerNum = playerNum;
        this.message = message;

    }

    /**
     * Get the number of the player sending the message.
     *
     * @return the number of the player sending the message
     */
    public String getPlayer() {

        return player;

    }

    /**
     * Get the text of the message.
     *
     * @return the text of the message
     */
    public String getMessage() {

        return message;

    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}

