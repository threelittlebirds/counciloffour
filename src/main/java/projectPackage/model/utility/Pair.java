package projectPackage.model.utility;

import projectPackage.model.Player;

import java.io.Serializable;

/**
 * Created by carme on 20/06/2016.
 */
public class Pair implements Serializable{
    private int num;
    private int price;

    public Pair(int num, int price) {
        this.num = num;
        this.price = price;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "<N. assistants: " + num + " , Price: " + price + ">";
    }
}
