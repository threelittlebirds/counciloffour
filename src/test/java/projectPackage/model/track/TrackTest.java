package projectPackage.model.track;

import org.junit.Before;
import org.junit.Test;
import projectPackage.model.track.square.MoneySquare;

import static org.junit.Assert.*;


public class TrackTest {

    Track<MoneySquare> moneyTrack;
    MoneySquare square;

    @Before
    public void setUp() throws Exception {
        moneyTrack = new Track<>(5);
        square = new MoneySquare(1);
        moneyTrack.add(square);
    }

    @Test
    public void indexOf() throws Exception {
        assertEquals(0, moneyTrack.indexOf(square));
    }

    @Test
    public void contains() throws Exception {
        assertTrue(moneyTrack.contains(square));
    }

    @Test
    public void isEmpty() throws Exception {
        assertFalse(moneyTrack.isEmpty());
    }

    @Test
    public void getSquare() throws Exception {
        assertEquals(square, moneyTrack.getSquare(0));
    }


}