package projectPackage.model;

import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.bonus.containers.RegionCard;
import projectPackage.model.cards.Deck;
import projectPackage.model.utility.table.TextTable;

import java.io.Serializable;

public class Region implements Serializable {

    private TypeRegion type;

    private Council regionCouncil;

    private RegionCard regionPrize;

    private Deck<PermitCard> regionDeck;
    
    private PermitCard firstPermitCard;
    
    private PermitCard secondPermitCard;
    
	public Region(TypeRegion type, Council regionCouncil) {
		super();
		this.type = type;
		this.regionCouncil = regionCouncil;
	}

	public PermitCard getFirstPermitCard() {
		return firstPermitCard;
	}

	public void setFirstPermitCard(PermitCard firstPermitCard) {
		this.firstPermitCard = firstPermitCard;
	}

	public PermitCard getSecondPermitCard() {
		return secondPermitCard;
	}

	public void setSecondPermitCard(PermitCard secondPermitCard) {
		this.secondPermitCard = secondPermitCard;
	}

	public TypeRegion getType() {
		return type;
	}

	public Council getRegionCouncil() {
		return regionCouncil;
	}

	public void setRegionCouncil(Council regionCouncil) {
		this.regionCouncil = regionCouncil;
	}

	public RegionCard getRegionPrize() {
		return regionPrize;
	}

	public void setRegionPrize(RegionCard regionPrize) {
		this.regionPrize = regionPrize;
	}

	public Deck<PermitCard> getRegionDeck() {
		return regionDeck;
	}
	
	//pesco una carta permesso dal mazzo e la ritorno:
	public PermitCard pickACard(){
		if(regionDeck.isEmpty()) return null;
		return regionDeck.removeFirst();
	}

	public TextTable printPermitCards(){
		String[] columnNames = {"Region Type", "First Permit Card", "Second Permit Card"};
		Object[][] data;
		if(firstPermitCard == null && secondPermitCard == null){
			data = new Object[3][columnNames.length];
			data[0][0] = "";
			data[1][0] = type;
			data[2][0] = "";
			data[0][1] = "";
			data[1][1] = "Empty";
			data[2][1] = "";
			data[0][2] = "";
			data[1][2] = "Empty";
			data[2][2] = "";
		}
		else{
			int maxNumberOfBonuses = 3;
			if(firstPermitCard != null && firstPermitCard.getBonus().getNumberOfBonuses() > maxNumberOfBonuses) maxNumberOfBonuses = firstPermitCard.getBonus().getNumberOfBonuses();
			if(secondPermitCard != null && secondPermitCard.getBonus().getNumberOfBonuses() > maxNumberOfBonuses) maxNumberOfBonuses = secondPermitCard.getBonus().getNumberOfBonuses();
			data = new Object[maxNumberOfBonuses+2][columnNames.length];
			data[0][0] = "";
			data[1][0] = type;
			for(int i = 2; i<maxNumberOfBonuses;i++){
				data[i][0] = "";
			}
			if(firstPermitCard == null){
				data[0][1] = "";
				data[1][1] = "Empty";
				for(int i = 2; i<maxNumberOfBonuses;i++){
					data[i][1] = "";
				}
			}
			else{
				String s = "";
				for(int j=0;j<firstPermitCard.getCities().length;j++){
					s = s + firstPermitCard.getCities()[j];
					if(j!=firstPermitCard.getCities().length-1) s = s + ",";
				}
				data[0][1] = s;
				data[1][1] = "";
				int k = 2;
				for (String string : firstPermitCard.getBonus().getSeparatedBonuses()) {
					data[k][1] = string;
					k++;
				}
			}
			if(secondPermitCard == null){
				data[0][2] = "";
				data[1][2] = "Empty";
				for(int i = 2; i<maxNumberOfBonuses;i++){
					data[i][2] = "";
				}
			}
			else{
				String s = "";
				for(int j=0;j<secondPermitCard.getCities().length;j++){
					s = s + secondPermitCard.getCities()[j];
					if(j!=secondPermitCard.getCities().length-1) s = s + ",";
				}
				data[0][2] = s;
				data[1][2] = "";
				int k = 2;
				for (String string : secondPermitCard.getBonus().getSeparatedBonuses()) {
					data[k][2] = string;
					k++;
				}
			}
		}
		TextTable tt = new TextTable(columnNames, data);
		return tt;
	}

	public PermitCard obtainCard(int choice) throws CloneNotSupportedException {
		PermitCard pc;
		if (choice == 1) {
			pc = firstPermitCard;
			firstPermitCard = null;
		}
		else {
			pc = secondPermitCard;
			secondPermitCard = null;
		}
		replaceACard();
		return pc;
	}

	public void replaceACard(){
		if (this.firstPermitCard == null) this.firstPermitCard = pickACard();
		else this.secondPermitCard = pickACard();
	}

	public void setRegionDeck(Deck<PermitCard> regionDeck) {
		this.regionDeck = regionDeck;
		firstPermitCard = pickACard();
		secondPermitCard = pickACard();
	}
    
    public void replaceCards() {

		regionDeck.addLast(firstPermitCard);
		regionDeck.addLast(secondPermitCard);
		firstPermitCard = pickACard();
		secondPermitCard = pickACard();
	}
    
    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("******************************************\n");
    	sb.append("*                 "+type+"               *\n");
    	sb.append("******************************************\n");
    	sb.append("Council of region: \n");
    	sb.append(regionCouncil+"\n");
    	sb.append("First permit card face up:\n");
    	sb.append(firstPermitCard+"\n");
    	sb.append("Second permit card face up:\n");
    	sb.append(secondPermitCard+"\n");
    	return sb.toString();
    }

}
