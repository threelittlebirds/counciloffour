package projectPackage.model.turn;

import projectPackage.controller.action.Action;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.io.Serializable;

public abstract class TurnState implements Serializable {

    protected TurnMachine tm;

    protected ResponseMsg responseMsg;

    protected BroadcastMessage broadcastMessage;

    protected transient ResultMsgPair resultMsgPair;

    public abstract void start(Action a) throws CloneNotSupportedException;

    public abstract ResultMsgPair nextState() throws CloneNotSupportedException;
    
    public TurnState(TurnMachine tm) {
    	this.tm=tm;
    }

}


