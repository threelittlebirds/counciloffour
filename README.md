# CouncilOfFour
Software Engineer's Project

To launch game start first the GameManager and then every GamePlayer!
Council of 4:

How to play:

1) Choose which connection you want to do: RMI or SOCKET
	 - RMI: Remote Method Invocation
	 - SOCKET: Connection by socket

2) If there are other playing waiting for a game you automatically connect to the waiting game, else you you have to wait for another player
to connect.

MATCH BEGIN:

Your turn began!

- Choose to begin your turn with a Main Action or a Quick Action!
  Type MAIN to begin with the Main Action.

  Type QUICK to begin with the Quick Action.

- Main Action:
  You can choose one of the 4 main actions:
                 Type MAIN1 for:Elect a councillor\n
                 Type MAIN2 for: Acquire a business permit tile
                 Type MAIN3 for: Build an emporium using permit tile
                 Type MAIN4 for: Build an emporium with the help of the King.

- Quick Action:
  You can choose one of the 4 quick actions or you can decide to don't do it:
                 Type QUICK1 for: Engage an assistant
                 Type QUICK2 for: Change permit tile
                 Type QUICK3 for: Send an assistant to elect a councillor
                 Type QUICK4 for: Perform an additional main action.
		 Type END for skip the quick action.


- Market:
In the Market phase you can sell your Permit Card, Politic Card and Assistants and decide the price you want.
After deciding what you want to sell you can choose what to buy from other players in the match.




