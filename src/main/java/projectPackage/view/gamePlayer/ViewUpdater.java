package projectPackage.view.gamePlayer;

import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 * Created by Carmelo on 27/06/2016.
 */
public interface ViewUpdater {

    /**
     * update the view with a received broadcast message.
     *
     * @param msg the received broadcast message
     */
    void update(BroadcastMessage msg);

    /**
     * Update the view with a received response message
     *
     * @param msg the received response message
     */
    void update(ResponseMsg msg);

    /**
     * Set the player number of this client in the game, it is used to filter out some
     * broadcast messages.
     *
     * @param playerNum the player number of this client in the game
     */
    void setPlayerNum(int playerNum);

    /**
     * Get the player number of this client in the game.
     *
     * @return the player number of this client in the game
     */
    int getPlayerNum();

}
