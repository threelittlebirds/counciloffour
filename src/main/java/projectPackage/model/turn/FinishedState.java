package projectPackage.model.turn;

import projectPackage.controller.action.Action;
import projectPackage.view.messages.ResultMsgPair;

/**
 * This is a state of the state machine that manages a game. It represents
 * the state where a game is finished, no further actions are allowed.
 */
public class FinishedState extends TurnState {

    public FinishedState(TurnMachine tm) {
        super(tm);
    }

    @Override
    public void start(Action a) throws CloneNotSupportedException {

    }

    @Override
    public ResultMsgPair nextState() throws CloneNotSupportedException {
        return null;
    }
}





