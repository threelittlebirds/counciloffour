package projectPackage.model.cards;

import org.junit.Before;
import org.junit.Test;
import projectPackage.model.bonus.containers.PermitCard;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Med on 02/07/2016.
 */
public class DeckTest {

    Deck<PoliticCard> politicDeck;
    Deck<PermitCard> permitDeck;
    List<PoliticCard> politicList;
    List<PermitCard> permitList;

    @Before
    public void setUp() throws Exception {
        politicDeck = new Deck<>();
        permitDeck = new Deck<>();
        politicList = new LinkedList<>();
        politicList.add(new PoliticCard(Color.GREEN));
        politicList.add(new PoliticCard(Color.BLUE));
        politicList.add(new PoliticCard(Color.RED));
        politicList.add(new PoliticCard(Color.BLACK));
    }

    @Test
    public void isEmpty() throws Exception {
        assertEquals(true, permitDeck.isEmpty());
        assertEquals(true, politicDeck.isEmpty());
        assertEquals(false, !politicDeck.isEmpty());
    }



    @Test
    public void add() throws Exception {
        Deck<PoliticCard> deck = new Deck<>();
        deck.add(new PoliticCard(Color.GREEN));
        assertEquals(deck.size(), 1);
    }

    @Test
    public void addLinkedList() throws Exception {
        politicDeck.add((LinkedList<PoliticCard>) politicList);
        assertTrue(politicDeck.size() == politicList.size());
    }

    @Test
    public void getDeckCards() throws Exception {
        LinkedList<PoliticCard> deck = politicDeck.getDeckCards();
        assertTrue(deck.equals(politicDeck.getDeckCards()));
    }

    @Test
    public void setDeckCards() throws Exception {
        politicDeck.setDeckCards((LinkedList<PoliticCard>) politicList);
        assertTrue(politicDeck.getDeckCards().equals(politicList));
    }

    @Test
    public void getCard() throws Exception {
        PoliticCard p = new PoliticCard(Color.RED);
        politicDeck.add(new PoliticCard(Color.BLACK));
        politicDeck.add(p);
        assertTrue(politicDeck.getCard(1).equals(p));
    }

    @Test
    public void getFirst() throws Exception {
        politicDeck.add((LinkedList<PoliticCard>) politicList);
        assertTrue(politicDeck.getFirst().equals(((LinkedList<PoliticCard>) politicList).getFirst()));

    }

    @Test
    public void getLast() throws Exception {
        politicDeck.add((LinkedList<PoliticCard>) politicList);
        assertTrue(politicDeck.getLast().equals(((LinkedList<PoliticCard>) politicList).getLast()));
    }

    @Test
    public void removeFirst() throws Exception {
        politicDeck.add((LinkedList<PoliticCard>) politicList);
        assertEquals(politicDeck.size(), 4);
        PoliticCard old = politicDeck.getFirst();
        PoliticCard pc = politicDeck.removeFirst();
        assertEquals(politicDeck.size(), 3);
        assertTrue(old.equals(pc));
    }

    @Test
    public void removeLast() throws Exception {

    }

    @Test
    public void removeCard() throws Exception {

    }

    @Test
    public void addLast() throws Exception {

    }

    @Test
    public void iterator() throws Exception {

    }

    @Test
    public void size() throws Exception {

    }

}