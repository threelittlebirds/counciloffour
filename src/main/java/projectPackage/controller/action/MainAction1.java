package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

/**
 * Main Action number 1: Elect a Councillor
 */

public class MainAction1 extends Action { 
	
	private int whichCouncillor;
	private int whichCouncil;
	
	

    public MainAction1(int whichCouncillor, int whichCouncil, TurnMachine tm) {
		super(tm);
		this.whichCouncillor = whichCouncillor;
		this.whichCouncil = whichCouncil;
	}

	public int getWhichCouncillor() {
		return whichCouncillor;
	}

	public int getWhichCouncil() {
		return whichCouncil;
	}

	@Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
		return tm.start(this);
    }

}
