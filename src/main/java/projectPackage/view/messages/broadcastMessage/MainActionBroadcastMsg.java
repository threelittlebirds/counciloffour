package projectPackage.view.messages.broadcastMessage;

import projectPackage.view.messages.MessageVisitor;

/**
 * Created by Carmelo on 27/06/2016.
 */
public class MainActionBroadcastMsg implements BroadcastMessage {

    private String mainSelected;

    private int player;

    public MainActionBroadcastMsg(String mainSelected, int player) {

        this.player = player;
        this.mainSelected = mainSelected;
    }

    public String getMainSelected() {
        return mainSelected;
    }

    public int getPlayer() {
        return player;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
