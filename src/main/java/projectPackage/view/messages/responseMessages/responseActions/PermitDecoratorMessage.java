package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

/**
 * Created by Med on 16/06/2016.
 */
public class PermitDecoratorMessage implements ResponseMsg {

    private int region;

    private int choice;

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public int getChoice() {
        return choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

}
