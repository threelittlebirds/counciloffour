package projectPackage.view.messages.requestMessages.requestActions;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.view.Token;

public class MainAction3Message extends ActionRequestMsg {
	
	private int whichPrivatePermitCard;
	private char whichCharacter;
	
	public MainAction3Message(Token token, int whichPrivatePermitCard, char whichCharacter) {
		super(token);
		this.whichPrivatePermitCard = whichPrivatePermitCard;
		this.whichCharacter = whichCharacter;
	}

	public int getWhichPrivatePermitCard() {
		return whichPrivatePermitCard;
	}

	public char getWhichCharacter() {
		return whichCharacter;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}
}
