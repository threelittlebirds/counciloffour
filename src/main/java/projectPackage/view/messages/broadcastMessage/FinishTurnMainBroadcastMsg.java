package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by carme on 05/07/2016.
 */
public class FinishTurnMainBroadcastMsg implements BroadcastMessage {

    private final int playerTurn;
    private final int nextPlayer;
    private final HashMap<Integer,LinkedList<TextTable>> playerSituation;
    private final LinkedList<TextTable> map;
    private final boolean end;

    public FinishTurnMainBroadcastMsg(int playerTurn, int nextPlayer, HashMap<Integer, LinkedList<TextTable>> playerSituation, LinkedList<TextTable> map, boolean isEnd) {
        this.playerTurn = playerTurn;
        this.nextPlayer = nextPlayer;
        this.playerSituation = playerSituation;
        this.map = map;
        this.end = isEnd;
    }

    public boolean isEnd() {
        return end;
    }

    public HashMap<Integer, LinkedList<TextTable>> getPlayerSituation() {
        return playerSituation;
    }

    public LinkedList<TextTable> getMap() {
        return map;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public int getPlayerTurn() {
        return playerTurn;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }


}
