package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.Deck;
import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.util.LinkedList;

public class AskMainActionResponseMsg implements ResponseMsg {

    private TextTable sideCouncillors; //main1
    private TextTable councils; //main1
    private TextTable regionCouncils; //main2
    private LinkedList<TextTable> permitCardsOfRegions; //main2
    private TextTable persPermitCards; //main3
    private Deck<PermitCard> personalPermitCards; //main3
    private TextTable cities; //main4
    private LinkedList<String> allCities; //main4

    public AskMainActionResponseMsg(TextTable sideCouncillors, TextTable councils, TextTable regionCouncils, LinkedList<TextTable> permitCardsOfRegions, TextTable persPermitCards, Deck<PermitCard> personalPermitCards, TextTable cities, LinkedList<String> allCities) {
        this.sideCouncillors = sideCouncillors;
        this.councils = councils;
        this.regionCouncils = regionCouncils;
        this.permitCardsOfRegions = permitCardsOfRegions;
        this.persPermitCards = persPermitCards;
        this.personalPermitCards = personalPermitCards;
        this.cities = cities;
        this.allCities = allCities;
    }



    public TextTable getSideCouncillors() {
        return sideCouncillors;
    }

    public TextTable getCouncils() {
        return councils;
    }

    public TextTable getRegionCouncils() {
        return regionCouncils;
    }

    public LinkedList<TextTable> getPermitCardsOfRegions() {
        return permitCardsOfRegions;
    }

    public TextTable getPersPermitCards() {
        return persPermitCards;
    }

    public Deck<PermitCard> getPersonalPermitCards() {
        return personalPermitCards;
    }

    public TextTable getCities() {
        return cities;
    }

    public LinkedList<String> getAllCities() {
        return allCities;
    }


    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
