package projectPackage.view.messages.requestMessages;


import projectPackage.view.Token;
import projectPackage.view.messages.requestMessages.RequestMessage;


public class SendChatMsg extends RequestMessage {

    private String chatMessage;

    public SendChatMsg(Token token, String chatMessage) {
        super(token);
        this.chatMessage = chatMessage;
    }

    public String getChatMessage() {
        return chatMessage;
    }


}
