package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.Token;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Carmelo on 19/06/2016.
 */
public class AskToBuyMessage implements ResponseMsg {

    private int moneyPosition;
    private HashMap<Token,TextTable> permitToBuy;
    private HashMap<Token,TextTable> politicToBuy;
    private HashMap<Token,TextTable> assistantToBuy;
    private HashMap<Token,Integer> numPermitToBuy;
    private HashMap<Token,Integer> numPoliticToBuy;
    private HashMap<Token,Integer> numAssistantToBuy;
    private HashMap<Token,LinkedList<Integer>> prizePermitToBuy;
    private HashMap<Token,LinkedList<Integer>> prizePoliticToBuy;
    private HashMap<Token,Integer> prizeAssistantToBuy;

    public AskToBuyMessage(HashMap<Token, TextTable> permitToBuy, HashMap<Token, TextTable> politicToBuy, HashMap<Token, TextTable> assistantToBuy, HashMap<Token, Integer> numPermitToBuy, HashMap<Token, Integer> numPoliticToBuy, HashMap<Token, Integer> numAssistantToBuy, HashMap<Token, LinkedList<Integer>> prizePermitToBuy, HashMap<Token, LinkedList<Integer>> prizePoliticToBuy, HashMap<Token, Integer> prizeAssistantToBuy, int moneyPosition) {
        this.moneyPosition = moneyPosition;
        this.permitToBuy = permitToBuy;
        this.politicToBuy = politicToBuy;
        this.assistantToBuy = assistantToBuy;
        this.numPermitToBuy = numPermitToBuy;
        this.numPoliticToBuy = numPoliticToBuy;
        this.numAssistantToBuy = numAssistantToBuy;
        this.prizePermitToBuy = prizePermitToBuy;
        this.prizePoliticToBuy = prizePoliticToBuy;
        this.prizeAssistantToBuy = prizeAssistantToBuy;
    }

    public int getMoneyPosition() {
        return moneyPosition;
    }

    public HashMap<Token,TextTable> getPermitToBuy() {
        return permitToBuy;
    }

    public HashMap<Token, TextTable> getPoliticToBuy() {
        return politicToBuy;
    }

    public HashMap<Token, TextTable> getAssistantToBuy() {
        return assistantToBuy;
    }

    public HashMap<Token, Integer> getNumPermitToBuy() {
        return numPermitToBuy;
    }

    public HashMap<Token, Integer> getNumPoliticToBuy() {
        return numPoliticToBuy;
    }

    public HashMap<Token, Integer> getNumAssistantToBuy() {
        return numAssistantToBuy;
    }

    public HashMap<Token, LinkedList<Integer>> getPrizePermitToBuy() {
        return prizePermitToBuy;
    }

    public HashMap<Token, LinkedList<Integer>> getPrizePoliticToBuy() {
        return prizePoliticToBuy;
    }

    public HashMap<Token, Integer> getPrizeAssistantToBuy() {
        return prizeAssistantToBuy;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }

}
