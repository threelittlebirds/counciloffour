package projectPackage.model.bonus.containers;

import projectPackage.model.GameBoard;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.decorators.NobilityDecorator;
import projectPackage.model.turn.TurnMachine;

import java.util.LinkedList;


public class BlankContainer implements Bonus {

    private TurnMachine turnMachine;

    private boolean nobilityDecorator;

    public BlankContainer(TurnMachine tm) {
        this.turnMachine = tm;
        nobilityDecorator = false;
    }

    @Override
    public GameBoard getGameBoard() {
        return turnMachine.getGameBoard();
    }

    @Override
    public void activateBonus() {
        System.out.println("I'm activating the bonuses:\n");
    }

    @Override
    public TurnMachine getTurnMachine() {
        return turnMachine;
    }

    @Override
    public boolean hasNobilityDecorator() {
        return nobilityDecorator;
    }

    @Override
    public void setNobilityDecorator(boolean value) {
        this.nobilityDecorator = value;
    }

    @Override
    public int getNumberOfBonuses() {
        return 0;
    }

    @Override
    public LinkedList<String> getSeparatedBonuses() {
        LinkedList<String> list = new LinkedList<>();
        return list;
    }

    @Override
    public String toString() {
        return "";
    }
}
