package projectPackage.model;

import projectPackage.model.bonus.containers.Coin;
import projectPackage.model.utility.Util;
import projectPackage.model.utility.table.TextTable;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class City implements Serializable {

    private String name;
    private Color cityColor;
    private ArrayList<Emporium> emporiums;
    private char shortName;
    private TypeRegion typeRegion;
    private Coin coin;
    private static int cont = 1;

    public City(String name, Color cityColor, TypeRegion typeRegion) {
        //per avere la prima lettera maiuscola:
        char c = name.charAt(0);
        String s1 = String.valueOf(c).toUpperCase();
        String s2 = name.substring(1,name.length());
        this.name = s1+s2;
        this.emporiums = new ArrayList<>();
        this.shortName = this.name.charAt(0);
        this.cityColor = cityColor;
        this.typeRegion = typeRegion;
        cont++;
    }

    public Coin getCoin() {
        return coin;
    }
    public void setCoin(Coin coin) {
        this.coin = coin;
    }
    public TypeRegion getTypeRegion() {
        return typeRegion;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Color getCityColor() {
        return cityColor;
    }
    public char getShortName(){
        return shortName;
    }
    public void setCityColor(Color cityColor) {
        this.cityColor = cityColor;
    }

    public ArrayList<Emporium> getEmporiums() {
        return emporiums;
    }

    public void build(Color color) throws CloneNotSupportedException {
        emporiums.add(new Emporium(color));
        coin.activateBonus();
    }

    public boolean contains(Color color) {
        return  emporiums.contains(new Emporium(color));
    }

    public int numberOfOthersEmporiums(Color color){
        int i=0;
        for(Emporium e: emporiums){
            if(!e.getColor().equals(color)) i++;
        }
        return i;
    }

    public void printEmporiums(){
        String[] columnNames = new String[emporiums.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = ""+i;
        }
        Object[][] data = new Object [3][columnNames.length];
        for(int i=0;i<emporiums.size();i++){
            data[0][i] = "";
            data[1][i] = Util.getStringFromColor(emporiums.get(i).getColor());
            data[2][i] = "";
        }
        TextTable tt = new TextTable(columnNames, data);
        tt.printTable();
    }

    /*
    @Override
    public int hashCode() {
        final int molt = 41;
        return molt*name.hashCode()+cont;
    }
    */
}

