package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * A message to broadcast the start of a new game.
 */
public class GameStartedBroadcastMsg implements BroadcastMessage {

    private final int gameNumber;
    private final int numberOfPlayers;
    private final String mapName;
    private final int playerTurn;
    private final HashMap<Integer,LinkedList<TextTable>> playerSituation;
    private final LinkedList<TextTable> map;

    /**
     * Create a new GameStartedBroadcastMsg.
     *
     * @param gameNumber the ID of the game
     * @param numberOfPlayers the number of players in the game
     * @param mapName the name of the zone of the game
     * @param playerTurn the player starting the turn
     */
    public GameStartedBroadcastMsg(int gameNumber, int numberOfPlayers, String mapName, int playerTurn, HashMap<Integer, LinkedList<TextTable>> playerSituation, LinkedList<TextTable> map) {
        this.gameNumber = gameNumber;
        this.numberOfPlayers = numberOfPlayers;
        this.mapName = mapName;
        this.playerTurn = playerTurn;
        this.playerSituation = playerSituation;
        this.map = map;
    }

    public HashMap<Integer, LinkedList<TextTable>> getPlayerSituation() {
        return playerSituation;
    }

    public LinkedList<TextTable> getMap() {
        return map;
    }

    /**
     * Get the ID of the game
     *
     * @return the ID of the game
     */
    public int getGameNumber() {

        return gameNumber;

    }

    /**
     * Get the number of players in the game.
     *
     * @return the number of players in the game
     */
    public int getNumberOfPlayers() {

        return numberOfPlayers;

    }

    /**
     * Get the name of the zone of the game.
     *
     * @return the name of the zone of the game
     */
    public String getMapName() {

        return mapName;

    }

    /**
     * Get the player starting the turn.
     *
     * @return the player starting the turn
     */
    public int getPlayerTurn() {

        return playerTurn;

    }

    @Override
    public void display(MessageVisitor visitor) {

        visitor.display(this);

    }

}
