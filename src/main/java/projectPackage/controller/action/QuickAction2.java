package projectPackage.controller.action;

import projectPackage.controller.exceptions.NotEnoughAssistants;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;


public class QuickAction2 extends Action {

    public QuickAction2(int whichRegion, TurnMachine tm) {
        super(tm);
        this.whichRegion = whichRegion;
    }

    private int whichRegion;

    public int getWhichRegion() {
        return whichRegion;
    }

    public void setWhichRegion(int whichRegion) {
        this.whichRegion = whichRegion;
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
        if(!enoughAssistant()) throw new NotEnoughAssistants();
        return tm.start(this);
    }

    private boolean enoughAssistant(){
        return tm.getGameBoard().getCurrPlayer().getNumAssistant() >= 1;
    }
}
