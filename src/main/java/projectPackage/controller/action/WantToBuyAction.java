package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.ResultMsgPair;

/**
 * Created by carme on 07/07/2016.
 */
public class WantToBuyAction extends Action {

    public WantToBuyAction(TurnMachine tm) {
        super(tm);
    }

    @Override
    public ResultMsgPair execute() throws CloneNotSupportedException {
        return tm.start(this);
    }
}
