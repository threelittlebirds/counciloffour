package projectPackage.model;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;


public class CouncillorTest {


    @Test
    public void getColor() throws Exception {

        Councillor councillor = new Councillor(Color.BLACK);
        Color color = Color.BLACK;
        Color councillorColor = councillor.getColor();

        assertEquals(color, councillorColor);

    }

}