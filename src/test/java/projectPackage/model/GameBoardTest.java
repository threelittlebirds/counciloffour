package projectPackage.model;

import org.junit.Before;
import org.junit.Test;
import projectPackage.model.cards.Deck;
import projectPackage.model.cards.MultiColor;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.Token;

import java.awt.*;
import java.util.LinkedList;
import java.util.UUID;

import static org.junit.Assert.*;


public class GameBoardTest {

    TurnMachine turnMachine;
    GameBoard gameBoard;
    LinkedList<Player> players;
    Player currPlayer;

    @Before
    public void setUp() throws Exception {

        players = new LinkedList<>();
        players.add(new Player(new Token(UUID.randomUUID()), Color.BLACK));
        players.add(new Player(new Token(UUID.randomUUID()), Color.RED));
        players.add(new Player(new Token(UUID.randomUUID()), Color.GREEN));
        players.add(new Player(new Token(UUID.randomUUID()), Color.BLUE));
        turnMachine = new TurnMachine("ORIGINAL.xml", players);
        gameBoard = turnMachine.getGameBoard();
        gameBoard.setCurrPlayer(gameBoard.getPlayers().get(0));
        currPlayer = gameBoard.getCurrPlayer();
    }

    @Test
    public void initializeRandomPlayers() throws Exception {
        //LOLLO
    }

    @Test
    public void shortestPathOfKing(){
        assertEquals(2,gameBoard.shortestPathOfKing(gameBoard.getCity('O')));

    }

    @Test
    public void addCouncillorToReserve() throws Exception {

        int currSize = gameBoard.getReserveOfCouncillors().size();
        gameBoard.addCouncillorToReserve(new Councillor(Color.BLACK));
        assertEquals(currSize+1, gameBoard.getReserveOfCouncillors().size());
    }

    @Test
    public void satisfyCouncil() throws Exception {
        LinkedList<Councillor> list = new LinkedList<>();
        list.add(new Councillor(Color.BLACK));
        list.add(new Councillor(Color.RED));
        list.add(new Councillor(Color.YELLOW));
        list.add(new Councillor(Color.BLACK));

        gameBoard.getRegion(TypeRegion.COAST).getRegionCouncil().setCouncillors(list);

        LinkedList<PoliticCard> deckList = new LinkedList<>();
        deckList.add(new PoliticCard(new MultiColor(200)));
        deckList.add(new PoliticCard(Color.RED));
        deckList.add(new PoliticCard(Color.YELLOW));
        deckList.add(new PoliticCard(Color.BLACK));

        currPlayer.getPoliticDeck().clear();
        currPlayer.getPoliticDeck().add(deckList);

        gameBoard.satisfyCouncil(TypeCouncil.COAST);

        assertEquals(9, currPlayer.getMoneyPosition().getPosition());


    }

    @Test
    public void adjacentCitiesBonus() throws Exception {

        Color color = currPlayer.getColor();
        gameBoard.getCity('A').getEmporiums().add(new Emporium(color));
        gameBoard.getCity('F').getEmporiums().add(new Emporium(color));
        LinkedList<City> list = gameBoard.adjacentCitiesBonus(gameBoard.getCity('C'));
        for (City c : list) {
            System.out.print(c.getName());
        }


        assertEquals(2, list.size());
        assertTrue(list.contains(gameBoard.getCity('F')));
        assertTrue(list.contains(gameBoard.getCity('A')));

    }


    @Test
    public void nextPlayer() throws Exception {
        assertEquals(gameBoard.getPlayers().get(1), gameBoard.nextPlayer());
    }


    @Test
    public void moveOnMoneyTrack1() throws Exception {
        int oldPos = currPlayer.getMoneyPosition().getPosition();
        gameBoard.moveOnMoneyTrack(1);
        gameBoard.moveOnMoneyTrack(currPlayer, 2);
        assertEquals(oldPos+3, currPlayer.getMoneyPosition().getPosition());

    }

    @Test
    public void moveOnVictoryTrack() throws Exception {
        int oldPos = currPlayer.getVictoryPosition().getPosition();
        gameBoard.moveOnVictoryTrack(1);
        assertEquals(oldPos+1, currPlayer.getVictoryPosition().getPosition());
    }

    @Test
    public void moveOnNobilityTrack() throws Exception {
        int oldPos = currPlayer.getNobilityPosition().getPosition();
        gameBoard.moveOnNobilityTrack(1);
        assertEquals(oldPos+1, currPlayer.getNobilityPosition().getPosition());
    }

    @Test
    public void hasBuiltInAllCities() throws Exception {
        for (City c : gameBoard.getMap().keySet()) {
            c.getEmporiums().add(new Emporium(currPlayer.getColor()));
        }
        assertTrue(gameBoard.hasBuiltInAllCities());
    }


    @Test
    public void buildEmporium() throws Exception {
        gameBoard.buildEmporium('D');
        assertTrue(gameBoard.getCity('D').contains(currPlayer.getColor()));
    }

    @Test
    public void containsShortCity() throws Exception {
        assertFalse(gameBoard.containsShortCity('Z'));
    }


    @Test
    public void alreadyBuilt() throws Exception {
        gameBoard.buildEmporium('D');
        assertTrue(gameBoard.alreadyBuilt('D'));
    }

    @Test
    public void corruptCouncil() throws Exception {
        Councillor c = gameBoard.getReserveOfCouncillors().get(1);
        gameBoard.corruptCouncil(1, 1);
        assertEquals(c, gameBoard.getRegion(TypeRegion.COAST).getRegionCouncil().getFirst());
    }


}