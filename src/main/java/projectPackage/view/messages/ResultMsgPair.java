package projectPackage.view.messages;

import projectPackage.view.messages.broadcastMessage.BroadcastMessage;
import projectPackage.view.messages.responseMessages.ResponseMsg;

import java.io.Serializable;


public class ResultMsgPair implements Serializable {

    private ResponseMsg privateMessage;
    private BroadcastMessage broadcastMessage;

    public ResultMsgPair(ResponseMsg privateMessage, BroadcastMessage broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
        this.privateMessage = privateMessage;
    }

    public ResponseMsg getPrivateMessage() {
        return privateMessage;
    }

    public void setPrivateMessage(ResponseMsg privateMessage) {
        this.privateMessage = privateMessage;
    }

    public BroadcastMessage getBroadcastMessage() {
        return broadcastMessage;
    }

    public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
    }
}
