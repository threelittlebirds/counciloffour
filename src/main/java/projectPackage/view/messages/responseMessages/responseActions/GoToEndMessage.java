package projectPackage.view.messages.responseMessages.responseActions;

import projectPackage.model.Player;
import projectPackage.view.Token;
import projectPackage.view.messages.MessageVisitor;
import projectPackage.view.messages.responseMessages.ResponseMsg;

public class GoToEndMessage implements ResponseMsg {
	
	private final String player;

	public GoToEndMessage(String player) {
		this.player = player;
	}

	public String getPlayer() {
		return player;
	}

	@Override
	public void display(MessageVisitor visitor) {
		visitor.display(this);
	}



}
