package projectPackage.model.cards;


import java.awt.*;

import projectPackage.model.utility.Util;
import projectPackage.model.utility.table.TextTable;

public class PoliticCard implements Card {

    private Color cardColor;
	private static int cont = 1;

    public PoliticCard(Color cardColor) {
		super();
		this.cardColor = cardColor;
		cont++;
	}

	public Color getCardColor() {
        return cardColor;
    }
    
    public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("* "+ Util.getStringFromColor(cardColor)+" *\n");
    	sb.append("*       *\n");
    	sb.append("*********");
    	return sb.toString();
	}

	public TextTable printCard(){
		String[] columnNames = { "Permit Card" };
		Object[][] data = new Object[3][columnNames.length];
		data[0][0] = "";
		data[1][0] = Util.getStringFromColor(cardColor);
		data[2][0] = "";
		TextTable tt = new TextTable(columnNames,data);
		return tt;
	}

	@Override
	public int hashCode() {
		final int molt = 41;
		return molt* cardColor.hashCode()+cont;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PoliticCard that = (PoliticCard) o;

		return getCardColor().equals(that.getCardColor());

	}
}
