package projectPackage.model;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.*;
import projectPackage.model.bonus.decorators.specialDecorator.ActionDecorator;
import projectPackage.model.bonus.decorators.specialDecorator.SpecialDecorator;
import projectPackage.model.cards.*;
import projectPackage.model.track.*;
import projectPackage.model.track.square.*;
import projectPackage.model.utility.Util;
import projectPackage.model.utility.ColorUtils;
import projectPackage.model.utility.Pair;
import projectPackage.model.utility.table.TextTable;
import projectPackage.view.Token;

import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class GameBoard implements Cloneable, Serializable {

    private static final Logger LOG = Logger.getLogger(GameBoard.class.getName());

    private King king;

    private Region regions[];

    private HashMap<City,LinkedList<City>> map;

    private LinkedList<Player> players;

    private Player currPlayer;

    private ArrayList<Councillor> reserveOfCouncillors;

	private Council kingCouncil;

    private Track<NobilitySquare> nobilityTrack;

    private Track<VictorySquare> victoryTrack;

    private Track<MoneySquare> moneyTrack;
    
    private Deck<PoliticCard> reserveOfPoliticCards;

    private Deck<ColorCard> colorCards;
    
    private Deck<KingCard> kingCards;
    
    private LinkedList<String> randomPlayerExctraction;

    private HashMap<PoliticCard, Integer> politicToBuy = new HashMap<>();

    private HashMap<PermitCard, Integer> permitToBuy = new HashMap<>();

    private LinkedList<Pair> assistantToBuy = new LinkedList<>();

    private boolean isFinished;

    private LinkedList<Bonus> specialBonus;


    public GameBoard(LinkedList<Player> allPlayers) {
        players = allPlayers;
        randomPlayerExctraction = new LinkedList<>();
        initializeRandomPlayers();
        factory();
    }

    /**
     * The following method build part of
     * variables that are needed for the game
     */

    private void factory(){
    	victoryTrack = new Track<>(100);
    	moneyTrack = new Track<>(21);
        nobilityTrack = new Track<>(21);
        kingCards = new Deck<>();
        colorCards = new Deck<>();
        regions = new Region[3];
        reserveOfPoliticCards = new Deck<>();
        reserveOfCouncillors = new ArrayList<>(8);
    	for(int i=0;i<100;i++){
    		victoryTrack.add(new VictorySquare(i));
    	}
    	for(int i=0;i<21;i++){
    		moneyTrack.add(new MoneySquare(i));
            nobilityTrack.add(new NobilitySquare(i));
    	}
        int i=0;
        for(Player p : players){
            p.setMoneyPosition(moneyTrack.getSquare(10+(i++)));
    		p.setVictoryPosition(victoryTrack.getSquare(0));
            p.setNobilityPosition(nobilityTrack.getSquare(0));
    	}

        specialBonus = new LinkedList<>();

    }

    public void initializeRandomPlayers(){
        randomPlayerExctraction.addAll(players.stream().map(Player::getName).collect(Collectors.toList()));
    }

    public void addCouncillorToReserve(Councillor c){
        reserveOfCouncillors.add(c);
    }

    public void setKing(King king) {
        this.king = king;
    }

    public HashMap<PoliticCard, Integer> getPoliticToBuy() {
        return politicToBuy;
    }

    public HashMap<PermitCard, Integer> getPermitToBuy() {
        return permitToBuy;
    }

    public LinkedList<Pair> getAssistantToBuy() {
        return assistantToBuy;
    }

    public LinkedList<Player> getPlayers() {
		return players;
	}

	public Deck<ColorCard> getColorCards() {
        return colorCards;
    }

    public Deck<KingCard> getKingCards() {
        return kingCards;
    }
    
    public Deck<PoliticCard> getReserveOfPoliticCards(){
    	return reserveOfPoliticCards;
    }

    public LinkedList<Bonus> getSpecialBonus() {
        return specialBonus;
    }

    public int shortestPathOfKing(City dest){
    	return Util.minPathOfGraph(map,getCity(king.getPosition()),dest);
    }

    public void satisfyCouncil(TypeCouncil council) {
        int cont = 0;
        LinkedList<PoliticCard> list = new LinkedList<>();
        for (Councillor c : getCouncil(council)) {
            Iterator<PoliticCard> iterator = currPlayer.getPoliticDeck().iterator();
            while (iterator.hasNext()) {
                PoliticCard politicCard = iterator.next();
                if (politicCard.getCardColor().equals(c.getColor())) {
                    cont++;
                    list.add(politicCard);
                    iterator.remove();
                    break;
                }
            }
        }
        putDiscardedPoliticCards(list);
        if(cont < 4) {
             if (currPlayer.getNumMulticolor() > 0) {
                LinkedList<PoliticCard> multicolorRemoved = currPlayer.removeMulticolor(cont);
                moveOnMoneyTrack(-multicolorRemoved.size());
                cont = cont + multicolorRemoved.size();
                putDiscardedPoliticCards(multicolorRemoved);
            }
        }
        switch(cont) {
            case 1: moveOnMoneyTrack(-10);
                break;
            case 2: moveOnMoneyTrack(-7);
                break;
            case 3: moveOnMoneyTrack(-4);
                break;
        }
    }

    //restituisce le citta' adiacenti in cui il giocatore ha gia' costruito un emporio

    public LinkedList<City> adjacentCitiesBonus(City c){
    	LinkedList<City> l = new LinkedList<>();
    	for(City city : map.get(c)) {
    		if(city.contains(currPlayer.getColor())) l.add(city);
    	}
    	return l;
    }

    private void putDiscardedPoliticCards(LinkedList<PoliticCard> l){
    	reserveOfPoliticCards.add(l);
    	reserveOfPoliticCards.shuffle();
    }
    
    public ArrayList<Councillor> getReserveOfCouncillors() {
		return reserveOfCouncillors;
	}

	public Region[] getRegions() {
		return regions;
	}

    public void initializeMap(HashMap<City,LinkedList<City>> map) {
    	this.map = map;
    }

    public Track<NobilitySquare> getNobilityTrack() {
        return nobilityTrack;
    }

    private Track<VictorySquare> getVictoryTrack() {
        return victoryTrack;
    }

    private Track<MoneySquare> getMoneyTrack() {
        return moneyTrack;
    }
    
    public int getNumPlayers() {
        return players.size();
    }

    public Player getCurrPlayer() {
        return currPlayer;
    }

    public void setCurrPlayer(Player currPlayer) {
        this.currPlayer = currPlayer;
    }
    
    public Council getKingCouncil() {
		return kingCouncil;
	}
    
    public void setKingCouncil(Council kingCouncil) {
		this.kingCouncil = kingCouncil;
	}

    public Region getRegion(int choice) {
        return regions[choice - 1];
    }

    public Region getRegion(TypeRegion tc) {
        for (Region region : regions) {
            if (region.getType().equals(tc)) return region;
        }
        return null;
    }

	public Player nextPlayer(){
    	int i = players.indexOf(currPlayer);
    	if(i == players.size()-1) return players.get(0);
    	return players.get(i+1);
    }

	public Player nextRandomPlayer(){
		int i = randomPlayerExctraction.size();
		int randomNumb = Util.randInt(0,i);
		String s = randomPlayerExctraction.remove(randomNumb);
		for(Player p : players){
			if(p.getName().equals(s)) return p;
		}
		return null;
	}

    public int indexOfLastPlayer(){
        if(players.indexOf(currPlayer) == 0) return players.size()-1;
        return players.indexOf(currPlayer)-1;
    }

    public int indexOfCurrentPlayer(){
        return players.indexOf(currPlayer);
    }

    public Player getPlayer(Token token) {
        for (Player p : players) {
            if (p.getToken().equals(token)) return p;
        }
        return null;
    }

    public TextTable printPlayer(Token token){
        Player p = getPlayer(token);
        String[] columnNames = { "Name", "Color", "Number of Assistants", "Politic Cards", "Victory Position", "Money Position", "Nobility Position"};
        Object[][] data = new Object [1][columnNames.length];
        if(p == null) return new TextTable(columnNames, data);
        data[0][0] = p.getName();
        data[0][1] = ColorUtils.getColorNameFromColor(p.getColor());
        data[0][2] = p.getNumAssistant();
        String s = "";
        Iterator<PoliticCard> iterator = getPlayer(token).getPoliticDeck().iterator();
        while(iterator.hasNext()){
            s = s + Util.getStringFromColor(iterator.next().getCardColor());
            if(iterator.hasNext()) s = s + ", ";
        }
        data[0][3] = s;
        data[0][4] = p.getVictoryPosition().getPosition();
        data[0][5] = p.getMoneyPosition().getPosition();
        data[0][6] = p.getNobilityPosition().getPosition();
        return new TextTable(columnNames, data);
    }

    private TextTable printNobilityTrack(){
        String[] columnNames = new String[nobilityTrack.size()];
        for(int i = 0;i<columnNames.length;i++){
            columnNames[i] = "Nobility Square "+(i+1);
        }
        int maxNumberOfBonuses = 3;
        for(NobilitySquare ns : nobilityTrack){
            if(ns.getBonus() != null && ns.getBonus().getNumberOfBonuses() > maxNumberOfBonuses) maxNumberOfBonuses = ns.getBonus().getNumberOfBonuses();
        }
        Object[][] data = new Object [maxNumberOfBonuses*2][columnNames.length];
        int i = 0;
        for(NobilitySquare ns : nobilityTrack){
            if(ns.getBonus() != null) {
                int k = 0;
                for (String string : ns.getBonus().getSeparatedBonuses()) {
                    data[k][i] = string;
                    data[k + 1][i] = "";
                    k = k + 2;
                }
            }
            i++;
        }
        return new TextTable(columnNames, data);
    }

    public void moveOnMoneyTrack(int quantity) {
        moveOnMoneyTrack(currPlayer, quantity);
    }

    public void moveOnMoneyTrack(Player p, int quantity){
        int pos = p.getMoneyPosition().getPosition();
        if (pos == 20) return;
        int newPos;
        if (pos+quantity >= 20 ) newPos=20;
        else newPos=pos+quantity;
        p.setMoneyPosition(getMoneyTrack().getSquare(newPos));
    }

    public void moveOnVictoryTrack(int quantity) {
        moveOnVictoryTrack(currPlayer, quantity);
    }

    private void moveOnVictoryTrack(Player p, int quantity){
    	int pos = p.getVictoryPosition().getPosition();
        if (pos == 99) return;
        int newPos;
        if (pos+quantity > 99 ) newPos=99;
        else newPos=pos+quantity;
        p.setVictoryPosition(getVictoryTrack().getSquare(newPos));
    }

    public void moveOnNobilityTrack(int quantity) throws CloneNotSupportedException {
        int pos = currPlayer.getNobilityPosition().getPosition();
        int newPos;
        if (pos+quantity >= 20 ) newPos=20;
        else newPos=pos+quantity;
        currPlayer.setNobilityPosition(getNobilityTrack().getSquare(newPos));
        NobilitySquare square = currPlayer.getNobilityPosition();
        if (square.getBonus() != null) {
            Bonus b = square.getBonus();
            if (b instanceof SpecialDecorator) {
                if (b instanceof ActionDecorator) {
                    if (((ActionDecorator) b).getTempBonus() instanceof SpecialDecorator) {
                        specialBonus.add(square.getBonus());
                    }
                } else {
                    specialBonus.add(square.getBonus());
                }
            } else b.activateBonus();
        }
    }

    public void getUltimatePointsFromBonus() throws CloneNotSupportedException{
        Player currP = currPlayer;
        currPlayer.activateUltimateBonuses();
        currPlayer = nextPlayer();
        while(true){
        	if(currPlayer.equals(currP)) break;
        	currPlayer.activateUltimateBonuses();
        	currPlayer = nextPlayer();
        }
    }

    public void getUltimatePointsFromPermitCards(){
    	LinkedList<Player> l = new LinkedList<>();
    	l.add(players.getFirst());
    	for(Player p : players){
    		if(p.getPermitDeck().size() > l.get(0).getPermitDeck().size()){
    			l.clear();
    			l.add(p);
    		}
    		if(p.getPermitDeck().size() == l.get(0).getPermitDeck().size()){
    			l.add(p);
    		}
    	}
    	for(Player p : l){
    		moveOnVictoryTrack(p,3);
    	}
    }

    public void getUltimatePointsFromNobilityPosition(){
    	LinkedList<Player> l = new LinkedList<>();
    	l.add(players.getFirst());
    	for(Player p : players){
    		if(p.getNobilityPosition().getPosition() > l.getFirst().getNobilityPosition().getPosition()){
    			l.clear();
    			l.add(p);
    		}
    		if(p.getNobilityPosition().getPosition() == l.getFirst().getNobilityPosition().getPosition()){
    			l.add(p);
    		}
    	}
    	if(l.size() > 1){
    		for(Player p : l){
    			moveOnVictoryTrack(p,5);
    		}
    	}
    	else{
    		moveOnVictoryTrack(l.getFirst(),5);
    		LinkedList<Player> l2 = new LinkedList<>();
    		for(Player p : players){
    			if(!p.equals(l.getFirst())) l2.add(p);
    		}
    		LinkedList<Player> l3 = new LinkedList<>();
            l3.add(l2.getFirst());
    		for(Player p : l2){
        		if(p.getNobilityPosition().getPosition() > l3.getFirst().getNobilityPosition().getPosition()){
        			l3.clear();
        			l3.add(p);
        		}
        		if(p.getNobilityPosition().getPosition() == l3.getFirst().getNobilityPosition().getPosition()){
        			l3.add(p);
        		}
        	}
    		for(Player p : l3){
    			moveOnVictoryTrack(p,2);
    		}
    	}
    }

    public LinkedList<City> getAllCities(){
    	LinkedList<City> l = new LinkedList<>();
        l.addAll(this.map.keySet());
		return l;
    }

    public boolean hasBuiltInAllCities(){
        for(City c : map.keySet()){
            if(!c.contains(currPlayer.getColor()))
                return false;
        }
        return true;
    }

    public void obtainCard(int whichRegion, int choicePc) throws CloneNotSupportedException {
        Region region = getRegion(whichRegion);
        currPlayer.addPermitCard(region.obtainCard(choicePc));
    }

    public void buildEmporium(char city) throws CloneNotSupportedException {
        City c = getCity(city);
        c.build(currPlayer.getColor());
        currPlayer.payAssistant(c.numberOfOthersEmporiums(currPlayer.getColor()));
        for(City cc : adjacentCitiesBonus(c)){
            cc.getCoin().activateBonus();
        }
        if (hasBuiltInAll(c.getTypeRegion())) {
            currPlayer.getBonusCards().add(getRegion(c.getTypeRegion()).getRegionPrize());
            if (!kingCards.isEmpty()) {
                currPlayer.getBonusCards().add(kingCards.removeFirst());
            }
        }
        if (hasBuiltInAll(c.getCityColor())) {
            currPlayer.getBonusCards().add(getColorCard(c.getCityColor()));
            if (!kingCards.isEmpty()) {
                currPlayer.getBonusCards().add(kingCards.removeFirst());
            }
        }
        if(hasBuiltInAllCities()) moveOnVictoryTrack(3);
    }


    private boolean hasBuiltInAll(Color color) {
        LinkedList<City> cities = getSameColor(color);
        int cont=0;
        for (City c : cities) {
            if (alreadyBuilt(c.getShortName())) {
                cont++;
            }
        }
        return cities.size() == cont;
    }

    private boolean hasBuiltInAll(TypeRegion tc) {
        LinkedList<City> cities = getSameRegion(tc);
        int cont=0;
        for (City c : cities) {
            if (alreadyBuilt(c.getShortName())) {
                cont++;
            }
        }
        return cities.size() == cont;
    }

    private LinkedList<City> getSameRegion(TypeRegion tc) {
        return map.keySet().stream().filter(c -> c.getTypeRegion().equals(tc)).collect(Collectors.toCollection(LinkedList::new));
    }

    private LinkedList<City> getSameColor(Color color) {
        return map.keySet().stream().filter(c -> c.getCityColor().equals(color)).collect(Collectors.toCollection(LinkedList::new));
    }

    public City getCity(char c) {
        char shortCity = Character.toUpperCase(c);
        for(City city : map.keySet()){
        	if(city.getShortName() == shortCity) return city;
        }
        return null;
    }

    public HashMap<City, LinkedList<City>> getMap() {
        return map;
    }

    public boolean containsShortCity(char c){
    	for(City city : map.keySet()){
        	if(city.getShortName() == c) return true;
        }
    	return false;
    }

    public LinkedList<City> getCitiesOfPlayer() {
        return map.keySet().stream().filter(this::alreadyBuilt).collect(Collectors.toCollection(LinkedList::new));
    }

    public HashMap<Integer,LinkedList<TextTable>> printPlayersSituation(){
        HashMap<Integer,LinkedList<TextTable>> printAllPlayers = new HashMap<>();
        for(Player p : players){
            LinkedList<TextTable> list = new LinkedList<>();
            list.addLast(printPlayer(p.getToken()));
            list.addLast(p.printPermitDeck());
            list.addLast(p.printGraveDeck());
            printAllPlayers.put(players.indexOf(p),list);
        }
        return printAllPlayers;
    }

    public LinkedList<TextTable> printSeparatePermitToSell(){
        LinkedList<TextTable> textTables = new LinkedList<>();
        for(PermitCard pc : currPlayer.getPermitInSellingTemp()){
            textTables.addLast(pc.printCard());
        }
        return textTables;
    }

    public LinkedList<TextTable> printSeparatePoliticToSell(){
        LinkedList<TextTable> textTables = new LinkedList<>();
        for(PoliticCard pc : currPlayer.getPoliticInSellingTemp()){
            textTables.addLast(pc.printCard());
        }
        return textTables;
    }

    public TextTable printAssistantsToSell(){
        String[] columnNames = {"Assistants to sell"};
        Object[][] data = new Object [3][columnNames.length];
        data[0][0] = "";
        data[1][0] = currPlayer.getNumAssistant();
        data[2][0] = "";
        return new TextTable(columnNames, data);
    }

    public HashMap<Token,TextTable> printAllPermitToBuy(){
        HashMap<Token,TextTable> allPermitToBuy = new HashMap<>();
        players.stream().filter(p -> p.getAssistantInSelling().getNum() != 0).forEach(p -> allPermitToBuy.put(p.getToken(), printPermitToBuy(p)));
        return allPermitToBuy;
    }

    public HashMap<Token,TextTable> printAllPoliticToBuy(){
        HashMap<Token,TextTable> allPoliticToBuy = new HashMap<>();
        players.stream().filter(p -> p.getPermitInSellingTemp().size() != 0).forEach(p -> allPoliticToBuy.put(p.getToken(), printPoliticToBuy(p)));
        return allPoliticToBuy;
    }

    public HashMap<Token,TextTable> printAllAssistantToBuy(){
        HashMap<Token,TextTable> allAssistantToBuy = new HashMap<>();
        players.stream().filter(p -> p.getPoliticInSellingTemp().size() != 0).forEach(p -> allAssistantToBuy.put(p.getToken(), printAssistantsToBuy(p)));
        return allAssistantToBuy;
    }

    public HashMap<Token,Integer> numberOfPermitCards(){
        HashMap<Token,Integer> temp = new HashMap<>();
        players.stream().filter(p -> p.getPermitInSellingTemp().size() != 0).forEach(p -> temp.put(p.getToken(), p.getPermitInSellingTemp().size()));
        return temp;
    }

    public HashMap<Token,Integer> numberOfPoliticCards(){
        HashMap<Token,Integer> temp = new HashMap<>();
        players.stream().filter(p -> p.getPoliticInSellingTemp().size() != 0).forEach(p -> temp.put(p.getToken(), p.getPoliticInSellingTemp().size()));
        return temp;
    }

    public HashMap<Token,Integer> numberOfAssistants(){
        HashMap<Token,Integer> temp = new HashMap<>();
        players.stream().filter(p -> p.getAssistantInSelling().getNum() != 0).forEach(p -> temp.put(p.getToken(), p.getAssistantInSelling().getNum()));
        return temp;
    }

    public HashMap<Token,LinkedList<Integer>> prizeOfPermitCards(){
        HashMap<Token,LinkedList<Integer>> temp = new HashMap<>();
        players.stream().filter(p -> !p.getPermitInSelling().isEmpty()).forEach(p -> {
            LinkedList<Integer> prizes = new LinkedList<>();
            for (PermitCard pc : p.getPermitInSellingTemp()) {
                prizes.addLast(p.getPermitInSelling().get(pc));
            }
            temp.put(p.getToken(), prizes);
        });
        return temp;
    }

    public HashMap<Token,LinkedList<Integer>> prizeOfPoliticCards(){
        HashMap<Token,LinkedList<Integer>> temp = new HashMap<>();
        players.stream().filter(p -> !p.getPoliticInSelling().isEmpty()).forEach(p -> {
            LinkedList<Integer> prizes = new LinkedList<>();
            for (PoliticCard pc : p.getPoliticInSellingTemp()) {
                prizes.addLast(p.getPoliticInSelling().get(pc));
            }
            temp.put(p.getToken(), prizes);
        });
        return temp;
    }

    public HashMap<Token,Integer> prizeOfAssistants(){
        HashMap<Token,Integer> temp = new HashMap<>();
        players.stream().filter(p -> p.getAssistantInSelling().getNum() != 0).forEach(p -> temp.put(p.getToken(), p.getAssistantInSelling().getPrice()));
        return temp;
    }

    private TextTable printPoliticToBuy(Player p){
        String[] columnNames = new String [p.getPoliticInSellingTemp().size()+1];
        columnNames[0] = "Player Name";
        for(int i=1;i<columnNames.length;i++){
            columnNames[i] = "Politic card "+i;
        }
        Object[][] data = new Object [5][columnNames.length];
        data[0][0] = p.getName();
        int i = 1;
        for(PoliticCard pc : p.getPoliticInSellingTemp()){
            data[0][i] = "";
            data[1][i] = Util.getStringFromColor(pc.getCardColor());
            data[2][i] = "";
            data[3][i] = "Prize = " + p.getPoliticInSelling().get(pc);
            data[4][i] = "";
            i++;
        }
        return new TextTable(columnNames, data);
    }

    public TextTable printSideCouncillors(){
        String[] columnNames = new String [8];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = "Councillor "+(i+1);
        }
        Object[][] data = new Object [3][columnNames.length];
        int i = 0;
        for(Councillor c : reserveOfCouncillors){
            data[0][i] = "";
            data[1][i] = Util.getStringFromColor(c.getColor());
            data[2][i] = "";
            i++;
        }
        return new TextTable(columnNames, data);
    }

    public TextTable printAllCouncils(){
        String[] columnNames = {"Type Council", "Councillor 1", "Councillor 2", "Councillor 3", "Councillor 4"};
        Object [][] data = new Object[12][columnNames.length];
        LinkedList<String> l = new LinkedList<>();
        l.addLast("COAST");
        l.addLast("HILLS");
        l.addLast("MOUNTAINS");
        l.addLast("KING");
        for(int i = 0;i<12;i=i+3){
            data[i][0] = "";
            data[i+1][0] = l.removeFirst();
            data[i+2][0] = "";
        }
        int i=1;
        for(Councillor c : regions[0].getRegionCouncil()){
            data[0][i] = "";
            data[1][i] = Util.getStringFromColor(c.getColor());
            data[2][i] = "";
            i++;
        }
        i=1;
        for(Councillor c : regions[1].getRegionCouncil()){
            data[3][i] = "";
            data[4][i] = Util.getStringFromColor(c.getColor());
            data[5][i] = "";
            i++;
        }
        i=1;
        for(Councillor c : regions[2].getRegionCouncil()){
            data[6][i] = "";
            data[7][i] = Util.getStringFromColor(c.getColor());
            data[8][i] = "";
            i++;
        }
        i=1;
        for(Councillor c : kingCouncil){
            data[9][i] = "";
            data[10][i] = Util.getStringFromColor(c.getColor());
            data[11][i] = "";
            i++;
        }
        return new TextTable(columnNames, data);
    }

    public TextTable printRegionCouncils(){
        String[] columnNames = {"Region Council", "Councillor 1", "Councillor 2", "Councillor 3", "Councillor 4"};
        Object [][] data = new Object[9][columnNames.length];
        LinkedList<String> l = new LinkedList<>();
        l.addLast("COAST");
        l.addLast("HILLS");
        l.addLast("MOUNTAINS");
        for(int i = 0;i<9;i=i+3){
            data[i][0] = "";
            data[i+1][0] = l.removeFirst();
            data[i+2][0] = "";
        }
        int i=1;
        for(Councillor c : regions[0].getRegionCouncil()){
            data[0][i] = "";
            data[1][i] = Util.getStringFromColor(c.getColor());
            data[2][i] = "";
            i++;
        }
        i=1;
        for(Councillor c : regions[1].getRegionCouncil()){
            data[3][i] = "";
            data[4][i] = Util.getStringFromColor(c.getColor());
            data[5][i] = "";
            i++;
        }
        i=1;
        for(Councillor c : regions[2].getRegionCouncil()){
            data[6][i] = "";
            data[7][i] = Util.getStringFromColor(c.getColor());
            data[8][i] = "";
            i++;
        }
        return new TextTable(columnNames, data);
    }

    public LinkedList<TextTable> printPermitCardsOfRegions(){
        LinkedList<TextTable> list = new LinkedList<>();
        for (Region region : regions) {
            list.addLast(region.printPermitCards());
        }
        return list;
    }

    public TextTable printPersonalPermitCards(){
        return currPlayer.printPermitDeck();
    }

    public TextTable printPersonalGraveDeck(){
        return currPlayer.printGraveDeck();
    }

    private TextTable printAssistantsToBuy(Player p){
        String[] columnNames = {"Player Name", "Assistants to buy"};
        Object[][] data = new Object [3][columnNames.length];
        data[0][0] = "";
        data[1][0] = p.getName();
        data[2][0] = "";
        data[0][1] = p.getAssistantInSelling().getNum();
        data[1][1] = "";
        data[2][1] = "Prize = " + p.getAssistantInSelling().getPrice();
        return new TextTable(columnNames, data);
    }

    private TextTable printPermitToBuy(Player p){
        String[] columnNames = new String [p.getPermitInSellingTemp().size()+1];
        columnNames[0] = "Player Name";
        for(int i=1;i<columnNames.length;i++){
            columnNames[i] = "Permit card "+i;
        }
        int maxNumberOfBonuses = 3;
        for(PermitCard pc : p.getPermitInSellingTemp()){
            if(pc.getBonus().getNumberOfBonuses() > maxNumberOfBonuses) maxNumberOfBonuses = pc.getBonus().getNumberOfBonuses();
        }
        Object[][] data = new Object [maxNumberOfBonuses+5][columnNames.length];
        data[0][0] = p.getName();
        int i = 1;
        for(PermitCard pc : p.getPermitInSellingTemp()){
            String s = "";
            for(int j=0;j<pc.getCities().length;j++){
                s = s + pc.getCities()[j];
                if(j!=pc.getCities().length-1) s = s + ",";
            }
            data[0][i] = s;
            data[1][i] = "";
            int k = 2;
            for (String string : pc.getBonus().getSeparatedBonuses()) {
                data[k][i] = string;
                k++;
            }
            data[maxNumberOfBonuses+2][i] = "";
            data[maxNumberOfBonuses+3][i] = "Prize = " + p.getPermitInSelling().get(pc);
            data[maxNumberOfBonuses+4][i] = "";
            i++;
        }
        return new TextTable(columnNames, data);
    }

    private TextTable printRegionCards(){
        String[] columnNames = {"Type Region", "Region Card"};
        Object[][] data = new Object [9][columnNames.length];
        int j = 0;
        for (Region region : regions) {
            data[j][0] = "";
            data[j + 1][0] = region.getType();
            data[j + 2][0] = "";
            data[j][1] = "";
            data[j + 1][1] = region.getRegionPrize().getBonus().getSeparatedBonuses().getFirst();
            data[j + 2][1] = "";
            j = j + 3;
        }
        return new TextTable(columnNames, data);
    }

    private TextTable printColorCards(){
        String[] columnNames = new String [colorCards.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = Util.getStringFromColor(colorCards.get(i).getColor()) + " card";
        }
        Object[][] data = new Object [3][columnNames.length];
        int i = 0;
        for(ColorCard cc : colorCards){
            data[1][i] = cc.getBonus().getSeparatedBonuses().getFirst();
            i++;
        }
        return new TextTable(columnNames, data);
    }

    private TextTable printKingCards(){
        String[] columnNames = new String [kingCards.size()];
        for(int i=0;i<columnNames.length;i++){
            columnNames[i] = "King card "+(i+1);
        }
        Object[][] data = new Object [3][columnNames.length];
        int i = 0;
        for(KingCard kc : kingCards){
            data[0][i] = "";
            data[1][i] = kc.getBonus().getSeparatedBonuses().getFirst();
            data[2][i] = "";
            i++;
        }
        return new TextTable(columnNames, data);
    }

    public boolean alreadyBuilt(char city) {
        return getCity(city).contains(currPlayer.getColor());
    }

    private boolean alreadyBuilt(City city) {
        return city.contains(currPlayer.getColor());
    }

    public void corruptCouncil(int council, int councillor) {
        Councillor newCouncillor = getFromReserve(councillor);
        if(council==4) {
            reserveOfCouncillors.add(getKingCouncil().corrupt(newCouncillor));
        }
        else {
            reserveOfCouncillors.add(getRegion(council).getRegionCouncil().corrupt(newCouncillor));
        }
    }

    private Councillor getFromReserve(int choice) {
        return reserveOfCouncillors.remove(choice);
    }

    public void drawPoliticCard() {
        PoliticCard pc = reserveOfPoliticCards.removeFirst();
        currPlayer.getPoliticDeck().add(pc);
    }

    private ColorCard getColorCard(Color color) {
        for (ColorCard cc : colorCards) {
            if(cc.getColor().equals(color)) return cc;
        }
        return null;
    }
    /**
     * Method used for draw a specific number
     * of Politic Card from Politic Card Deck
     *
     * @param num number of politic card to draw
     * @return list containing <strong>num<strong/> of politic to draw
     */
    private LinkedList<PoliticCard> drawCards(int num){
        LinkedList<PoliticCard> l = new LinkedList<>();
        while (num > 0) {
            l.add(reserveOfPoliticCards.removeFirst());
            num--;
        }
        return l;
    }
    public King getKing() {
        return king;
    }

    private Council getCouncil(TypeCouncil tc) {
        switch (tc) {
            case COAST: return getRegion(TypeRegion.COAST).getRegionCouncil();
            case HILLS: return getRegion(TypeRegion.HILLS).getRegionCouncil();
            case MOUNTAINS: return getRegion(TypeRegion.MOUNTAINS).getRegionCouncil();
            default: return kingCouncil;
        }
    }

    public Council getCouncil(int council) {
        switch (council) {
            case 1: return getCouncil(TypeCouncil.COAST);
            case 2: return getCouncil(TypeCouncil.HILLS);
            case 3: return getCouncil(TypeCouncil.MOUNTAINS);
            default: return kingCouncil;
        }
    }

    public void drawInitPoliticCards(){
        for(Player p : players){
            p.getPoliticDeck().add(drawCards(6));
        }
    }

    @Override
    public String toString() {
    	printBoard();
        return "GameBoard{" +
                "king=" + king +
                ", regions=" + Arrays.toString(regions) +
                ", map=" + map +
                ", players=" + players +
                ", currPlayer=" + currPlayer +
                ", reserveOfCouncillors=" + reserveOfCouncillors +
                ", kingCouncil=" + kingCouncil +
                ", nobilityTrack=" + nobilityTrack +
                ", victoryTrack=" + victoryTrack +
                ", moneyTrack=" + moneyTrack +
                ", reserveOfPoliticCards=" + reserveOfPoliticCards +
                ", colorCards=" + colorCards +
                ", kingCards=" + kingCards +
                ", randomPlayerExctraction=" + randomPlayerExctraction +
                ", politicToBuy=" + politicToBuy +
                ", permitToBuy=" + permitToBuy +
                ", assistantToBuy=" + assistantToBuy +
                '}';

    }

    public LinkedList<TextTable> printMap(){
        LinkedList<TextTable> map = new LinkedList<>();
        map.addLast(printBoard());
        map.addLast(printRegionCards());
        map.addLast(printColorCards());
        map.addLast(printKingCards());
        printPermitCardsOfRegions().forEach(map::addLast);
        map.addLast(printNobilityTrack());
        map.addLast(printSideCouncillors());
        map.addLast(printAllCouncils());
        return map;
    }

    public TextTable printBoard(){
    	String[] columnNames = {"City", "Type Region", "Adjacent Cities", "Emporiums", "Coins' Bonuses"};
    	Object[][] data = new Object [map.keySet().size()][columnNames.length];
    	int i = 0;
    	for(City c : map.keySet()){
            String s1 = c.getName();
            if(c.getName().charAt(0) == king.getPosition()) s1 = s1 + "(K)";
    		data[i][0] = s1;
            data[i][1] = c.getTypeRegion();
    		String s = "";
    		Iterator<City> iterator = map.get(c).iterator();
    		while(iterator.hasNext()){
    			s = s + iterator.next().getName();
    			if(iterator.hasNext()) s = s + ", ";
    		}
    		data[i][2] = s;
            s = "";
            Iterator<Emporium> iterator1 = c.getEmporiums().iterator();
            while(iterator1.hasNext()){
                s = s + Util.getStringFromColor(iterator1.next().getColor());
                if(iterator1.hasNext()) s = s + ", ";
            }
            data[i][3] = s;
            s = "";
            Iterator<String> iterator2 = c.getCoin().getBonus().getSeparatedBonuses().iterator();
            while(iterator2.hasNext()){
                s = s + iterator2.next();
                if(iterator2.hasNext()) s = s + ", ";
            }
            data[i][4] = s;
    		i++;
    	}
        return new TextTable(columnNames,data);
    }

    public TextTable printPlayers(){
    	LinkedList<LinkedList<Player>> podium = podium();
    	String[] columnNames = { "Name", "Color","Number of Assistants", "Politic Cards", "Victory Position", "Money Position", "Nobility Position", "Ranking" };
    	Object[][] data = new Object [players.size()][columnNames.length];
    	int i = 0;
    	for(Player p : players){
    		data[i][0] = p.getName();
    		data[i][1] = ColorUtils.getColorNameFromColor(p.getColor());
    		data[i][2] = p.getNumAssistant();
            String s = "";
            Iterator<PoliticCard> iterator = getPlayer(p.getToken()).getPoliticDeck().iterator();
            while(iterator.hasNext()){
                s = s + Util.getStringFromColor(iterator.next().getCardColor());
                if(iterator.hasNext()) s = s + ", ";
            }
            data[i][5] = s;
    		data[i][4] = p.getVictoryPosition().getPosition();
    		data[i][5] = p.getMoneyPosition().getPosition();
    		data[i][6] = p.getNobilityPosition().getPosition();
    		if(podium.get(0).contains(p)) data[i][7] = 1;
    		if(podium.get(1).contains(p)) data[i][7] = 2;
    		if(podium.get(2).contains(p)) data[i][7] = 3;
    		i++;
    	}
        return new TextTable(columnNames, data);
    }

    public LinkedList<String> getNameOfCities(){
        return map.keySet().stream().map(City::getName).collect(Collectors.toCollection(LinkedList::new));
    }

    public LinkedList<String> nameOfCitiesOfPlayerExceptNobilityDec(){

        LinkedList<String> list = new LinkedList<>();
        for (City c : getCitiesOfPlayerExceptNobilityDec()) {
            list.addLast(c.getName());
        }

        return list;
    }

    private LinkedList<LinkedList<Player>> podium(){
    	LinkedList<LinkedList<Player>> podium = new LinkedList<>();
    	LinkedList<Player> first = new LinkedList<>();
    	LinkedList<Player> second = new LinkedList<>();
    	LinkedList<Player> third = new LinkedList<>();
    	first.add(players.getFirst());
    	for(Player p : players){
    		if(p.getVictoryPosition().getPosition() > first.getFirst().getVictoryPosition().getPosition()){
    			first.clear();
    			first.add(p);
    		}
    		if(p.getVictoryPosition().getPosition() == first.getFirst().getVictoryPosition().getPosition()){
    			first.add(p);
    		}
    	}
    	podium.addLast(first);
    	if(first.size() < players.size()){
    		LinkedList<Player> temp1 = players.stream().filter(p -> !podium.get(0).contains(p)).collect(Collectors.toCollection(LinkedList::new));
            for (Player p : temp1) {
                if (p.getVictoryPosition().getPosition() > second.getFirst().getVictoryPosition().getPosition()) {
                    second.clear();
                    second.add(p);
                }
                if (p.getVictoryPosition().getPosition() == second.getFirst().getVictoryPosition().getPosition()) {
                    second.add(p);
                }
            }

    	}
    	podium.addLast(second);
    	if(first.size() + second.size() < players.size()){
    		LinkedList<Player> temp2 = new LinkedList<>();
    		for(Player p : players){
    			if(!first.contains(p) || !second.contains(p)) temp2.add(p);
    		}
    		for(Player p : temp2){
    			if(p.getVictoryPosition().getPosition() > third.getFirst().getVictoryPosition().getPosition()){
    				third.clear();
    				third.add(p);
    			}
    			if(p.getVictoryPosition().getPosition() == third.getFirst().getVictoryPosition().getPosition()){
    				third.add(p);
    			}
    		}
    	}
    	podium.addLast(third);
    	return podium;
    }

    public void setIsFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public LinkedList<City> getCitiesOfPlayerExceptNobilityDec() {
        LinkedList<City> list = getCitiesOfPlayer();
        list.stream().filter(c -> c.getCoin().getBonus().hasNobilityDecorator()).forEach(list::remove);
        return list;
    }

    /*
    public int numOfCitiesOfPlayerExceptNobilityDec(){
        return getCitiesOfPlayerExceptNobilityDec().size();
    }
    */

    public int numOfAllPermit(){
        return currPlayer.numOfAllPermit();
    }

    public TextTable printAllPersonalPermit(){
        return currPlayer.printAllPermitCards();
    }

    public TextTable printCitiesOfPlayerExceptNobilityDec(){
        LinkedList<City> cities = getCitiesOfPlayerExceptNobilityDec();
        String[] columnNames = {"Cities", "Coins' Bonuses"};
        Object[][] data = new Object [cities.size()][columnNames.length];
        int i = 0;
        for(City c : cities){
            data[i][0] = c.getName();
            Iterator<String> iterator = c.getCoin().getBonus().getSeparatedBonuses().iterator();
            String s = "";
            while(iterator.hasNext()){
                s = s + iterator.next();
                if(iterator.hasNext()) s = s +", ";
            }
        }
        TextTable tt = new TextTable(columnNames, data);
        return tt;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

