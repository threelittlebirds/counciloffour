package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.view.messages.ResultMsgPair;
import projectPackage.view.messages.broadcastMessage.GeneralInfoBroadcastMsg;
import projectPackage.view.messages.responseMessages.responseActions.AskToBuyMessage;
import projectPackage.view.messages.responseMessages.responseActions.AskToSellMessage;

public class MarketState extends TurnState {

	private boolean buySell;

	public MarketState(TurnMachine tm) {
		super(tm);
	}

	@Override
	public void start(Action a) throws CloneNotSupportedException {
		if(a instanceof WantToBuyAction) {
			//WantToBuyAction buyAction = (WantToBuyAction) a;
			buySell = false;
		}
		if(a instanceof WantToSellAction){
			//SellAction sellAction = (SellAction) a;
			buySell = true;
		}
	}

	@Override
	public ResultMsgPair nextState() throws CloneNotSupportedException {
		if(buySell){		//Selling
			tm.setCurrentState(new SellBuyState(tm));
			broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), "Player " + tm.getGameBoard().getCurrPlayer().getName() + " starts his sell-phase");
			responseMsg = new AskToSellMessage(tm.getGameBoard().printSeparatePermitToSell(),tm.getGameBoard().printSeparatePoliticToSell(),tm.getGameBoard().printAssistantsToSell(),tm.getGameBoard().getCurrPlayer().getNumAssistant(), tm.getGameBoard().getCurrPlayer().getMoneyPosition()
			.getPosition());
			return new ResultMsgPair(responseMsg, broadcastMessage);
		}else{			//Buying
			tm.setCurrentState(new SellBuyState(tm));
			broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), "Player " + tm.getGameBoard().getCurrPlayer().getName() + " starts his sell-phase");
			responseMsg = new AskToBuyMessage(tm.getGameBoard().printAllPermitToBuy(),tm.getGameBoard().printAllPoliticToBuy(),tm.getGameBoard().printAllAssistantToBuy(),tm.getGameBoard().numberOfPermitCards(), tm.getGameBoard().numberOfPoliticCards(),tm.getGameBoard().numberOfAssistants(),tm.getGameBoard().prizeOfPermitCards(),tm.getGameBoard().prizeOfPoliticCards(),tm.getGameBoard().prizeOfAssistants(), tm.getGameBoard().getCurrPlayer().getMoneyPosition()
					.getPosition());
			return new ResultMsgPair(responseMsg, broadcastMessage);
		}
		//tm.setCurrentState(new EndState(tm));
		//broadcastMessage = new GeneralInfoBroadcastMsg(tm.getGameBoard().indexOfCurrentPlayer(), "Player " + tm.getGameBoard().getCurrPlayer().getName() + " finish his sell-phase");
		//responseMsg = new GoToEndMessage(tm.getGameBoard().getCurrPlayer().getName());
		//return tm.start(new EndAction(tm));
		//return new ResultMsgPair(responseMsg, broadcastMessage);
	}


}
