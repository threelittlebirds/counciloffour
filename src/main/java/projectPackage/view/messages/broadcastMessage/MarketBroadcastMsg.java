package projectPackage.view.messages.broadcastMessage;

import projectPackage.model.utility.table.TextTable;
import projectPackage.view.messages.MessageVisitor;

import java.util.LinkedList;

/**
 * Created by carme on 29/06/2016.
 */
public class MarketBroadcastMsg implements BroadcastMessage {

    private final int nextPlayer;
    private final int player;
    private LinkedList<TextTable> separatePermitCards;
    private LinkedList<TextTable> separatePoliticCards;
    private TextTable assistants;
    private int numAssistants;

    public MarketBroadcastMsg(int player, int nextPlayer, LinkedList<TextTable> separatePermitCards, LinkedList<TextTable> separatePoliticCards, TextTable assistants, int numAssistants) {
        this.player = player;
        this.nextPlayer = nextPlayer;
        this.separatePermitCards = separatePermitCards;
        this.separatePoliticCards = separatePoliticCards;
        this.assistants = assistants;
        this.numAssistants = numAssistants;
    }

    public int getPlayer() {
        return player;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public int getNumAssistants() {
        return numAssistants;
    }

    public TextTable getAssistants() {
        return assistants;
    }

    public LinkedList<TextTable> getSeparatePoliticCards() {
        return separatePoliticCards;
    }

    public LinkedList<TextTable> getSeparatePermitCards() {
        return separatePermitCards;
    }

    @Override
    public void display(MessageVisitor visitor) {
        visitor.display(this);
    }
}
